export interface LooseObject {
    [key: string]: any
} 

export interface LocalData {
    sv?: Array<{}>,
    fl?: Array<{}>,
    ki?: Array<{}>,
    el?: Array<{}>,
    pr?: Array<{}>
}
