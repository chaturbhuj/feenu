//Modules
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { HttpModule } from "@angular/http";
import { HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { MetaModule } from "ng2-meta";
import { MetaGuard } from "ng2-meta";

import * as $ from "jquery";

import {
  MatDatepickerModule,
  MatInputModule,
  MatNativeDateModule,
  MatSnackBarModule,
  MatDialogModule
} from "@angular/material"; /**/

import { RouterModule, Routes } from "@angular/router";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { ServiceWorkerModule } from "@angular/service-worker";

import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown'; 

import {} from "../environments/environment";


//Services 
import { PanelServices } from "./services/panel.service";


  
//Components
import { AppComponent } from "./app.component";

import { adstxtComponent } from "./footer/adstxt.component";
import { NgxDateRangePickerModule } from "ngx-daterangepicker";

import { homeComponent } from "./home/home.component";
import { redirectComponent } from './redirect/redirect.component';


import { environment } from "../environments/environment.prod";
import { ModalLoaderComponent } from "./modal-loader/modal-loader.component";
import { feenuLoaderComponent } from "./loader/feenuloader.component";

// image cropper
import { ImageCropperModule } from "ngx-image-cropper";

// auth guard
import { AuthGuard } from "./auth.guard";

import {GoogleAnalyticsEventsServiceService} from "./google-analytics-events-service.service";
//
import { ApiServices } from "./services/api.service";

import { NgxPermissionsModule } from 'ngx-permissions'; 

import { SharedModule } from './shared/shared.module';



const appRoutes: Routes = [
  {
    path: "adminlogin",
	loadChildren: './login/login.module#loginModule',
    canActivate: [MetaGuard],
    data: { meta: { title: "Admin Login" } }
  },
 {
    path: "enquirylist",
	loadChildren: './enquiry/enquirylist.module#enquirylistModule',
    //component: enquirylistComponent,
    canActivate: [MetaGuard],
    data: { meta: { title: "Enquiry list" } }
  },
 {
    path: "ads.txt",
    component: adstxtComponent,
    canActivate: [MetaGuard],
    data: { meta: { title: "" } }
  },
  {
    path: "game-feedback",
    loadChildren: './game_feedback/game_feedback.module#game_feedbackModule',
    canActivate: [MetaGuard],
    data: { meta: { title: "Game feedback" } }
  },
  {
    path: "tags",
     loadChildren: './tags/tags.module#tagsModule',
    canActivate: [MetaGuard],
    data: { meta: { title: "Tags" } }
  },
  {
    path: "publiccontent",
     loadChildren: './publiccontent/publiccontent.module#publiccontentModule',
    canActivate: [MetaGuard],
    data: { meta: { title: "Public Contents" } }
  },
   {
    path : 'customer-resetpassword/:verification_code',
     loadChildren: './user/reset_password.module#reset_passwordModule',
	canActivate: [MetaGuard],
    data: {
		meta: {
			title: 'ResetPassword',
			'og:description': 'Welcome to Feenu.com! Feenu is committed to protecting your privacy online. On this page, you can find out more about our cookies and our privacy policy.',
		      'og:image': 'assets/images/favicon.png'
			}
		}
  },
   {
    path: "privacy",
	loadChildren: './publiccontent/privacy.module#privacyModule', 
    canActivate: [MetaGuard],
    data: { 
		meta: {  
			title: "Privacy Policy | Feenu.com",
			'og:description': 'Welcome to Feenu.com! Feenu is committed to protecting your privacy online. On this page, you can find out more about our cookies and our privacy policy.',
			'og:image': 'assets/images/favicon.png'
			 } 
		}
  },
   {
    path: "dmca",
	loadChildren: './publiccontent/dmca.module#dmcaModule',
    canActivate: [MetaGuard],
    data: { 
	meta: { 
	    title: "DMCA | Feenu.com",
		'og:description': 'Welcome to Feenu.com! Feenu is committed to protecting your privacy online. On this page, you can find out more about our cookies and our privacy policy.',
		'og:image': 'assets/images/favicon.png'
	     } 
	}
  },
  {
    path: "sitemap",
    loadChildren: './sitemap/sitemap.module#sitemapModule',
    canActivate: [MetaGuard],
    data: { 
	meta: { 
	    title: "Sitemap | Feenu.com",
		'og:description': 'Welcome to Feenu.com! Feenu is committed to protecting your privacy online. On this page, you can find out more about our cookies and our privacy policy.',
		'og:image': 'assets/images/favicon.png'
	     } 
	}
  },
  {
    path : 'game-comment',
   loadChildren: './commentlist/commentlist.module#commentlistModule',
	  canActivate: [MetaGuard],
		data: {filter:{status:null},
          meta: {
			  title: 'Feenu comment list',
			  description: 'Feenu Search Page',
			  'og:image': 'assets/images/favicon.png'
			  }
      }
  },
   {
    path: 'addstaff/:staff_id',
     loadChildren: './staff/addstaff.module#addstaffModule',
    canActivate: [MetaGuard],
    data: { meta: { title: "Add staff" } }
  },
   {
    path: 'user-edit/:user_id',
     loadChildren: './userlist/user_edit.module#user_editModule',
    canActivate: [MetaGuard],
    data: { meta: { title: "edit user" } }
  },
  {
    path: 'user-fav-game/:game_id',
     loadChildren: './games/game_user_fav.module#game_user_favModule',
    canActivate: [MetaGuard],
    data: { meta: { title: "game_user_fav" } }
  },
  {
    path: 'user-play-game/:game_id',
     loadChildren: './games/game_user_play.module#game_user_playModule',
    canActivate: [MetaGuard],
    data: { meta: { title: "user-play-game" } }
  },
   {
    path: 'addgames/:game_slug/:games_id',
	loadChildren: './games/addgames.module#addgamesModule',
    canActivate: [MetaGuard],
    data: { meta: { title: "Add Game" } }
  },
  {
    path : 'redirect/:newpageurl',
    component: redirectComponent,
	canActivate: [MetaGuard],
      data: {
		meta:{ 
				title: "Play 1000s of Html5 and WebGL games on Feenu.com",
				description: 'Play the best online games for free at Feenu! Here you ll find everything from the latest action and racing games to the cutest dress-up games, and more!',
			    'og:image': 'assets/images/favicon.png'
			
			} 
		}
  },
  
  {
    path : 'u/:user_id',
   loadChildren: './user_profile/user_profile.module#user_profileModule',
	canActivate: [MetaGuard],
      data: {
		meta:{ 
				title: "Play 1000s of Html5 and WebGL games on Feenu.com",
				description: 'Play the best online games for free at Feenu! Here you ll find everything from the latest action and racing games to the cutest dress-up games, and more!',
			    'og:image': 'assets/images/favicon.png'
				
				} 
			}
  },
  
  {
    path : 'search',
    loadChildren: './search/search.module#searchModule',
	  canActivate: [MetaGuard],
		data: {filter:{category_id:null,category_name:null,searchby:null},
          meta: {
			  title: 'Feenu Search Page',
			  description: 'Feenu Search Page',
			  'og:image': 'assets/images/favicon.png'
			  }
      }
  },
   
  {
    path : 'popular-games',
    loadChildren: './fav_game/fav_game.module#fav_gameModule',
	  canActivate: [MetaGuard],
	  data: {
		 meta: {
			  title: 'Feenu favourite game',
			  description: 'Feenu favourite game',
			  'og:image': 'assets/images/favicon.png'
			  }
		}
      
  },
  {
    path : 'new-games',
    loadChildren: './new_game/new_game.module#new_gameModule',
	  canActivate: [MetaGuard],
	   data: {
		 meta: {  
			  title: 'Feenu new game',
			  description: 'Feenu new game',
			  'og:image': 'assets/images/favicon.png'
			  }
		}
      
  },
  {
    path : 'c/:mycate',
    loadChildren: './search/bycategory.module#bycategoryModule',
	canActivate: [MetaGuard],
    data: {
		meta:{ 
				title: "Play 1000s of Html5 and WebGL games on Feenu.com",
				description: 'Play the best online games for free at Feenu! Here you ll find everything from the latest action and racing games to the cutest dress-up games, and more!',
			    'og:image': 'assets/images/favicon.png'
				
				} 
			}
  },
   {
    path : 't/:mytag',
    loadChildren: './search/bytag.module#bytagModule',
	 canActivate: [MetaGuard],
    data: {
		meta:{ 
				title: "Play 1000s of Html5 and WebGL games on Feenu.com",
				description: 'Play the best online games for free at Feenu! Here you ll find everything from the latest action and racing games to the cutest dress-up games, and more!',
			    'og:image': 'assets/images/favicon.png'
				
				} 
			}
  },
  {
    path : 'cookies-policy',
	loadChildren: './publiccontent/cookies_policy.module#cookies_policyModule',
	canActivate: [MetaGuard],
    data: {
		meta:{ 
				title: "cookies-policy | Feenu",
				description: 'Play the best online games for free at Feenu! Here you ll find everything from the latest action and racing games to the cutest dress-up games, and more!',
			    'og:image': 'assets/images/favicon.png'
				
				} 
			}
  },
  {
    path: 'g/:games_id',
	loadChildren: './games/gamedetail.module#gamedetailModule',
    canActivate: [MetaGuard],
    data: {
		meta:{ 
				title: "Play Free Online Games on Feenu.com - Life is Fun! | Feenu",
				description: 'Play the best online games for free at Feenu! Here you ll find everything from the latest action and racing games to the cutest dress-up games, and more!',
			    'og:image': 'assets/images/favicon.png'
				
				} 
			}
  }, 
  {
    path: "gamelist",
	loadChildren: './games/gamelist.module#gamelistModule',
    canActivate: [MetaGuard],
    data: { meta: { title: "gamelist" } }
  },
  {
    path: "staffprofile",
    loadChildren: './staff/staffprofile.module#staffprofileModule',
    canActivate: [MetaGuard],
    data: { meta: { title: "staffprofile" } } 
  },
  {
    path: "module",
    loadChildren: './module/module.module#moduleModule',
    canActivate: [MetaGuard],
    data: { meta: { title: "module" } }
  },
   {
    path: "userlist",
     loadChildren: './userlist/userlist.module#userlistModule',
    canActivate: [MetaGuard],
    data: { meta: { title: "User List" } }
  },
  {
    path: "home",
    component: homeComponent,
    canActivate: [MetaGuard],
    data: {
		meta:{ 
				title: "Play 1000s of Html5 and WebGL games on Feenu.com",
				description: 'Play the best online games for free at Feenu! Here you ll find everything from the latest action and racing games to the cutest dress-up games, and more!',
			    'og:image': 'assets/images/favicon.png'
				
				} 
			}
  },
   {
    path: "change-password",
  loadChildren: './user/change_password.module#changepasswordModule',
    canActivate: [MetaGuard],
    data: {
		meta:{ 
				title: "Change password | Feenu",
				description: 'Play the best online games for free at Feenu! Here you ll find everything from the latest action and racing games to the cutest dress-up games, and more!',
			    'og:image': 'assets/images/favicon.png'
				
				} 
			}
  },
  {
    path: "",
    component: homeComponent,
    pathMatch: "full",
    canActivate: [MetaGuard],
    data: {
		meta:{ 
				title: "Play 1000s of Html5 and WebGL games on Feenu.com",
				description: 'Play the best online games for free at Feenu! Here you ll find everything from the latest action and racing games to the cutest dress-up games, and more!',
			    'og:image': 'assets/images/favicon.png'
				
				} 
			}
  },
  {
    path: "aboutus",
	loadChildren: './publiccontent/aboutus.module#aboutusModule',
    canActivate: [MetaGuard],
    data: {
		meta:{ 
				title: "  About Feenu.com - Get To Know Us | Feenu",
				description: 'Welcome to Feenu.com, the best website for free online games! Browse this page to read reviews and learn more about our websites, apps, and other products!',
			    'og:image': 'assets/images/favicon.png'
				
				} 
			}
  },
  {
    path: "staff-list",
	loadChildren: './staff/stafflist.module#stafflistModule',
    canActivate: [MetaGuard],
    data: { meta: { title: "staff list" } }
  },
  {
    path: "gamecategory",
	loadChildren: './games/gamecategory.module#gamecategoryModule',
    canActivate: [MetaGuard],
    data: { meta: { title: "game-category" } }
  },
   {
    path: "addcategory/:cat_id",
	loadChildren: './games/addgamecategory.module#addgamecategoryModule',
    canActivate: [MetaGuard],
    data: { meta: { title: "game-category" } }
  },
  {
    path: "addtag/:tag_id",
	loadChildren: './tags/addtag.module#addtagModule',
    canActivate: [MetaGuard],
    data: { meta: { title: "game-tag" } }
  },
  {
    path: "submodule",
	loadChildren: './module/submodule.module#submoduleModule',
    canActivate: [MetaGuard],
    data: { meta: { title: "submodule" } }
  },
  {
    path: "contact-us",
	loadChildren: './publiccontent/contactus.module#contactusModule',
    canActivate: [MetaGuard],
     data: {
		meta:{ 
				title: "Contact Our Team For Questions Or Comments | Feenu",
				description: 'Welcome to Feenu! Would you like to get in touch with the team at Feenu.com? Whatever your query, we d love to hear from you. Here s how you can contact us!',
			    'og:image': 'assets/images/favicon.png'
				
				} 
			}
  },
   {
    path: "login",
	loadChildren: './register/login.module#loginModule',
    canActivate: [MetaGuard],
     data: {
		meta:{ 
				title: "login | Feenu",
				description: 'Welcome to Feenu! Would you like to get in touch with the team at Feenu.com? Whatever your query, we d love to hear from you. Here s how you can contact us!',
			    'og:image': 'assets/images/favicon.png'
				
				} 
			}
  },
   {
    path: "c-verify/:verification_code",
	loadChildren: './register/c_verify.module#c_verifyModule',
    canActivate: [MetaGuard],
     data: {
		meta:{ 
				title: "c_verify | Feenu",
				description: 'Welcome to Feenu! Would you like to get in touch with the team at Feenu.com? Whatever your query, we d love to hear from you. Here s how you can contact us!',
			    'og:image': 'assets/images/favicon.png'
				
				} 
			}
  },
    {
    path: "registration",
	loadChildren: './register/register.module#registerModule',
    canActivate: [MetaGuard],
     data: {
		meta:{ 
				title: "registration | Feenu",
				description: 'Welcome to Feenu! Would you like to get in touch with the team at Feenu.com? Whatever your query, we d love to hear from you. Here s how you can contact us!',
			    'og:image': 'assets/images/favicon.png'
				
				} 
			}
  },
  {
    path: "forgetpassword",
   loadChildren: './user/forget_password.module#forgetpasswordModule',
    canActivate: [MetaGuard],
    data: {
		meta:{ 
				title: "Play Free Online Games on Feenu.com - Life is Fun! | Feenu",
				description: 'Play the best online games for free at Feenu! Here you ll find everything from the latest action and racing games to the cutest dress-up games, and more!',
			    'og:image': 'assets/images/favicon.png'
				
				} 
			}
  },
  {
    path: "changeprofile",
    loadChildren: './user/changeprofile.module#changeprofileModule',
	canActivate: [MetaGuard],
    data: { meta: { 
	title: "changeprofile",
	description: 'Play the best online games for free at Feenu! Here you ll find everything from the latest action and racing games to the cutest dress-up games, and more!',
	'og:image': 'assets/images/favicon.png'
	} }
  },
  {
    path: "dashboard",
	loadChildren: './dashboard/dashboard.module#dashboardModule',
    canActivate: [MetaGuard],
     data: {
		meta:{ 
				title: "Play 1000s of Html5 and WebGL games on Feenu.com",
				description: 'Play the best online games for free at Feenu! Here you ll find everything from the latest action and racing games to the cutest dress-up games, and more!',
			    'og:image': 'assets/images/favicon.png'
				
				} 
			}
  },
  {
    path: "user-play-game",
    loadChildren: './user_dashboard/user_dashboard.module#user_dashboardModule',
    canActivate: [MetaGuard],
    data: { meta: { title: "User Dashboard" } }
  },
  {
    path: "user-fav-game",
    loadChildren: './user_dashboard/user_fav.module#user_favModule',
    canActivate: [MetaGuard],
    data: { meta: { title: "User Dashboard" } }
  },
  {
    path: "page-not-found",
    loadChildren: './page_not_found/page_not_found.module#page_not_foundModule',
    canActivate: [MetaGuard],
    data: { meta: { title: "page-not-found" } }
  },
  {
    path: "admin-forgetpassword",
	loadChildren: './login/forget_password.module#forgetpasswordModule',
    canActivate: [MetaGuard],
    data: { meta: { title: "Admin Forget Password" } }
  },
  {
    path: "admin-resetpassword/:verification_code",
	loadChildren: './login/resetpassword.module#resetpasswordModule',
    canActivate: [MetaGuard],
    data: { meta: { title: "Admin Reset Password" } }
  },
  {
    path: "staff",
	loadChildren: './staff/staff.module#staffModule',
    canActivate: [MetaGuard]
  },
  {path: '**', redirectTo: 'page-not-found'}
];


@NgModule({
  declarations: [
    AppComponent,
    homeComponent,
    redirectComponent,
	adstxtComponent, 
    ModalLoaderComponent,
	feenuLoaderComponent
  ],
  imports: [
    BrowserModule,SharedModule,
    ServiceWorkerModule,
    BrowserAnimationsModule,
    MatDatepickerModule,
    MatInputModule,
    MatNativeDateModule,
    MatSnackBarModule,
    ReactiveFormsModule,NgMultiSelectDropDownModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    
    RouterModule.forRoot(appRoutes),
    ImageCropperModule,
    NgxDateRangePickerModule,
    MetaModule.forRoot(),
    NgxPermissionsModule.forRoot(),	
    ReactiveFormsModule
  ],
  exports: [],
  providers: [
    PanelServices,
    ApiServices,
    AuthGuard,
	GoogleAnalyticsEventsServiceService
  ],
  bootstrap: [AppComponent],
  entryComponents: []
})
export class AppModule {}

