import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { FormGroup, FormControl } from "@angular/forms";
import { PanelServices } from '../services/panel.service';

declare var jquery: any;
declare var $ : any;
@Component({
  selector: 'app-enquirylist',
  templateUrl: './enquirylist.component.html',
  styleUrls: ['./enquirylist.component.css']
})
export class enquirylistComponent implements OnInit {
	loader;
	enquirylist = '';
	constructor(public router: Router,
              private panelService: PanelServices) {
				$('body').removeClass('body_img');
				
		var admin_data = JSON.parse(localStorage.getItem("FeenuAdminData"));
	   
	   console.log(admin_data.user._id);
		if(admin_data.user == null){
			this.panelService.openSnackBar('Please Login to continue', 'Ok','errorMsg');
			this.router.navigate(['/adminlogin']);
		} else if(!admin_data.user._id){
			this.panelService.openSnackBar('Please Login to continue', 'Ok','errorMsg');
			this.router.navigate(['/adminlogin']);
		}
			  }

	ngOnInit(){ 
		this.get_all_enquiry_list(); 
	}
	
	get_all_enquiry_list(){
	this.loader = true;
		var normalizeSellerPayload = {
			'apiname': 'list_enquiry'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				this.enquirylist = data.data;
				
		});
	}
	
	remove_enquiry(contact_id){
		var val = confirm("Do you want to delete");
		if(val == true){
		this.loader = true;
		var normalizeSellerPayload = {
			'apiname': 'delete_enquiry',
			'contact_id': contact_id
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
			this.loader = false;
			 this.get_all_enquiry_list(); 
				if(data.status == 201){
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					this.panelService.openSnackBar(data.message, 'OK','successMsg');
					
				}
		});
	  	return true;
		}else{
			return false;
		}
	}
  
  
}
