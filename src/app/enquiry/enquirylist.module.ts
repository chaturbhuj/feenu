import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { enquirylistComponent } from './enquirylist.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [enquirylistComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([{
      path: '',
      component: enquirylistComponent
    }])
  ]
})
export class enquirylistModule { }
