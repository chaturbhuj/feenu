import { Component, OnInit } from '@angular/core';
import { Router , ActivatedRoute} from '@angular/router';
import { Location } from '@angular/common';
import { FormGroup, FormControl } from "@angular/forms";
import { PanelServices } from '../services/panel.service';

declare var jquery: any;
declare var $ : any;
@Component({
  selector: 'app-commentlist',
  templateUrl: './commentlist.component.html',
  styleUrls: ['./commentlist.component.css']
})
export class commentlistComponent implements OnInit {
	loader;
	commentlist = '';
	submodulelist = '';
	accesslist = '';
	user_access_list = '';
	emp_name = '';
	userstatus = '';
	commentstatus = '';
	user_id = 0;
	comment_id = 0;
	emp_id = 0;
	submudule_id = '';
	module_id = '';
	permission = '';
	search = 'no';
	skip = 0;
	limit = 20;
	count: any;
		index1 = 1;
	public current_page: number = 0;
		totalcount: number = 0;
	constructor(public router: Router,  private route: ActivatedRoute, private panelService: PanelServices) {
				$('body').removeClass('body_img');
				this.route.queryParams.subscribe(params => {
					
					this.commentstatus = params['status'] ? params['status'] : null;
					console.log(this.commentstatus);
				}); 
				
			  }

	ngOnInit(){ 
		this.get_all_comment_list(this.skip,this.limit); 
		
	}
	
	open_game(gameid){
		    this.loader = true;
		var normalizeSellerPayload = {
			'game_id': gameid,
			'apiname': 'by_id_open_game'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				console.log(data.data);
				if(data.data[0]){
				this.router.navigate(['g/'+data.data[0].game_slug]); 	
				}else{
					this.panelService.openSnackBar('This game not active yet', 'OK','successMsg');
				}
				//this.router.navigate(['g/'+this.gameurl]); 
		});
	}
	
	get_all_comment_list(skip,limit){
	    this.loader = true;
		var normalizeSellerPayload = {
			'commentstatus': this.commentstatus,
			'skip':skip,
			'limit':limit,
			'apiname': 'get_all_comment_list'
		} 
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				//console.log(data.data);
			 this.commentlist = data.data;
			 this.count = Array(Math.ceil(data.count/this.limit)).fill(0).map((x,i)=>i);
				this.totalcount = (Math.round(data.count/this.limit));
				
		});
	}
	
	setpage(i, event: any) {
		//event.prevendDefult();
		this.current_page = i;
		
		console.log(i);
		if(i == 0){
		  var page = ((i*20));	
		}else{
			var page = ((i*20)+1);
		}
		this.index1 = ((i*20)+1);
		

		this.get_all_comment_list(page,this.limit);
     	 
     	   
	}
	
	delete_comment(user_id){
		this.loader = true;
		var normalizeSellerPayload = {
			'apiname': 'delete_comment',
			'id': user_id
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
			this.loader = false;
			 this.get_all_comment_list(this.skip,this.limit); 
				if(data.status == 201){
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					this.panelService.openSnackBar(data.message, 'OK','successMsg');
					
				}
		});
	}
	
	editcommentstatus(comment_id, commentstatus){
		
		this.loader = true;
		var normalizeSellerPayload = {
			'comment_id': comment_id,
			'commentstatus': commentstatus,
			'apiname': 'update_comment_status'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
			this.loader = false;
			this.get_all_comment_list(this.skip,this.limit); 
				if(data.status == 201){
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					this.panelService.openSnackBar(data.message, 'OK','successMsg');
					
				}
				//$('.close').trigger('click');
		});
  }
	
	/* editcommentstatus(comment_id, commentstatus){
		this.comment_id = comment_id;
		this.commentstatus = commentstatus;
	} */
	approval(skip,limit,permission){
		this.search = 'yes'
		console.log(permission);
		this.permission = permission;
		this.loader = true;
		var normalizeSellerPayload = {
			'skip':skip,
			'limit':limit,
			'permission': permission,
			'apiname': 'get_approval_comment_list'
			
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				console.log(data.data);
			 this.commentlist = data.data;
			this.count = Array(Math.ceil(data.count/this.limit)).fill(0).map((x,i)=>i);
				this.totalcount = (Math.round(data.count/this.limit));
				
		});
	}
		setsearchpage(i, event: any){
		this.current_page = (i);
		console.log("yes");
		//this.gamelist = '';
		//this.gamelist.splice(0,this.gamelist.length)
		if(i == 0){
		  var page = ((i*20));	
		}else{
			var page = ((i*20)+1);
		}
		this.index1 = ((i*20)+1);
		
		this.approval(page,20,this.permission); 
		
	}
	
	get_all_submodule_list(){
	this.loader = true;
		var normalizeSellerPayload = {
			'apiname': 'list_submodule'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
			this.loader = false;
				this.submodulelist = data.data;
				console.log(this.submodulelist);
				console.log(this.accesslist);
				
				/* $.each(this.submodulelist,function(i,val){
					var result=$.inArray(val,this.accesslist);
					if(result!=-1)
					alert(result); 
					}) */
				//$(".myCheckbox").prop('checked', true);
		});
	}
	
	get_all_access_list(){
	this.loader = true;
		var normalizeSellerPayload = {
			'apiname': 'list_access'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
			this.loader = false;
				this.accesslist = data.data;
				console.log(this.accesslist);
		});
	}
	
	emp_access(user_id, name){
		this.emp_id = user_id;
		this.emp_name = name;
		
	    this.loader = true;
		var normalizeSellerPayload = {
			'user_id': this.emp_id,
			'apiname': 'access_list_by_id'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				this.user_access_list = data.data;
				 $.each(this.user_access_list,function(i,val){
					console.log(val.submodule_id);
					$('input[value="'+val.submodule_id+'"]').prop('checked', true);
				})
				console.log(this.user_access_list); 
		});
	
	}
	
	
	
	accessSubmit(){
		console.log(this.emp_id);
		//this.loader = true;
		var access_info = [];
		$('input[type=checkbox]').each(function () {
			var aa = this.emp_id;
			console.log(this.emp_id);
				   if (this.checked) {
					    console.log($(this).closest("td").siblings("td")); 
					   // console.log($('#module_link').val()); 
					 access_info.push({
							submodule_id:$(this).val(),
							module_id :$(this).attr('name')
						}); 
				   }
		});	
		
	
		
		console.log(access_info);
		
		var normalizeSellerPayload = {
			'access_info': access_info,
			'staff_id': this.emp_id,
			'apiname': 'add_access1'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
			//this.loader = false;
			
				if(data.status == 201){
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					this.panelService.openSnackBar(data.message, 'OK','successMsg');
					$('#browseAccessModal .close').trigger('click');
					$('#browseAccessModal').removeClass('show');
				}
				
		});
	}
  
  
}
