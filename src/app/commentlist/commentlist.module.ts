import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { commentlistComponent } from './commentlist.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [commentlistComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([{
      path: '',
      component: commentlistComponent
    }])
  ]
})
export class commentlistModule { }
