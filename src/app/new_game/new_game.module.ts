import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { new_gameComponent } from './new_game.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [new_gameComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([{
      path: '',
      component: new_gameComponent
    }])
  ]
})
export class new_gameModule { }
