import { Component, OnInit } from '@angular/core';
//import { Router } from '@angular/router';
import { PanelServices } from '../services/panel.service';
import { Router, ActivatedRoute } from '@angular/router';
declare var jquery : any;
declare var $ : any;

@Component({
  selector: 'app-new_game',
  templateUrl: './new_game.component.html',
  styleUrls: ['./new_game.component.css']
})
export class new_gameComponent implements OnInit {
	created_date = '';
	created_time = '';
	game_category_id=0;
	gamelist= '';
	first_name = '';
	last_name = '';
	email_id = '';
	pass = '';
	searchgamelist = '';
	searchgamecatelist = '';
	searchby;
	confirm_pass = '';
	category_name;
	gender = '';
	check_device = '';
	category_id = '';
	user_password = '';
	  length = 0;
	 loader;

   constructor(private router: Router,private route: ActivatedRoute,
              public panelService: PanelServices) {
				 
			  }

  ngOnInit() {
	  var d = new Date();
		var month = d.getMonth()+1;
		var day = d.getDate();

		this.created_date = d.getFullYear() + '-' + (month<10 ? '0' : '') + month + '-' + (day<10 ? '0' : '') + day;
		this.created_time = (d.getHours()<10 ? '0' : '') + d.getHours() + ":" + (d.getMinutes()<10 ? '0' : '') + d.getMinutes() + ":" + (d.getSeconds()<10 ? '0' : '') + d.getSeconds();
		
		this.get_new_game_list();
	if(!this.panelService.checkIfUserOnMobile()){
			$('.navbar').addClass('head1');
	        $('.footer').addClass('foot');
		}else{
			
		}
  }
   
  get_new_game_list(){
	  if(this.panelService.checkIfUserOnMobile()){
			this.check_device = 'mobile';
		}else{
			this.check_device = 'pc';
			}
	   this.loader = true;
		var normalizeSellerPayload = {
			'check_device': this.check_device,
			'apiname': 'get_new_game_list'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				console.log(data.data);
				this.searchgamelist = data.data;
			});
	}
	
	getFiltercategame(){
	    this.loader = true;
		var normalizeSellerPayload = {
			'category_name': this.category_name,
			'apiname': 'search_game_by_cate'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				 this.loader = false;
				console.log(data.data);
				this.searchgamecatelist = data.data;
			});
	}
   
}
