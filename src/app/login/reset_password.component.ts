import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatSnackBar, MatDialog } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { PanelServices } from '../services/panel.service';

@Component({
  selector: 'app-resetpassword',
  templateUrl: './reset_password.component.html',
  styleUrls: ['./reset_password.component.css']
})
export class adminresetpasswordComponent implements OnInit{
  
 
  loader;
  verification_code;
  new_password = '';
  confirm_password = '';
  constructor(
              public snackBar : MatSnackBar,
              public matDialog : MatDialog,private activatedRoute: ActivatedRoute,
              public router : Router,
							private panelService: PanelServices
						) {
			$('body').addClass('body_img');
					this.verification_code = activatedRoute.snapshot.params.verification_code;
					
				 var admin_data = JSON.parse(localStorage.getItem("FeenuAdminData"));
					 console.log(admin_data);
					if (admin_data =='') {
					} else if (admin_data.user._id) {
					  this.router.navigate(["/dashboard"]);
					} 
					console.log(admin_data); 
					
			  }

  ngOnInit() {
 
	
  } 

	resetpassword(){
	
		var NAME_REGEXP = /^([a-zA-Z0-9\s]{6,})$/;
		//console.log(NAME_REGEXP.test(form.value.reset_pass));
		
		if(!this.new_password ){
			this.panelService.openSnackBar('Password is required', 'Ok','errorMsg');
			return;
		}
		if(!this.confirm_password ){
			this.panelService.openSnackBar('Confirm password is required', 'Ok','errorMsg');
			return;
		}
		if(this.confirm_password != this.new_password){
			this.panelService.openSnackBar('Password does not match', 'Ok','errorMsg');
			return;
		}
        this.loader = true;
		var normalizeSellerLoginPayload = {
			'new_password': this.new_password,
			'confirm_password': this.confirm_password,
			'verification_code': this.verification_code,
			'apiname': 'admin_reset_password'
			
		}

		this.panelService.comman_service_funcation(normalizeSellerLoginPayload)
			.subscribe( data => {
				this.loader = false;
				if(data.status == 201){
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					this.panelService.openSnackBar(data.message, 'OK','successMsg');
					this.router.navigate(['adminlogin']);
				}
				
		});
	}
  
}
