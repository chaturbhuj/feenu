import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { adminforgetpasswordComponent } from './forget_password.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [adminforgetpasswordComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([{
      path: '',
      component: adminforgetpasswordComponent
    }])
  ]
})
export class forgetpasswordModule { }
