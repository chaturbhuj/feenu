import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { adminresetpasswordComponent } from './reset_password.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [adminresetpasswordComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([{
      path: '',
      component: adminresetpasswordComponent
    }])
  ]
})
export class resetpasswordModule { }
