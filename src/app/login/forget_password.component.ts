import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MetaConfig, MetaService } from 'ng2-meta';
import { MatSnackBar, MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { PanelServices } from '../services/panel.service';

@Component({
  selector: 'app_forgetpassword',
  templateUrl: './forget_password.component.html',
  styleUrls: ['./forget_password.component.css']
})
export class adminforgetpasswordComponent implements OnInit{
  
  loader;
  admin_email = '';
  constructor(
              public snackBar : MatSnackBar,
              public matDialog : MatDialog,private metaService: MetaService,
              public router : Router,
							private panelService: PanelServices
						) {
				$('body').addClass('body_img');

			  }

  ngOnInit() {
   
	
	this.metaService.setTitle('Play Free Online Games on Feenu.com - Life is Fun! | Feenu');
	this.metaService.setTag('og:image','assets/images/favicon.png'); 
		
  } 

	forgetpassword(){
		
		console.log("this.admin_email");
		console.log(this.admin_email);
		
		if(!this.admin_email){
			this.panelService.openSnackBar(' Email is required', 'Ok','errorMsg');
			return;
		}else{
			var EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
			if((!EMAIL_REGEXP.test(this.admin_email))){
				this.panelService.openSnackBar('Email not valid', 'Ok','errorMsg');
				return;
			}
		}
        this.loader = true;
		var normalizeSellerLoginPayload = {
			'admin_email': this.admin_email,
			'apiname': 'admin_forget_password'
		}

		this.panelService.comman_service_funcation(normalizeSellerLoginPayload)
			.subscribe( data => {
				this.loader = false;
				if(data.status == 201){
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
					//this.router.navigate(['login']);
					this.admin_email = '';
				}
				
		});
	}
  
}
