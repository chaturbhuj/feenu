import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { MatSnackBar, MatDialog } from "@angular/material";
import { Router } from "@angular/router";
import { PanelServices } from "../services/panel.service";
import { ApiServices } from "../services/api.service";
@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  admin_email = "";
  admin_password = "";
  admin_id= '';
  loader;
  constructor(
    public snackBar: MatSnackBar,
    public matDialog: MatDialog,
    public router: Router,
    private panelService: PanelServices,
    private apiServices: ApiServices
  ) {
     var admin_data = JSON.parse(localStorage.getItem("FeenuAdminData"));
	 console.log(admin_data);
    if (admin_data) {
		
		  this.router.navigate(["/dashboard"]);
		
    }  
    console.log(admin_data); 
  }

  ngOnInit() {
    //$.blockUI();
  }

  onSubmit() {
    if (!this.admin_email) {
      this.panelService.openSnackBar("Email is required", "Ok", "errorMsg");
      return;
    } else {
      var EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
      if (!EMAIL_REGEXP.test(this.admin_email)) {
        this.panelService.openSnackBar("Email not valid", "Ok", "errorMsg");
        return;
      }
    }
    if (!this.admin_password) {
      this.panelService.openSnackBar("Password is required", "Ok", "errorMsg");
      return;
    }
    this.loader = true;
    var normalizeSellerPayload = {
      apiname: "admin/check_admin_auth",
      admin_email: this.admin_email,
      admin_password: this.admin_password
    };
    this.panelService
      .comman_service_funcation(normalizeSellerPayload)
      .subscribe(data => {
        this.loader = false;
        if (data.status == 201) {
          this.panelService.openSnackBar(data.message, "OK", "errorMsg");
        } else {
          this.panelService.openSnackBar(data.message, "OK", "successMsg");
          localStorage.setItem("VeggmiAdminData", JSON.stringify(data.data[0]));
          this.router.navigate(["dashboard"]);
        }
      });
  }

  onSiginin() {
    if (!this.admin_email) {
      this.panelService.openSnackBar("Email is required", "Ok", "errorMsg");
      return;
    } else {
      var EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
      if (!EMAIL_REGEXP.test(this.admin_email)) {
        this.panelService.openSnackBar("Email not valid", "Ok", "errorMsg");
        return;
      }
    }
    if (!this.admin_password) {
      this.panelService.openSnackBar("Password is required", "Ok", "errorMsg");
      return;
    }
   this.loader = true;
    var normalizeSellerPayload = {
      email: this.admin_email,
      password: this.admin_password
    };
    this.apiServices.login(normalizeSellerPayload)
	.subscribe(data => {
		 this.loader = false;
		 if (data.status == 200) {
			 console.log(data.data.user._id);
			 this.admin_id = data.data.user._id;
			 if(this.admin_id){
				  var normalizeSellerPayload = {
					   'admin_id': this.admin_id,
						'apiname': 'get_all_access_menu'
					}
					this.panelService.comman_service_funcation(normalizeSellerPayload)
						.subscribe( data => {
							console.log(data.data);
					localStorage.setItem('Feenumenu' , JSON.stringify(data.data))
					 this.router.navigate(["/dashboard"]); 
					});
			this.apiServices.openSnackBar(data.message, "OK", "successMsg");
			  localStorage.setItem('FeenuAdminData' , JSON.stringify(data.data));
			 
			 }
			 
		 }else{
			this.apiServices.openSnackBar(data.message, "OK", "errorMsg");
		 }
      });
	  
	  
  }
}
