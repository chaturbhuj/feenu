import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { sitemapComponent } from './sitemap.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [sitemapComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([{
      path: '',
      component: sitemapComponent
    }])
  ]
})
export class sitemapModule { }
