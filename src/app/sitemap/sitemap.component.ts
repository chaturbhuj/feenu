import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { MetaConfig, MetaService } from 'ng2-meta';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ChangeEvent } from '@ckeditor/ckeditor5-angular/ckeditor.component';

import { PanelServices } from '../services/panel.service';

declare var jquery: any;
declare var $ : any;
@Component({
  selector: 'app-sitemap',
  templateUrl: './sitemap.component.html',
  styleUrls: ['./sitemap.component.css']
})
export class sitemapComponent implements OnInit {
	public Editor = ClassicEditor;

google_plus='';
content='';
game_list='';
gamecategorylist='';
gametagslist='';
contactus_content_id='';
	 loader;
  constructor(public router: Router, private panelService: PanelServices,private metaService: MetaService,
  private location: Location) {
	$('body').removeClass('body_img');
  }


  ngOnInit() {
    this.game_list_menu();
    this.get_all_game_category_list();
    this.get_all_game_tags_list();
	$('.header').addClass('head1');
	$('.footer').addClass('foot');
	    
  }
  
	game_list_menu(){
	this.loader = true;
		var normalizeSellerPayload = {
			'apiname': 'game_list_menu'
			
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				this.game_list = data.data;
		});
	}
	
	get_all_game_category_list(){
		this.loader = true;
		var normalizeSellerPayload = {
			'apiname': 'list_game_category'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				this.gamecategorylist = data.data;
		});
	}
	
	get_all_game_tags_list(){
		this.loader = true;
		var normalizeSellerPayload = {
			'apiname': 'list_game_tags'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				this.gametagslist = data.data;
		});
	}
  
  
}
