import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { FormGroup, FormControl } from "@angular/forms";
import { PanelServices } from '../services/panel.service';

declare var jquery: any;
declare var $ : any;
@Component({
  selector: 'app-game_feedback',
  templateUrl: './game_feedback.component.html',
  styleUrls: ['./game_feedback.component.css']
})
export class game_feedbackComponent implements OnInit {
	loader;
	reportlist = '';
	userstatus = '';
	report_id = '';
	report_message = '';
	user_id = 0;
	constructor(public router: Router,
              private panelService: PanelServices) {
				$('body').removeClass('body_img');
			  }

	ngOnInit(){ 
		this.get_all_report_list(); 
	}
	
	get_all_report_list(){
	    this.loader = true;
		var normalizeSellerPayload = {
			'apiname': 'list_game_report'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				this.reportlist = data.data;
				console.log(this.reportlist);
				
		});
	}
	
	delete_user(user_id){
		this.loader = true;
		var normalizeSellerPayload = {
			'apiname': 'delete_user',
			'user_id': user_id
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
			this.loader = false;
			 this.get_all_report_list(); 
				if(data.status == 201){
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					this.panelService.openSnackBar(data.message, 'OK','successMsg');
					
				}
		});
	}
	
	onSubmit(){
		
		this.loader = true;
		var normalizeSellerPayload = {
			'report_id': this.report_id,
			'report_message': this.report_message,
			'apiname': 'update_report_message'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
			this.loader = false;
			 this.get_all_report_list(); 
				if(data.status == 201){
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					this.panelService.openSnackBar(data.message, 'OK','successMsg');
					
				}
				$('.close').trigger('click');
		});
  }
	
	edit_report(report_id, message){
		this.report_id = report_id;
		this.report_message = message;
	}
  
  remove_feedback(report_id){
	  
	  	var val = confirm("Do you want to delete");
		if(val == true){
		this.loader = true;
		var normalizeSellerPayload = {
			'apiname': 'remove_game_report',
			'report_id': report_id
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
			this.loader = false;
				if(data.status == 201){
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					this.ngOnInit();  
					this.panelService.openSnackBar(data.message, 'OK','successMsg');
				}
		});
			return true;
		}else{
			return false;
		}
	}
  
  
}
