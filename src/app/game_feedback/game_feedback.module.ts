import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { game_feedbackComponent } from './game_feedback.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [game_feedbackComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([{
      path: '',
      component: game_feedbackComponent
    }])
  ]
})
export class game_feedbackModule { }
