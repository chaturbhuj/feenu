import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { staffprofileComponent } from './staffprofile.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [staffprofileComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([{
      path: '',
      component: staffprofileComponent
    }])
  ]
})
export class staffprofileModule { }
