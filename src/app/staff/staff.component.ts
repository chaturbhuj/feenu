import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ApiServices } from "../services/api.service";
import { NgxPermissionsService } from "ngx-permissions";
import "rxjs/add/observable/merge";
declare var jquery: any;
declare var $: any;

@Component({
  selector: "app-staff",
  templateUrl: "./staff.component.html",
  styleUrls: ["./staff.component.css"]
})
export class StaffComponent implements OnInit {
  loader;
  name = "";
  email = "";
  staffId: any;
  staffList = [];
  editStaffFlag = false;
  systemAccessList = [];
  accessNameList = [];

  constructor(
    public router: Router,
    private apiServices: ApiServices,
    private permissionsService: NgxPermissionsService
  ) {
    $("body").removeClass("body_img");
  }

  ngOnInit() {
    this.getStaffList();
  }

  addAddStaffDialog() {
    this.name = "";
    this.email = "";
    this.editStaffFlag = false;
  }

  editStaff(item) {
    this.name = item.name;
    this.email = item.email;
    this.staffId = item._id;
    this.editStaffFlag = true;
  }

  saveStaff() {
    if (this.editStaffFlag == true) {
      if (!this.name) {
        this.apiServices.openSnackBar(
          "Menu category name is required",
          "Ok",
          "errorMsg"
        );
        return;
      }
      this.loader = true;
      var normalizeSellerPayload1 = {
        name: this.name,
        _id: this.staffId,
        email: this.email,
        role: "user",
        accessLevel: this.accessNameList.toString()
      };
      this.apiServices.editStaff(normalizeSellerPayload1).subscribe(data => {
        this.loader = false;
        this.getStaffList();
      },
      err => {
        this.loader = false;
        this.apiServices.openSnackBar(err, "OK", "errorMsg");
      });
      $("#browseNewCategory").modal("hide");
      $("#aclDialog").modal("hide");
    } else {
      if (!this.name) {
        this.apiServices.openSnackBar(
          "Menu category name is required",
          "Ok",
          "errorMsg"
        );
        return;
      }
      this.loader = true;
      var normalizeSellerPayload = {
        name: this.name,
        email: this.email,
        password: "12345",
        role: "user"
      };
      this.apiServices.addStaff(normalizeSellerPayload).subscribe(data => {
        this.loader = false;
        this.getStaffList();
        this.apiServices.openSnackBar(data.message, "OK", "successMsg");
      },
      err => {
        this.loader = false;
        this.apiServices.openSnackBar(err, "OK", "errorMsg");
      });
      $("#browseNewCategory").modal("hide");
    }
  }

  getStaffList() {
    this.staffList = [];
    this.apiServices.getStaff().subscribe(data => {
      this.loader = false;
      data.docs.forEach(element => {
        this.staffList.push(element);
      });
    },
	err => {
	  this.loader = false;
	  this.apiServices.openSnackBar(err, "OK", "errorMsg");
	});
  }

  getSystemAccessList() {
    this.systemAccessList = [];
    this.apiServices.getSystemAccess().subscribe(data => {
      this.loader = false;
      data.forEach(element => {
        element["checked"] = false;
        this.systemAccessList.push(element);
      },
      err => {
        this.loader = false;
        this.apiServices.openSnackBar(err, "OK", "errorMsg");
      });
    });
  }

  openStaffSetting(item) {
    this.name = item.name;
    this.email = item.email;
    this.staffId = item._id;
    this.editStaffFlag = true;
    this.editStaffFlag = true;
    this.loader = true;
    this.getSystemAccessList();
  }

  changeCheckbox(tags, i) {
    this.accessNameList = [];
    if (tags) {
      this.systemAccessList[i].checked = !this.systemAccessList[i].checked;
    }

    this.systemAccessList.forEach(element => {
      if (element.checked == true) {
        this.accessNameList.push(element.name);
      }
    });
  }
  
 
}
