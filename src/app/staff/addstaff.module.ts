import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { addstaffComponent } from './addstaff.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [addstaffComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([{
      path: '',
      component: addstaffComponent
    }])
  ]
})
export class addstaffModule { }
