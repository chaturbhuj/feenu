import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { stafflistComponent } from './stafflist.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [stafflistComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([{
      path: '',
      component: stafflistComponent
    }])
  ]
})
export class stafflistModule { }
