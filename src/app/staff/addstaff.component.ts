import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FormGroup, FormControl } from "@angular/forms";
import { PanelServices } from '../services/panel.service';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ChangeEvent } from '@ckeditor/ckeditor5-angular/ckeditor.component';
import { ImageCroppedEvent } from 'ngx-image-cropper';
declare var jquery: any;
declare var $ : any;
@Component({
  selector: 'app-addstaff',
  templateUrl: './addstaff.component.html',
  styleUrls: ['./addstaff.component.css']
})
export class addstaffComponent implements OnInit {
	loader;
	public Editor = ClassicEditor;
	 editorData = '';
	imageChangedEvent: any = '';
	croppedImage: any;
	staff_name = '';
	blog_image = '';
	blog_img_src = '';
	created_date = '';
	created_time = '';
	staff_email = '';
	staff_password='';
	staff_id = 0;
	blog_id = 0;
	staff_verifiy = '';
	staff_city = '';
	staff_phone_number = '';
	constructor(public router: Router,private activatedRoute: ActivatedRoute,
              private panelService: PanelServices) {
					$('body').removeClass('body_img');
					this.staff_id = activatedRoute.snapshot.params.staff_id;
					
					 
					
			}
 
	ngOnInit(){ 
		var d = new Date();
		var month = d.getMonth()+1;
		var day = d.getDate();

		this.created_date = d.getFullYear() + '-' + (month<10 ? '0' : '') + month + '-' + (day<10 ? '0' : '') + day;
		this.created_time = (d.getHours()<10 ? '0' : '') + d.getHours() + ":" + (d.getMinutes()<10 ? '0' : '') + d.getMinutes() + ":" + (d.getSeconds()<10 ? '0' : '') + d.getSeconds();
		var admin_data = JSON.parse(localStorage.getItem("FeenuAdminData"));
		
		if(this.staff_id == 0){
		
		}else{
			this.get_staff_by_id();
			if(admin_data.user._id == this.staff_id){
				$(".verify_account").attr('disabled','disabled');
			}else{
				$(".verify_account").removeAttr('disabled');
			}
		}
		
     console.log(this.staff_id);
     console.log(admin_data.user._id);
   		
	}
	
	get_staff_by_id(){
	this.loader = true;
		var normalizeSellerPayload = {
			'apiname': 'by_id_staff',
			'staff_id': this.staff_id
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
			this.loader = false;
			console.log(data.data);
			this.staff_name = data.data.staff_name;
			this.staff_email = data.data.staff_email;
			this.staff_phone_number = data.data.staff_phone_number;
			this.staff_password = data.data.staff_password;
			this.staff_verifiy = data.data.staff_verifiy;
			this.staff_city = data.data.staff_city;
				
		});
	}

  
	
	onSubmit(){
		if(!this.staff_name ){
		  this.panelService.openSnackBar('Staff Name is required', 'Ok','errorMsg');
		  return;
		}
		
		if(!this.staff_email ){
		  this.panelService.openSnackBar('Staff Email is required', 'Ok','errorMsg');
		  return;
		}
		
		if(!this.staff_password){
		  this.panelService.openSnackBar('Password is required', 'Ok','errorMsg');
		  return;
		}
		if(!this.staff_verifiy){
		  this.panelService.openSnackBar('This is required', 'Ok','errorMsg');
		  return;
		}
		if(!this.staff_city){
		  this.panelService.openSnackBar('Staff city is required', 'Ok','errorMsg');
		  return;
		}
		if(!this.staff_phone_number){
		  this.panelService.openSnackBar('Staff Phone Number is required', 'Ok','errorMsg');
		  return;
		}
		this.loader = true;
		var normalizeSellerPayload = {
			'staff_id': this.staff_id,
			'staff_name': this.staff_name,
			'staff_email': this.staff_email,
			'staff_password': this.staff_password,
			'staff_verifiy': this.staff_verifiy,
			'staff_city': this.staff_city,
			'staff_phone_number': this.staff_phone_number,
			'apiname': 'add_staff'
		}
		
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				if(data.status == 201){
				    this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					this.panelService.openSnackBar(data.message, 'OK','successMsg');
					this.router.navigate(['staff-list']);
					
				}
		});
		
  }
  fileChangeEvent(event: any): void {
		this.imageChangedEvent = event;
	}

	imageCropped(event: ImageCroppedEvent) {
			this.croppedImage = event.base64;
	}

	onSaveCropImage(){
		const normalizeSellerPayload = {
			'image_base_64': this.croppedImage,
			'apiname': 'googlecoludImagebase64'
		}
		$('#close_register_modal').trigger('click');
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				
			this.blog_image = (data.myImage);
			this.blog_img_src = (data.myImage);
			$('.mycropimageuploadinput').val('');
			$('.source-image').attr('src','');
			this.croppedImage = '';
			this.fileChangeEvent('ok');
		});
	}
  
  
}
