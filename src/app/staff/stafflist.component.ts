import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { FormGroup, FormControl } from "@angular/forms";
import { PanelServices } from '../services/panel.service';

declare var jquery: any;
declare var $ : any;
@Component({
  selector: 'app-stafflist',
  templateUrl: './stafflist.component.html',
  styleUrls: ['./stafflist.component.css']
})
export class stafflistComponent implements OnInit {
	loader;
	stafflist = '';
	user_access_list = '';
	submodulelist = '';
	accesslist = '';
	emp_name = '';
	emp_id = 0;
	constructor(public router: Router,
              private panelService: PanelServices) {
				$('body').removeClass('body_img');
			  }

	ngOnInit(){ 
		this.get_all_staff_list(); 
		this.get_all_submodule_list(); 
		this.get_all_access_list(); 
	}
	
	get_all_staff_list(){
		this.loader = true;
		var normalizeSellerPayload = {
			'apiname': 'list_staff'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				this.stafflist = data.data;
				console.log(this.stafflist);
		});
	}
	
	get_all_submodule_list(){
	this.loader = true;
		var normalizeSellerPayload = {
			'apiname': 'list_submodule'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
			this.loader = false;
				this.submodulelist = data.data;
				console.log(this.submodulelist);
				console.log(this.accesslist);
				
				/* $.each(this.submodulelist,function(i,val){
					var result=$.inArray(val,this.accesslist);
					if(result!=-1)
					alert(result); 
					}) */
				//$(".myCheckbox").prop('checked', true);
		});
	}
	
	get_all_access_list(){
	this.loader = true;
		var normalizeSellerPayload = {
			'apiname': 'list_access'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
			this.loader = false;
				this.accesslist = data.data;
				console.log(this.accesslist);
		});
	}
	
	removestaff(staff_id){
		this.loader = true;
		var normalizeSellerPayload = {
			'apiname': 'delete_staff',
			'staff_id': staff_id
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
			this.loader = false;
				this.get_all_staff_list(); 
				if(data.status == 201){
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					this.panelService.openSnackBar(data.message, 'OK','successMsg');
				}
		});
	}
	
	emp_access(user_id, name){
		this.emp_id = user_id;
		this.emp_name = name;
		
	    this.loader = true;
		var normalizeSellerPayload = {
			'user_id': this.emp_id,
			'apiname': 'access_list_by_id'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				this.user_access_list = data.data;
				 $.each(this.user_access_list,function(i,val){
					console.log(val.submodule_id);
					$('input[value="'+val.submodule_id+'"]').prop('checked', true);
				})
				console.log(this.user_access_list); 
		});
	
	}
	
	
	
	accessSubmit(){
		//console.log(this.emp_id);
		//this.loader = true;
		var access_info = [];
		  $.each($("input[name='case[]']:checked"), function() {
			//console.log("yes");
			//console.log($(this).text());
			//console.log($(this).parents('tr').closest("td").find('.submodule_id'))
			//console.log($(this).find('.accessdata').closest("td").closest("td").find('.submodule_id')); 
			console.log($(this).parents('tr:eq(0)').find('td:eq(1)').find('.submodule_id').val()); 
			console.log($(this).parents('tr:eq(0)').find('td:eq(2)').text()); 
			console.log($(this).parents('tr:eq(0)').find('td:eq(3)').find('.module_id').val()); 
			console.log($(this).parents('tr:eq(0)').find('td:eq(4)').find('.submodule_link').val()); 
			console.log($(this).parents('tr:eq(0)').find('td:eq(5)').text()); 
			access_info.push({
							submodule_id:$(this).parents('tr:eq(0)').find('td:eq(1)').find('.submodule_id').val(),
							module_name :$(this).parents('tr:eq(0)').find('td:eq(2)').text(),
							module_id :$(this).parents('tr:eq(0)').find('td:eq(3)').find('.module_id').val(),
							submodule_link :$(this).parents('tr:eq(0)').find('td:eq(4)').find('.submodule_link').val(),
							submodule_name :$(this).parents('tr:eq(0)').find('td:eq(5)').text(),
						}); 
		});	
		
	
		
	console.log(access_info);
		
		var normalizeSellerPayload = {
			'access_info': access_info,
			'staff_id': this.emp_id,
			'apiname': 'add_access'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
			//this.loader = false;
			
				if(data.status == 201){
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					this.panelService.openSnackBar(data.message, 'OK','successMsg');
					$('#browseAccessModal .close').trigger('click');
					$('#browseAccessModal').removeClass('show');
				}
				
		});
	}
  
  
}
