import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FormGroup, FormControl } from "@angular/forms";
import { PanelServices } from '../services/panel.service';
import { ImageCroppedEvent } from 'ngx-image-cropper';
declare var jquery: any;
declare var $ : any;
@Component({
  selector: 'app-staffprofile',
  templateUrl: './staffprofile.component.html',
  styleUrls: ['./staffprofile.component.css']
})
export class staffprofileComponent implements OnInit {
  	loader;
	full_name = '';
	staff_name = '';
	admin_email = '';
	staff_email = '';
	staff_city = '';
	staff_phone_number = '';
	imageChangedEvent: any = '';
	croppedImage: any;
	contact = '';
	admin_id = 0;
	veggmi_admin_id = '';
	admin_img = '';
	admin_img_src = '';
	first_name = '';
	google_plus_link = '';
	twitter_link = '';
	fb_link = '';
	last_name = '';
	gender = '';
	email_id ='';
	constructor(public router: Router,private activatedRoute: ActivatedRoute,
              private panelService: PanelServices) {
		
				var admin_data = JSON.parse(localStorage.getItem("FeenuAdminData"));
				
				    if(admin_data){
					  console.log(admin_data.user._id);
					  this.admin_id = admin_data.user._id;
					}else{
						
						
					}
				
				console.log(admin_data);
				
			}
 
	ngOnInit(){ 
		   if(this.admin_id == 0){   
			
		   }else{
			  this.get_profile_by_id(); 
		   }
	}
	
	get_profile_by_id(){
		this.loader = true;
		var normalizeSellerPayload = {
			'apiname': 'get_admin_profile',
			'admin_id': this.admin_id
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
			this.loader = false;
				 this.staff_name = data.data.staff_name;
				this.staff_email = data.data.staff_email;
				this.staff_city = data.data.staff_city;
				this.staff_phone_number = data.data.staff_phone_number;
				console.log(data.data);
		});
	}

  /* 
	block_account() {
		var val = confirm("Do you want to delete");
		if(val == true){
		this.loader = true;
		var normalizeSellerPayload = {
			'userstatus': 'remove',
			'user_id': this.user_id,
			'apiname': 'block_account'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				if(data.status == 201){
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					localStorage.setItem('userData' , JSON.stringify(''));
					this.panelService.openSnackBar(data.message, 'OK','successMsg');
					this.router.navigate(['home']);
				}
				
		});
					return true;
		}else{
			return false;
		}
	} */
	
	
	submitprofile(){
	//var contactno = /^([0-9\s]{10,})$/;
		if(!this.staff_name){
		  this.panelService.openSnackBar('Staff name is required', 'Ok','errorMsg');
		  return;
		}		
		if(!this.staff_email){
		  this.panelService.openSnackBar('Staff email is required', 'Ok','errorMsg');
		  return;
		}
		if(!this.staff_city){
			  this.panelService.openSnackBar('staff city is required', 'Ok','errorMsg');
			  return;
		 }		
		if(!this.staff_phone_number){
		  this.panelService.openSnackBar('staff phone number is required', 'Ok','errorMsg');
		  return;
		}
      
		this.loader = true;
		var normalizeSellerPayload = {
		    'admin_id': this.admin_id,
			'staff_name': this.staff_name,
			'staff_email': this.staff_email,
			'staff_city': this.staff_city,
			'staff_phone_number': this.staff_phone_number,
			'apiname': 'update_admin_profile'
		}
		
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				if(data.status == 201){
				
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					this.panelService.openSnackBar(data.message, 'OK','successMsg');
					
					this.get_profile_by_id();
				}
		});
		
  }
    fileChangeEvent(event: any): void {
		this.imageChangedEvent = event;
	}

	imageCropped(event: ImageCroppedEvent) {
			this.croppedImage = event.base64;
	}

	onSaveCropImage(){
		const normalizeSellerPayload = {
			'image_base_64': this.croppedImage,
			'apiname': 'googlecoludImagebase64'
		}
		$('#close_register_modal').trigger('click');
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				
			this.admin_img = (data.myImage);
			this.admin_img_src = (data.myImage);
			$('.mycropimageuploadinput').val('');
			$('.source-image').attr('src','');
			this.croppedImage = '';
			this.fileChangeEvent('ok');
		});
		 $('.close').trigger('click');	
	}
  
}
