import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FormGroup, FormControl } from "@angular/forms";
import { PanelServices } from '../services/panel.service';

declare var jquery: any;
declare var $ : any;
@Component({
  selector: 'app-module',
  templateUrl: './module.component.html',
  styleUrls: ['./module.component.css']
})
export class moduleComponent implements OnInit {
    module_name = '';
	created_date = '';
	created_time = '';
	module_id=0;
	modulelist= '';
	loader;
	constructor(public router: Router,private activatedRoute: ActivatedRoute,
              private panelService: PanelServices) {
				$('body').removeClass('body_img');
			}
 
	ngOnInit(){ 
		var d = new Date();
		var month = d.getMonth()+1;
		var day = d.getDate();

		this.created_date = d.getFullYear() + '-' + (month<10 ? '0' : '') + month + '-' + (day<10 ? '0' : '') + day;
		this.created_time = (d.getHours()<10 ? '0' : '') + d.getHours() + ":" + (d.getMinutes()<10 ? '0' : '') + d.getMinutes() + ":" + (d.getSeconds()<10 ? '0' : '') + d.getSeconds();
		
		
		this.get_all_module_list(); 
	}
	
	get_all_module_list(){
		this.loader = true;
		var normalizeSellerPayload = {
			'apiname': 'list_module'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
		.subscribe( data => {
			this.loader = false; 
				this.modulelist = data.data;
				console.log(this.modulelist);
		});
	}
	
	
	onSubmit(){
		
		
		if(!this.module_name ){
		  this.panelService.openSnackBar('Module name is required', 'Ok','errorMsg');
		  return;
		}
		this.loader = true;
		var normalizeSellerPayload = {
			'id': this.module_id,
			'modulename': this.module_name,
			'apiname': 'add_module'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				if(data.status == 201){
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					this.panelService.openSnackBar(data.message, 'OK','successMsg');
					this.get_all_module_list(); 
					$('#browseNewCategory .close').trigger('click');
					$('#browseNewCategory').removeClass('show');
				}
		});
		//$("#browseNewCategory").modal('hide');
  }
  
  removecategory(module_id){
	  	var val = confirm("Do you want to delete");
		if(val == true){
    this.loader = true;
		var normalizeSellerPayload = {
			'apiname': 'delete_module',
			'id': module_id
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
			this.loader = false;
				this.get_all_module_list(); 
				if(data.status == 201){
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					this.panelService.openSnackBar(data.message, 'OK','successMsg');
				}
		});
			return true;
		}else{
			return false;
		}
	}
	
	editmodule(id, modulename){
		this.module_id = id;
		this.module_name = modulename;
		console.log(this.module_id);
	}
  
	addNewCategory(){
		this.module_id = 0;
		this.module_name = "";
	}
  
}
