import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { submoduleComponent } from './submodule.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [submoduleComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([{
      path: '',
      component: submoduleComponent
    }])
  ]
})
export class submoduleModule { }
