import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FormGroup, FormControl } from "@angular/forms";
import { PanelServices } from '../services/panel.service';
import * as _ from 'lodash';
declare var jquery: any;
declare var $ : any;
@Component({
  selector: 'app-submodule',
  templateUrl: './submodule.component.html',
  styleUrls: ['./submodule.component.css']
})
export class submoduleComponent implements OnInit {
    module_name = '';
    submodule_name = '';
    modulelist = '';
    submodulelist = '';
	created_date = '';
	created_time = '';
	submodule_link = '';
	menu_subcategory_name = '';
	menu_category_name = '';
	module_id=0;
	submodule_id=0;
	menu_category_id=0;
	loader;
	constructor(public router: Router,private activatedRoute: ActivatedRoute,
              private panelService: PanelServices) {
				$('body').removeClass('body_img');
			}
 
	ngOnInit(){ 
		var d = new Date();
		var month = d.getMonth()+1;
		var day = d.getDate();

		this.created_date = d.getFullYear() + '-' + (month<10 ? '0' : '') + month + '-' + (day<10 ? '0' : '') + day;
		this.created_time = (d.getHours()<10 ? '0' : '') + d.getHours() + ":" + (d.getMinutes()<10 ? '0' : '') + d.getMinutes() + ":" + (d.getSeconds()<10 ? '0' : '') + d.getSeconds();
		
		
		this.get_all_submodule_list(); 
		this.get_all_module_list(); 
		
	}
	
	get_all_module_list(){
		this.loader = true;
		var normalizeSellerPayload = {
			'apiname': 'list_module'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
		
			.subscribe( data => {
				this.loader = false;
				this.modulelist = data.data;
		});
	}
	
	get_all_submodule_list(){
	this.loader = true;
		var normalizeSellerPayload = {
			'apiname': 'list_submodule'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
			this.loader = false;
				this.submodulelist = data.data;
				console.log(this.submodulelist);
		});
	}
	
	onmoduleChange($event) {
		this.module_id = (this.module_id);
        this.module_name = _.filter(this.modulelist, {
            "_id": (this.module_id)
        })[0].modulename;
		console.log(this.module_name);
		console.log(this.module_id);
	}
	
	onSubmit(){
	
		
		if(!this.module_id ){
		  this.panelService.openSnackBar('Module is required', 'Ok','errorMsg');
		  return;
		}
		if(!this.submodule_name ){
		  this.panelService.openSnackBar('Sub module name is required', 'Ok','errorMsg');
		  return;
		}
		if(!this.submodule_link ){
		  this.panelService.openSnackBar('submodule link is required', 'Ok','errorMsg');
		  return;
		}
		
		this.loader = true;
		var normalizeSellerPayload = {
			'submodule_id': this.submodule_id,
			'submodule_name': this.submodule_name,
			'submodule_link': this.submodule_link,
			'module_id': this.module_id,
			'module_name': this.module_name,
			'apiname': 'add_submodule'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
			this.loader = false;
				if(data.status == 201){
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					this.panelService.openSnackBar(data.message, 'OK','successMsg');
					this.get_all_submodule_list(); 
					$('#browseNewCategory .close').trigger('click');
					$('#browseNewCategory').removeClass('show');
				}
		});
		//$("#browseNewCategory").modal('hide');
  }
  
  removesubmodule(submodule_id){
	    	var val = confirm("Do you want to delete");
		if(val == true){
			this.loader = true;
		var normalizeSellerPayload = {
			'apiname': 'delete_sub_module',
			'id': submodule_id
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
			this.loader = false;
				this.get_all_submodule_list(); 
				if(data.status == 201){
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					this.panelService.openSnackBar(data.message, 'OK','successMsg');
				}
		});
			return true;
		}else{
			return false; 
		}
	}
	
	editsubmodule(submodule_id, submodule_name,module_id,submodule_link){
		this.submodule_id = submodule_id;
		this.submodule_name = submodule_name;
		this.submodule_link = submodule_link;
		this.module_id = module_id;
		$('#menu_category_id').val(module_id);
	}
	
	addNewCategory(){
		this.module_id = 0;
		this.submodule_name = "";
	}
  
}
