import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { tagsComponent } from './tags.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [tagsComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([{
      path: '',
      component: tagsComponent
    }])
  ]
})
export class tagsModule { }
