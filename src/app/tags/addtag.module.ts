import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { addtagComponent } from './addtag.component';
import { RouterModule } from '@angular/router';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [addtagComponent],
  imports: [
    CommonModule,CKEditorModule,
    SharedModule,
    RouterModule.forChild([{
      path: '',
      component: addtagComponent
    }])
  ]
})
export class addtagModule { }
