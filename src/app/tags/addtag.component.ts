import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FormGroup, FormControl } from "@angular/forms";
import { PanelServices } from '../services/panel.service';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ChangeEvent } from '@ckeditor/ckeditor5-angular/ckeditor.component';

declare var jquery: any;
declare var $ : any;
@Component({
  selector: 'app-addtag',
  templateUrl: './addtag.component.html',
  styleUrls: ['./addtag.component.css']
})
export class addtagComponent implements OnInit {
	public Editor = ClassicEditor;
    game_category_name = '';
	created_date = '';
	created_time = '';
	category_slug = '';
	content = '';
	category_meta_keywords = '';
	category_meta_description = '';
	game_category_id=0;
	gamecategorylist= '';
	tag_id;
	 editorData = '';
	 tag_name = '';
	tag_slug = '';
	tag_meta_keywords = '';
	tag_meta_description = '';
	loader; 
	constructor(public router: Router,private activatedRoute: ActivatedRoute,
              private panelService: PanelServices) {
				$('body').removeClass('body_img');
					this.tag_id = activatedRoute.snapshot.params.tag_id;
					if(this.tag_id !=0){
						this.get_tag_by_id();
					}
			}
 
	ngOnInit(){ 
		var d = new Date();
		var month = d.getMonth()+1;
		var day = d.getDate();

		this.created_date = d.getFullYear() + '-' + (month<10 ? '0' : '') + month + '-' + (day<10 ? '0' : '') + day;
		this.created_time = (d.getHours()<10 ? '0' : '') + d.getHours() + ":" + (d.getMinutes()<10 ? '0' : '') + d.getMinutes() + ":" + (d.getSeconds()<10 ? '0' : '') + d.getSeconds();
		
		
		
			 $('.tag_name').keyup(function(){  
				 this.tag_slug =  $(this).val().toLowerCase().replace(/ /g, '-');
				console.log(this.tag_slug);
				$('.tag_slug').val(this.tag_slug);  
			});
		 
		
	}

	
	get_tag_by_id(){
		this.loader = true;
		var normalizeSellerPayload = {
			'tag_id': this.tag_id,
			'apiname': 'get_tag_by_id'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				this.tag_name = data.data[0].tag_name;
				this.tag_slug = data.data[0].tag_slug;
				this.tag_meta_description = data.data[0].tag_meta_description;
				this.content = (data.data[0].content);
				console.log(this.content);
		});
	}
	public onChangeckeditor( { editor }: ChangeEvent ) {
		this.content = editor.getData();
	}
	
	onSubmit(){
		this.tag_slug = $('.tag_slug').val();
		
		if(!this.tag_name ){
		  this.panelService.openSnackBar('Tag name is required', 'Ok','errorMsg');
		  return;
		}
		if(!this.tag_slug ){
		  this.panelService.openSnackBar('Tag slug is required', 'Ok','errorMsg');
		  return;
		}
		/* if(!this.tag_meta_keywords ){
		  this.panelService.openSnackBar('Tag keywords is required', 'Ok','errorMsg');
		  return;
		} */
		if(!this.tag_meta_description ){
		  this.panelService.openSnackBar('Tag description is required', 'Ok','errorMsg');
		  return;
		}
		this.loader = true;
		var normalizeSellerPayload = {
			'tag_id': this.tag_id,
			'tag_name': this.tag_name,
			'tag_slug': this.tag_slug,
			'tag_meta_keywords': this.tag_meta_keywords,
			'tag_meta_description': this.tag_meta_description,
			'content': this.content,
			'created_date': this.created_date,
			'created_time': this.created_time,
			'apiname': 'add_tag'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				if(data.status == 201){
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					$("#browseNewCategory").modal('hide');
					$("#browseedit_tag").modal('hide');
					$('.close_btn').trigger('click');
					this.panelService.openSnackBar(data.message, 'OK','successMsg');
					
                   this.tag_name = '';					
                   this.tag_slug = '';					
                   this.tag_meta_keywords = '';					
                   this.tag_meta_description = '';	
                   this.router.navigate(['tags']);				   
				}
		});
		
  }
 

  
}
