import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FormGroup, FormControl } from "@angular/forms";
import { PanelServices } from '../services/panel.service';

declare var jquery: any;
declare var $ : any;
@Component({
  selector: 'app-tags',
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.css']
})
export class tagsComponent implements OnInit {
    game_category_name = '';
	created_date = '';
	created_time = '';
	category_slug = '';
	category_meta_keywords = '';
	category_meta_description = '';
	game_category_id=0;
	tag_id = 0;
	tag_name = '';
	tag_slug = '';
	tag_meta_keywords = '';
	tag_meta_description = '';
	gamecategorylist= '';
	taglist= '';
	tag_id_='';
	loader;
	constructor(public router: Router,private activatedRoute: ActivatedRoute,
              private panelService: PanelServices) {
				$('body').removeClass('body_img');
			}
 
	ngOnInit(){ 
		var d = new Date();
		var month = d.getMonth()+1;
		var day = d.getDate();

		this.created_date = d.getFullYear() + '-' + (month<10 ? '0' : '') + month + '-' + (day<10 ? '0' : '') + day;
		this.created_time = (d.getHours()<10 ? '0' : '') + d.getHours() + ":" + (d.getMinutes()<10 ? '0' : '') + d.getMinutes() + ":" + (d.getSeconds()<10 ? '0' : '') + d.getSeconds();
		
		
		this.get_all_tag_list(); 
		 
		 console.log(this.tag_id);
			if(this.tag_id == 0){
			 $('.tag_name').keyup(function(){  
				 this.tag_slug =  $(this).val().toLowerCase().replace(/ /g, '-');
				console.log(this.tag_slug);
				$('.tag_slug').val(this.tag_slug);  
			});
		  }else{
			  console.log('sdfsdf');
		  }
		
	}
	
	/* tag_slug_click(){
		if(this.tag_name){
	     this.tag_slug =  this.tag_name.toLowerCase().replace(' ', '-').replace(/[0-9]/g, '');
		} 
	} */
	
	get_all_tag_list(){
		this.loader = true;
		var normalizeSellerPayload = {
			'apiname': 'list_tags'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				this.taglist = data.data;
				console.log(this.taglist);
		});
	}
	
	
	onSubmit(){
		this.tag_slug = $('.tag_slug').val();
		
		if(!this.tag_name ){
		  this.panelService.openSnackBar('Tag name is required', 'Ok','errorMsg');
		  return;
		}
		if(!this.tag_slug ){
		  this.panelService.openSnackBar('Tag slug is required', 'Ok','errorMsg');
		  return;
		}
		/* if(!this.tag_meta_keywords ){
		  this.panelService.openSnackBar('Tag keywords is required', 'Ok','errorMsg');
		  return;
		} */
		if(!this.tag_meta_description ){
		  this.panelService.openSnackBar('Tag description is required', 'Ok','errorMsg');
		  return;
		}
		this.loader = true;
		var normalizeSellerPayload = {
			'tag_id': this.tag_id,
			'tag_name': this.tag_name,
			'tag_slug': this.tag_slug,
			'tag_meta_keywords': this.tag_meta_keywords,
			'tag_meta_description': this.tag_meta_description,
			'created_date': this.created_date,
			'created_time': this.created_time,
			'apiname': 'add_tag'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				if(data.status == 201){
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					$("#browseNewCategory").modal('hide');
					$("#browseedit_tag").modal('hide');
					$('.close_btn').trigger('click');
					this.panelService.openSnackBar(data.message, 'OK','successMsg');
					
                   this.tag_name = '';					
                   this.tag_slug = '';					
                   this.tag_meta_keywords = '';					
                   this.tag_meta_description = '';	
                    this.get_all_tag_list(); 				   
				}
		});
		
  }
  
  remove_tags(tag_id){
	  	var val = confirm("Do you want to delete");
		if(val == true){
		this.loader = true;
		var normalizeSellerPayload = {
			'apiname': 'delete_tags',
			'tag_id': tag_id
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
			this.loader = false;
				if(data.status == 201){
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					$('.close_btn').trigger('click');
					this.ngOnInit();  
					this.panelService.openSnackBar(data.message, 'OK','successMsg');
				}
		});
			return true;
		}else{
			return false;
		}
	}
	
	edittag(tag_id, tag_name,tag_slug,tag_meta_keywords,tag_meta_description){
		this.tag_id = tag_id;
		this.tag_name = tag_name;
		this.tag_slug = tag_slug;
		this.tag_meta_keywords = tag_meta_keywords;
		this.tag_meta_description = tag_meta_description;
		
		this.ngOnInit();
	}
  
	addNewCategory(){
		
		this.tag_id = 0;
		this.tag_name = '';
		this.tag_slug = '';
		this.tag_meta_keywords = '';
		this.tag_meta_description = '';
	}
  
}
