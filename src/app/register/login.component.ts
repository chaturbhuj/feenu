import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { AuthService } from "angular2-social-login";
import { MetaConfig, MetaService } from 'ng2-meta';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ChangeEvent } from '@ckeditor/ckeditor5-angular/ckeditor.component';

import { PanelServices } from '../services/panel.service';

declare var jquery: any;
declare var $ : any;
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class loginComponent implements OnInit {
	public Editor = ClassicEditor;
contact_name='';
contact_email='';
contact_number='';
contact_subject='';
contact_message='';
contact_url='';
contact_facebook='';
contact_twitter='';
contact_address='';
contact_youtube='';
contact_pinterest='';
google_plus='';
content_title='';
content='';
contactus_content_id='';
created_date='';
created_time='';
user_email='';
user_password='';
auth_logn: any = '';
	 loader;
  constructor(public router: Router,public _auth: AuthService, private panelService: PanelServices,private metaService: MetaService,
  private location: Location) {
	$('body').removeClass('body_img');
  }

 
  ngOnInit() {
	     var d = new Date();
		var month = d.getMonth()+1;
		var day = d.getDate();

		this.created_date = d.getFullYear() + '-' + (month<10 ? '0' : '') + month + '-' + (day<10 ? '0' : '') + day;
		this.created_time = (d.getHours()<10 ? '0' : '') + d.getHours() + ":" + (d.getMinutes()<10 ? '0' : '') + d.getMinutes() + ":" + (d.getSeconds()<10 ? '0' : '') + d.getSeconds();
	
    this.get_public_contactus_info();
	if(!this.panelService.checkIfUserOnMobile()){
			$('.navbar').addClass('head1');
	        $('.footer').addClass('foot');
		}else{
			$('.header').addClass('head1');
		}
  }
  
	get_public_contactus_info(){
	this.loader = true;
		var normalizeSellerPayload = {
			'apiname': 'by_id_public_content',
			'content_id': '5cb80d05ce79b806542e1aab'
			
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				this.content = data.data.content;
				this.content_title = data.data.content_title;
				console.log(this.content);
		this.metaService.setTitle(' Contact Feenu.com - Get To Know Us | Feenu '+this.content_title);
		this.metaService.setTag('og:image','assets/images/favicon.png');
		this.metaService.setTag('og:description',this.content);
		});
	}
	
  
  
	    onSubmit(){
		if(!this.user_email ){
		  this.panelService.openSnackBar('User email is required', 'Ok','errorMsg');
		  return;
		}
		if(!this.user_password ){
		  this.panelService.openSnackBar('Password name is required', 'Ok','errorMsg');
		  return;
		}
		
		this.loader = true;
		var normalizeSellerPayload = {
			'user_email': this.user_email,
			'user_password': this.user_password,
			'apiname': 'userlogin'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				if(data.status == 201){
					//this.panelService.openSnackBar(data.message, 'OK','errorMsg');
					$('html, body').animate({ scrollTop: 0 }, 'fast');
			          $('.msg_show').removeClass('display_none');
					  $('.msg_show').empty();
					  $('.msg_show').append(data.message);
				         $('.msg_show').fadeTo(2000, 500).slideUp(500, function(){
				         
			           });
				}else{
				
					localStorage.setItem('userData' , JSON.stringify(data.data));
					this.panelService.openSnackBar(data.message, 'OK','successMsg');
					$('#browseLogin .close').trigger('click');
					$('#browseLogin').removeClass('show');
					this.router.navigate(['home']);
					
				   //console.log('yes');
					//$("#browseLogin").modal('hide');
				}
				
				
		});
		
  }
  
  
 signIn(provider){
		//console.log(this.declarativeFormCaptchaValue);
		console.log(provider);
		this._auth.login(provider).subscribe(
			(data) => {
				console.log(data);
			 this.auth_logn = (data);
			  //user data
			  //first_name, last_name, image, uid, provider, email, token (returns  accessToken for Facebook and Google no token for linkedIn)
				//if(provider == "facebook"){
				 	var normalizeSellerPayload1 = {
					'email_id': this.auth_logn.email,
					'apiname': 'check_user_exist_before_login'
				  }
				  
				this.panelService.comman_service_funcation(normalizeSellerPayload1)
					.subscribe(data => {
						console.log(data);
						if((data.data).length){
						  console.log('yes');
						  console.log(data.data[0]);
							localStorage.setItem('userData', JSON.stringify(data.data));

							this.router.navigate(['home']);
						}else{
							/* var fb_user_data = {
								'first_name': this.auth_logn.name.split(" ")[0],
								'last_name': this.auth_logn.name.split(" ")[1],
								'email': this.auth_logn.email
							} */
							this.social_registration(this.auth_logn.email,this.auth_logn.name.split(" ")[0],this.auth_logn.name.split(" ")[1]);
							
							//localStorage.setItem('fb_user_data', JSON.stringify(fb_user_data));
						
							//this.router.navigate(['registration']);
						
						}

					});
			
				//}
			},
			(error) => {
				console.log(error);
			}
		)
	}
	
	 social_registration(email,fname,lname){
		
		this.loader = true;
		var normalizeSellerPayload = {
			'first_name': fname,
			'last_name': lname,
			'user_name': fname,
			'email_id': email,
			'gender': '',
			'userpassword': '123456',
			'userstatus': 'inactive',
			'user_image': 'profile.png',
			'created_date': this.created_date,
			'created_time': this.created_time,
			'google_plus_link': '',
			'profilepicturestatus': 'Approved',
			'twitter_link': '',
			'fb_link': '',
			'profile_show': 'Public',
			'apiname': 'social_registeration'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
			this.loader = false;
				if(data.status == 201){
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					
					//localStorage.setItem('userData' , JSON.stringify(data.data));
					this.panelService.openSnackBar(data.message, 'OK','successMsg');
					this.create_session(data.data)
					//this.router.navigate(['home']);
                  			  
				}
				
		});
		
  }
  
  create_session(email){
	  console.log(email);
	  this.loader = true;
	 const normalizeSellerPayload = {
			'user_email': email,
			'apiname': 'create_session'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe(data => {
					this.loader = false;
				if(data.status == 201){
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
					
				}else{
				
					localStorage.setItem('userData' , JSON.stringify(data.data));
					this.panelService.openSnackBar(data.message, 'OK','successMsg');

					this.router.navigate(['home']);
				
				}
			}); 
  }
  
	
}
