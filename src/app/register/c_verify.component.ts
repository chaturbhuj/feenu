import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatSnackBar, MatDialog } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { PanelServices } from '../services/panel.service';

@Component({
  selector: 'app-c_verify',
  templateUrl: './c_verify.component.html',
  styleUrls: ['./c_verify.component.css']
})
export class c_verifyComponent implements OnInit{
  
 
  loader;
  verification_code;
  new_password = '';
  confirm_password = '';
  constructor(
              public snackBar : MatSnackBar,
              public matDialog : MatDialog,private activatedRoute: ActivatedRoute,
              public router : Router,
							private panelService: PanelServices
						) {
			$('body').addClass('body_img');
					this.verification_code = activatedRoute.snapshot.params.verification_code;
					
				console.log(this.verification_code);
					
			  }

  ngOnInit() {
 
	this.verify_registeration();
  } 

	verify_registeration(){
	    this.loader = true;
		var normalizeSellerLoginPayload = {
			'verification_code': this.verification_code,
			'apiname': 'verify_registeration'
			
		}

		this.panelService.comman_service_funcation(normalizeSellerLoginPayload)
			.subscribe( data => {
				this.loader = false;
				if(data.status == 201){
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					this.panelService.openSnackBar(data.message, 'OK','successMsg');
					this.router.navigate(['login']);
				}
				
		});
	}
  
}
