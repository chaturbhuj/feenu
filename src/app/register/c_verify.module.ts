import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { c_verifyComponent } from './c_verify.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [c_verifyComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([{
      path: '',
      component: c_verifyComponent
    }])
  ]
})
export class c_verifyModule { }
