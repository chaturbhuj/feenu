import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { registerComponent } from './register.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { Angular2SocialLoginModule } from "angular2-social-login";

import { RecaptchaModule, RECAPTCHA_SETTINGS, RecaptchaSettings } from 'ng-recaptcha';
import { RecaptchaFormsModule } from 'ng-recaptcha/forms';

let providers = {
    "google": {
      "clientId": "223170190457-6skcepvghff1kgdstags5ude0hkhfb0p.apps.googleusercontent.com"
    },
    "linkedin": {
      "clientId": "LINKEDIN_CLIENT_ID"
    },
    "facebook": {
      "clientId": "569941400506800",
      "apiVersion": "v3.3" //like v2.4
    }
  };
  //569941400506800
@NgModule({
  declarations: [registerComponent],
	imports: [
		CommonModule,
		RecaptchaModule,
		RecaptchaFormsModule,
		SharedModule,Angular2SocialLoginModule,
		RouterModule.forChild([{
		  path: '',
		  component: registerComponent
		}])
	],
	providers: [
		{
			provide: RECAPTCHA_SETTINGS,
			useValue: {
			  siteKey: '6LddDagUAAAAACtrsz9GosCbBtlo9wxI-S1bLs58',
			} as RecaptchaSettings,
		}
	]
})
export class registerModule { }
Angular2SocialLoginModule.loadProvidersScripts(providers);