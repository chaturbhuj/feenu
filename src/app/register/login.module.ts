import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { loginComponent } from './login.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { Angular2SocialLoginModule } from "angular2-social-login";

let providers = {
    "google": {
      "clientId": "223170190457-6skcepvghff1kgdstags5ude0hkhfb0p.apps.googleusercontent.com"
    },
    "linkedin": {
      "clientId": "LINKEDIN_CLIENT_ID"
    },
    "facebook": {
      "clientId": "569941400506800",
      "apiVersion": "v3.3" //like v2.4
    }
  };
  
@NgModule({
  declarations: [loginComponent],
  imports: [
    CommonModule,
    SharedModule,Angular2SocialLoginModule,
    RouterModule.forChild([{
      path: '',
      component: loginComponent
    }])
  ]
})
export class loginModule { }
Angular2SocialLoginModule.loadProvidersScripts(providers);
