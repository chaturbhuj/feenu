import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { MetaConfig, MetaService } from 'ng2-meta';
import { AuthService } from "angular2-social-login";
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ChangeEvent } from '@ckeditor/ckeditor5-angular/ckeditor.component';

import { PanelServices } from '../services/panel.service';

declare var jquery: any;
declare var $ : any;
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
}) 
export class registerComponent implements OnInit {
	public Editor = ClassicEditor;
contact_name='';
contact_email='';
contact_number='';
contact_subject='';
contact_message='';
contact_url='';
contact_facebook='';
contact_twitter='';
contact_address='';
contact_youtube='';
contact_pinterest='';
google_plus='';
content_title='';
content='';
contactus_content_id='';
created_date='';
created_time='';
first_name='';
last_name='';
user_name='';
email_id='';
gender='';
pass='';
auth_logn: any = '';
confirm_pass='';
user_email = '';
user_password = '';
declarativeFormCaptchaValue = '';
checkuser_name = 'no';
read_policy = '';
fill_check = 'no';
fb_user_data:any;
	 loader;
  constructor(public router: Router,public _auth: AuthService, private panelService: PanelServices,private metaService: MetaService,
  private location: Location) {
	$('body').removeClass('body_img');
	
			 
	 this.fb_user_data = JSON.parse(localStorage.getItem("fb_user_data"));
	console.log(this.fb_user_data);
	
	if(this.fb_user_data)
	  {
		 this.first_name = 	this.fb_user_data.first_name;
		 this.last_name = 	this.fb_user_data.last_name;
		 this.email_id = 	this.fb_user_data.email;
		 this.fill_check = 'yes';
		
	 setTimeout(function(){
           $('.fb').trigger('click');
        },1000); 
		
	 } 
    console.log(this.email_id);	 
	
	}

 
  ngOnInit(){
	
	  //setTimeout(function(){ $('#regi_fb').click()}, 100);
	     var d = new Date();
		var month = d.getMonth()+1;
		var day = d.getDate();

		this.created_date = d.getFullYear() + '-' + (month<10 ? '0' : '') + month + '-' + (day<10 ? '0' : '') + day;
		this.created_time = (d.getHours()<10 ? '0' : '') + d.getHours() + ":" + (d.getMinutes()<10 ? '0' : '') + d.getMinutes() + ":" + (d.getSeconds()<10 ? '0' : '') + d.getSeconds();
	
 
	if(!this.panelService.checkIfUserOnMobile()){
			$('.navbar').addClass('head1');
	        $('.footer').addClass('foot');
		}else{
			$('.header').addClass('head1');
		}

        localStorage.setItem('fb_user_data' , JSON.stringify(''));	
	}
  
   facebook(){
	   console.log('trigger success');
   }
	
	
	 onregSubmit(){
		 //console.log(this.$('#checkbox').is(":checked"));
		 console.log(this.read_policy);
		if(!this.user_name){
		  this.panelService.openSnackBar('User Name is required', 'Ok','errorMsg');
		  return;
		}
		var EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
		if((!EMAIL_REGEXP.test(this.email_id))){
			this.panelService.openSnackBar(' Email not valid', 'Ok','errorMsg');
			return;
		}
		if(!this.pass){
		  this.panelService.openSnackBar('Password is required', 'Ok','errorMsg');
		  return;
		}
		if(this.confirm_pass != this.pass){
		  this.panelService.openSnackBar('Confirm password not match', 'Ok','errorMsg');
		  return;
		}
		  if(!this.read_policy){
		   this.panelService.openSnackBar('Please read privacy policy', 'Ok','errorMsg');
		  return;	
		}
		 if(!this.declarativeFormCaptchaValue){
		   this.panelService.openSnackBar('Please check captcha', 'Ok','errorMsg');
		  return;	
		}  
		
		this.loader = true;
		var normalizeSellerPayload = {
			'first_name': this.first_name,
			'last_name': this.last_name,
			'user_name': this.user_name,
			'email_id': this.email_id,
			'gender': this.gender,
			'userpassword': this.pass,
			'userstatus': 'inactive',
			'user_image': 'profile.png',
			'created_date': this.created_date,
			'created_time': this.created_time,
			'google_plus_link': '',
			'profilepicturestatus': 'Approved',
			'twitter_link': '',
			'fb_link': '',
			'profile_show': 'Public',
			'apiname': 'user_registeration'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
			this.loader = false;
				if(data.status == 201){
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					this.panelService.openSnackBar(data.message, 'OK','successMsg');
					$('#browseRegister .close').trigger('click');
					$('#browseRegister').removeClass('show');
					 localStorage.setItem('fb_user_data' , JSON.stringify(''));	
                    this.first_name = '';					
                    this.last_name = '';					
                    this.email_id = '';					
                    this.gender = '';					
                    this.pass = '';	
                
                  this.router.navigate(['login']);	
                  			  
				}
				
		});
		
  }
  
   social_registration(email,fname,lname){
		
		this.loader = true;
		var normalizeSellerPayload = {
			'first_name': fname,
			'last_name': lname,
			'user_name': fname,
			'email_id': email,
			'gender': '',
			'userpassword': '123456',
			'userstatus': 'inactive',
			'user_image': 'profile.png',
			'created_date': this.created_date,
			'created_time': this.created_time,
			'google_plus_link': '',
			'profilepicturestatus': 'Approved',
			'twitter_link': '',
			'fb_link': '',
			'profile_show': 'Public',
			'apiname': 'social_registeration'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
			this.loader = false;
				if(data.status == 201){
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					
					//localStorage.setItem('userData' , JSON.stringify(data.data));
					this.panelService.openSnackBar(data.message, 'OK','successMsg');
					this.create_session(data.data)
					//this.router.navigate(['home']);
                  			  
				}
				
		});
		
  }
  
  create_session(email){
	  console.log(email);
	  this.loader = true;
	 const normalizeSellerPayload = {
			'user_email': email,
			'apiname': 'create_session'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe(data => {
					this.loader = false;
				if(data.status == 201){
					//this.panelService.openSnackBar(data.message, 'OK','errorMsg');
					
				}else{
				
					localStorage.setItem('userData' , JSON.stringify(data.data));
					this.panelService.openSnackBar(data.message, 'OK','successMsg');

					this.router.navigate(['home']);
				
				}
			}); 
  }
  
  
	signIn(provider){
		console.log(this.declarativeFormCaptchaValue);
		console.log(provider);
		this._auth.login(provider).subscribe(
			(data) => {
				console.log(data);
			 this.auth_logn = (data);
			  //user data
			  //first_name, last_name, image, uid, provider, email, token (returns  accessToken for Facebook and Google no token for linkedIn)
				//if(provider == "facebook"){
					
					var normalizeSellerPayload1 = {
					'email_id': this.auth_logn.email,
					'apiname': 'check_user_exist_before_login'
				  }
				  
					this.panelService.comman_service_funcation(normalizeSellerPayload1)
					.subscribe(data => {
						console.log(data);
						if((data.data).length){
						  console.log('yes');
						  console.log(data.data[0]);
							localStorage.setItem('userData', JSON.stringify(data.data));

							this.router.navigate(['home']);
						}else{
							 console.log('noooo');
							 this.email_id = (this.auth_logn.email);
							console.log(this.auth_logn.image);
							//this.uploadprofileimage(this.auth_logn.image);
							this.first_name = this.auth_logn.name.split(" ")[0];
							this.last_name = this.auth_logn.name.split(" ")[1];
							console.log(this.first_name);
							console.log(this.last_name);
							this.social_registration(this.email_id,this.first_name,this.last_name);
							 setTimeout(function(){
								   $('.fb').trigger('click');
								},1000); 
						}

					});
			
				//}
			},
			(error) => {
				console.log(error);
			}
		)
	}
	
	uploadprofileimage(img){
		console.log(img);
		const normalizeSellerPayload = {
			'image_base_64': img,
			'apiname': 'download_pro_img'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe(data => {
				console.log(data.imgname);
				//this.download_pro_img = this.panelService.returnbaseUrl() + '/' + (data.imgname);
			});
	}
  
  enquirySubmit(){
	var descripation = /^([a-zA-Z0-9\s]{30,})$/;
	
		if(!this.contact_name ){
		  this.panelService.openSnackBar('Contact name is required', 'Ok','errorMsg');
		  return;
		}
		
		if(!this.contact_email){
			this.panelService.openSnackBar(' Email field is required', 'Ok','errorMsg');
			return;
		}else{
			var EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
			if((!EMAIL_REGEXP.test(this.contact_email))){
				this.panelService.openSnackBar('Email not valid', 'Ok','errorMsg');
				return;
			}
		}
		if(!this.contact_subject){
		  this.panelService.openSnackBar('Contact subject is required', 'Ok','errorMsg');
		  return;
		}
		
		if(!this.contact_message){
		  this.panelService.openSnackBar('Contact message is required', 'Ok','errorMsg');
		  return;
		}
		
		
		this.loader = true;
		var normalizeSellerPayload = {
			'contact_name': this.contact_name,
			'contact_email': this.contact_email,
			'contact_subject': this.contact_subject,
			'contact_message': this.contact_message,
			'created_date': this.created_date,
			'created_time': this.created_time,
			'apiname': 'contact_enquiry_email'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				if(data.status == 201){
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					this.panelService.openSnackBar(data.message, 'OK','successMsg');
					//this.router.navigate(['aboutus']);
					this.contact_name = '';
					this.contact_email = '';
					this.contact_subject = '';
					this.contact_message = '';
				}
		});
	}
}
