import { TestBed } from '@angular/core/testing';

import { GoogleAnalyticsEventsServiceService } from './google-analytics-events-service.service';

describe('GoogleAnalyticsEventsServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GoogleAnalyticsEventsServiceService = TestBed.get(GoogleAnalyticsEventsServiceService);
    expect(service).toBeTruthy();
  });
});
