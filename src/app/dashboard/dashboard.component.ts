import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { PanelServices } from '../services/panel.service';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ChangeEvent } from '@ckeditor/ckeditor5-angular/ckeditor.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class dashboardComponent implements OnInit {
	loader:boolean = false;
	termAndConditionDoc = '';
	products_count = 0;
	recentproducts='';
	recentsellerlist='';
	recentcustomerlist='';
	recentorderlist='';
	user_count = 0;
	game_count = 0;
	product_img_src='';
	customer_count = 0;
	approve_count = 0;
	unapprove_count = 0;
	unapprove_comment = 0;
	baseUrl='';
	mainUrl='';
	constructor(public router: Router, private panelService: PanelServices, private location: Location) {
	$('body').removeClass('body_img');
	
	  /*   var admin_data = JSON.parse(localStorage.getItem("FeenuAdminData"));
	   
	   //console.log(admin_data.user._id);
		if(admin_data.user == null){
			this.panelService.openSnackBar('Please Login to continue', 'Ok','errorMsg');
			this.router.navigate(['/adminlogin']);
		} else if(!admin_data.user._id){
			this.panelService.openSnackBar('Please Login to continue', 'Ok','errorMsg');
			this.router.navigate(['/adminlogin']);
		} */  
	
  }
 
	ngOnInit() {
		// this.get_products_count();	
		 this.get_user_count();	
		 this.get_game_count();	
		 this.get_profile_count();	
	}
	
	
	
	 get_user_count() {
		 this.loader = true;
		var normalizeSellerPayload = {
				'apiname': 'user_count'
			}
			this.panelService.comman_service_funcation(normalizeSellerPayload)
				.subscribe( data => {
					this.loader = false;
					this.user_count = data.data;
					console.log(this.user_count);
			});
	 }
	 
	  get_profile_count() {
		 this.loader = true;
		var normalizeSellerPayload = {
				'apiname': 'get_profile_count'
			}
			this.panelService.comman_service_funcation(normalizeSellerPayload)
				.subscribe( data => {
					this.loader = false;
					this.approve_count = data.approved;
					this.unapprove_count = data.unapproved;
					this.unapprove_comment = data.comment;
			});
	 }
	 
	 get_game_count() {
		 this.loader = true;
		var normalizeSellerPayload = {
				'apiname': 'game_count'
			}
			this.panelService.comman_service_funcation(normalizeSellerPayload)
				.subscribe( data => {
					this.loader = false;
					this.game_count = data.data;
					console.log(this.game_count);
			});
	 }
	 
	
}
