export class SellerProduct {
    "product_id": number;
    "menu_category_id": number;
    "menu_category_name": string;
    "menu_subcategory_id": number;
    "menu_subcategory_name": string;
    "product_category_id": number;
    "product_category_name": string;
    "product_subcategory_id": number;
    "product_subcategory_name": string;
    "product_brand_id": number;
    "product_brand_name": string;
    "product_name": string;
    "product_desc": string;
    "product_images": string;
    "actual_price": number;
    "discount_per": number;
    "after_discount_price": number;
    "valuess": string;
    "impacts": string;
    "created_date": string;
    "created_time": string;
    "product_status": string;
    "seller_id": number
}