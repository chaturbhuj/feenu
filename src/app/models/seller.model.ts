export class Seller {
    
    "seller_id": number;
    "seller_name": string;
    "seller_email": string;
    "seller_company": string;
    "seller_logo": string;
    "seller_password": string;
    "created_date": string;
    "seller_status": string;
    "verification_code": string;
    "seller_banner": string;
}