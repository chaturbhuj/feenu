import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FormGroup, FormControl } from "@angular/forms";
import { MatButtonToggleChange } from '@angular/material';
import { PanelServices } from '../services/panel.service';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ChangeEvent } from '@ckeditor/ckeditor5-angular/ckeditor.component';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import * as _ from 'lodash';
declare var jquery: any;
declare var $ : any;
@Component({
  selector: 'app-user_edit',
  templateUrl: './user_edit.component.html',
  styleUrls: ['./user_edit.component.css']
})
export class user_editComponent implements OnInit {
	loader;
	public Editor = ClassicEditor;
	dropdowntagList = [];
	dropdowntagList1 = [];
	selectedtagItems = [];
	dropdowntagSettings = {};
	 editorData = '';
	 editorData1 = '';
	imageChangedEvent: any = '';
	croppedImage: any;
	game_title = '';
	game_image = '';
	games_img_src = '';
	created_date = '';
	created_time = '';
	game_url = '';
	games_content='';
	games_content1='';
	game_tag='';
	game_walkthrough='tubia';
	game_orientation='both';
	game_height='600';
	game_category_id = '';
	game_status = 'Inactive';
	staff_id = 0;
	game_id = 0;
	meta_keyword = '';
	game_slug = '';
	mobile_friendly = 'No';
	youtube_link = '';
	play_store_url = '';
	apple_store_url = '';
	
	game_category_name = '';
	gamecategorylist = '';
	meta_description = '';
	meta_tags = '';
	games_control = '';
	game_width = '';
	game_slugs = '';
	user_id = '';
	first_name = '';
	last_name = '';
	email_id = '';
	user_name = '';
	profile_show = '';
	user_image_src = '';
	userstatus = '';
	gender = '';
	profilepicturestatus = '';
	google_plus_link = '';
	twitter_link = '';
	fb_link = '';
	admin_img = '';
	
	constructor(public router: Router,private activatedRoute: ActivatedRoute,
              private panelService: PanelServices) {
					$('body').removeClass('body_img');
					this.user_id = activatedRoute.snapshot.params.user_id;
				this.get_user_by_id(this.user_id);	
			}
 
	ngOnInit(){   
		var d = new Date();
		var month = d.getMonth()+1;
		var day = d.getDate();

		this.created_date = d.getFullYear() + '-' + (month<10 ? '0' : '') + month + '-' + (day<10 ? '0' : '') + day;
		this.created_time = (d.getHours()<10 ? '0' : '') + d.getHours() + ":" + (d.getMinutes()<10 ? '0' : '') + d.getMinutes() + ":" + (d.getSeconds()<10 ? '0' : '') + d.getSeconds();
		
		
		
		$('.select_game_walkthrough').on('change', function() {
			console.log('yes');
			var select_val = $(this).val();
		
			if(select_val == 'youtube'){
				$('.youtube_link').removeClass('display_none');
			}
			
		});
		
		
		
	}
	
	/* game_slug_click(){
		
		if(this.game_title){
	     this.game_slug =  this.game_title.toLowerCase().replace(' ', '-').replace(/[0-9]/g, '');
		}	
	} */
	
	onItemSelect(item: any) {
    console.log(item);
  }
  onSelectAll(items: any) {
    console.log(items);
  }

  

	get_user_by_id(user_id){
	this.loader = true;
		var normalizeSellerPayload = {
			'apiname': 'user_account',
			'user_id': user_id
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
			this.loader = false;
			this.first_name = data.data[0].first_name;
			this.last_name = data.data[0].last_name;
			this.email_id = data.data[0].email_id;
			this.user_name = data.data[0].user_name;
			this.gender = data.data[0].gender;
			this.profile_show = data.data[0].profile_show;
			this.userstatus = data.data[0].userstatus;
			this.profilepicturestatus = data.data[0].profilepicturestatus;
			this.google_plus_link = data.data[0].google_plus_link;
			this.twitter_link = data.data[0].twitter_link;
			this.fb_link = data.data[0].fb_link;
			this.admin_img = data.data[0].user_image;
			
			if(data.data[0].user_image != 'profile.png'){
				console.log('user_imageuser_imageuser_image');
						this.user_image_src = data.data[0].user_image;
			  }else{
						this.user_image_src  = this.panelService.returnbaseUrl()+'/'+('profile.png')
			
					 }
		});
	}

  
	public onChangeckeditor( { editor }: ChangeEvent ) {
		this.games_content = editor.getData();
	}
	public onChangeckeditor1( { editor }: ChangeEvent ) {
		this.games_control = editor.getData();
	}
	
	onmoduleChange($event) {
		this.game_category_id = (this.game_category_id);
        this.game_category_name = _.filter(this.gamecategorylist, {
            "_id": (this.game_category_id)
        })[0].game_category_name;
		console.log(this.game_category_name);
		console.log(this.game_category_id);
	}
	
	onSubmit(){
	//var contactno = /^([0-9\s]{10,})$/;
		if(!this.first_name ){
		  this.panelService.openSnackBar('First name is required', 'Ok','errorMsg');
		  return;
		}		
		if(!this.last_name ){
		  this.panelService.openSnackBar('Last name is required', 'Ok','errorMsg');
		  return;
		}
		if(!this.user_name ){
		  this.panelService.openSnackBar('User name is required', 'Ok','errorMsg');
		  return;
		}
		  if(!this.gender ){
			  this.panelService.openSnackBar('Gender is required', 'Ok','errorMsg');
			  return;
			}		
				
		if(!this.email_id ){
		  this.panelService.openSnackBar('Email id is required', 'Ok','errorMsg');
		  return;
		}
       		
		
		this.loader = true;
		var normalizeSellerPayload = {
		    'user_id': this.user_id,
			'first_name': this.first_name,
			'last_name': this.last_name,
			'user_name': this.user_name,
			'gender': this.gender,
			'user_image': this.admin_img,
			'email_id': this.email_id,
			'fb_link': this.fb_link,
			'twitter_link': this.twitter_link,
			'google_plus_link': this.google_plus_link,
			'profile_show': this.profile_show,
			'profilepicturestatus': 'Approved',
			'apiname': 'update_profile'
		}
		
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				if(data.status == 201){
				
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					this.panelService.openSnackBar(data.message, 'OK','successMsg');
					
					this.router.navigate(['userlist']);
				}
		});
		
  }
  deleteimg(){
	  console.log('yes');
	  this.loader = true;
		var normalizeSellerPayload = {
		    'user_id': this.user_id,
			'apiname': 'delete_profile_img'
		}
		
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
			   if(data.status == 200){
				this.panelService.openSnackBar(data.message, 'OK','successMsg');  
            this.get_user_by_id(this.user_id);				
			   }
		});
	  
  }
   chooseimg(){
	  $('.mycropimageuploadinput').trigger('click');
  }
    fileChangeEvent(event: any): void {
		this.imageChangedEvent = event;
	}

	imageCropped(event: ImageCroppedEvent) {
			this.croppedImage = event.base64;
	}

	onSaveCropImage(){
		
        this.loader = true;	
        var check = 'no';     
		const normalizeSellerPayload = {
			'image_base_64': this.croppedImage,
			'folder_name': 'user-profile-pics',
			'apiname': 'googlecoludImagebase64'
		}
		$('#close_register_modal').trigger('click');
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.admin_img = (data.myImage);
			  this.user_image_src = (data.myImage);
			$('.mycropimageuploadinput').val('');
			$('.source-image').attr('src','');
			this.croppedImage = '';
			this.fileChangeEvent('ok');
			setTimeout(
				this.loader = false ,
					  1000
					);
			
		});
	 
		 $('.close').trigger('click');	
	}
   
  
  
}
