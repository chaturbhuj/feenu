import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown'; 
import { user_editComponent } from './user_edit.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [user_editComponent],
  imports: [
    CommonModule,CKEditorModule,NgMultiSelectDropDownModule,
    SharedModule,
    RouterModule.forChild([{
      path: '',
      component: user_editComponent
    }])
  ]
})
export class user_editModule { }
