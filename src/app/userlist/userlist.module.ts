import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { userlistComponent } from './userlist.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [userlistComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([{
      path: '',
      component: userlistComponent
    }])
  ]
})
export class userlistModule { }
