import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { FormGroup, FormControl } from "@angular/forms";
import { PanelServices } from '../services/panel.service';

declare var jquery: any;
declare var $ : any;
@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.css']
})
export class userlistComponent implements OnInit {
	loader;
	userlist = '';
	submodulelist = '';
	accesslist = '';
	user_access_list = '';
	emp_name = '';
	userstatus = '';
	user_id = 0;
	emp_id = 0;
	submudule_id = '';
	module_id = '';
	skip = 0;
	limit = 20;
	count: any;
	search ='no';
	permission ='';
	index1 = 1;
	search_string = '';
	email_search_string = '';
	public current_page: number = 0;
		totalcount: number = 0;
	constructor(public router: Router,
              private panelService: PanelServices) {
				$('body').removeClass('body_img');
			  }

	ngOnInit(){ 
		this.get_all_user_list(this.skip,this.limit); 
		this.get_all_submodule_list(); 
		this.get_all_access_list(); 
	}
	
	get_all_user_list(skip,limit){
	    this.loader = true;
		var normalizeSellerPayload = {
			'skip':skip,
			'limit':limit,
			'apiname': 'list_user'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				this.userlist = data.data;
				console.log(this.userlist);
				this.count = Array(Math.ceil(data.count/this.limit)).fill(0).map((x,i)=>i);
				this.totalcount = (Math.round(data.count/this.limit));
		});
	}
	
	setpage(i, event: any) {
		//event.prevendDefult();
		this.current_page = i;
		
		console.log(i);
		if(i == 0){
		  var page = ((i*20));	
		}else{
			var page = ((i*20)+1);
		}
		this.index1 = ((i*20)+1);
		

		this.get_all_user_list(page,this.limit);
		
     	 
     	   
	}
	
	delete_user(user_id){
		var val = confirm("Do you want to delete");
		if(val == true){
		this.loader = true;
		var normalizeSellerPayload = {
			'apiname': 'delete_user',
			'user_id': user_id
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
			this.loader = false;
			 this.get_all_user_list(this.skip,this.limit); 
				if(data.status == 201){
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					this.panelService.openSnackBar(data.message, 'OK','successMsg');
					
				}
		});
		return true;
		}else{
			return false;
		}
	}
	
	approval(skip,limit,permission){
		this.permission = permission;
		this.search = 'yes';
		console.log(permission);
		this.loader = true;
		var normalizeSellerPayload = {
			'skip':skip,
			'limit':limit,
			'permission': permission,
			'apiname': 'get_approval_user_list'
			
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				console.log(data.data);
			this.userlist = data.data;
			this.count = Array(Math.ceil(data.count/this.limit)).fill(0).map((x,i)=>i);
				this.totalcount = (Math.round(data.count/this.limit));
				//console.log(this.commentlist); 
				
		});
	}
	setsearchpage(i, event: any){
		this.current_page = (i);
		console.log("yes");
		//this.gamelist = '';
		//this.gamelist.splice(0,this.gamelist.length)
		if(i == 0){
		  var page = ((i*20));	
		}else{
			var page = ((i*20)+1);
		}
		this.index1 = ((i*20)+1);
		
		this.approval(page,20,this.permission); 
		
	}
	
	setsearchpage1(i, event: any){
		this.current_page = (i);
		console.log("yes");
		console.log(i);
		if(i == 0){
		  var page = ((i*20));	
		}else{
			var page = ((i*20)+1);
		}
		
		this.index1 = ((i*20)+1);
		
		this.go_to_search_page_query(page,this.limit);
		
	}
	
	go_to_search_page_query(skip,limit){
		this.search = 'filter';
		
			var normalizeSellerPayload = {
				'searchby': this.search_string,
				'searchbyemail': this.email_search_string,
				'skip': skip,
				'limit': 20,
				'apiname': 'search_user'
			}
			this.panelService.comman_service_funcation(normalizeSellerPayload)
				.subscribe( data => {
					this.userlist = data.data;
			     this.count = Array(Math.ceil(data.count/this.limit)).fill(0).map((x,i)=>i);
				  this.totalcount = (Math.round(data.count/this.limit));
					
				
				 });
	    
	}
	
	keyDownFunction(event) {
		 console.log('nooooooooo');
	  if(event.keyCode == 13) {
		  console.log('yesss');
		  this.go_to_search_page_query(0 ,20);
		//$('.search_icon').trigger('click');
		// rest of your code
	  }
	}
	
	onSubmit(){
		$("#browseChangeStatus").modal('hide');
		this.loader = true;
		var normalizeSellerPayload = {
			'user_id': this.user_id,
			'userstatus': this.userstatus,
			'apiname': 'update_user_status'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
			this.loader = false;
			 this.get_all_user_list(this.skip,this.limit); 
				if(data.status == 201){
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					this.panelService.openSnackBar(data.message, 'OK','successMsg');
					
				}
				$('.close').trigger('click');
		});
  }
	
	edituserstatus(user_id, userstatus){
		this.user_id = user_id;
		this.userstatus = userstatus;
	}
	
	profilepic(user_id,userimg,picstatus){
		console.log(user_id);
		console.log(picstatus);
		console.log(userimg);
		this.loader = true;
		var normalizeSellerPayload = {
			'apiname': 'profile_pic_status',
			'user_id': user_id,
			'picstatus': picstatus,
			'userimg': userimg
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
			this.loader = false;
			this.get_all_user_list(this.skip,this.limit); 
		});
	}
	get_all_submodule_list(){
	this.loader = true;
		var normalizeSellerPayload = {
			'apiname': 'list_submodule'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
			this.loader = false;
				this.submodulelist = data.data;
				console.log(this.submodulelist);
				console.log(this.accesslist);
				
				/* $.each(this.submodulelist,function(i,val){
					var result=$.inArray(val,this.accesslist);
					if(result!=-1)
					alert(result); 
					}) */
				//$(".myCheckbox").prop('checked', true);
		});
	}
	
	get_all_access_list(){
	this.loader = true;
		var normalizeSellerPayload = {
			'apiname': 'list_access'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
			this.loader = false;
				this.accesslist = data.data;
				console.log(this.accesslist);
		});
	}
	
	emp_access(user_id, name){
		this.emp_id = user_id;
		this.emp_name = name;
		
	    this.loader = true;
		var normalizeSellerPayload = {
			'user_id': this.emp_id,
			'apiname': 'access_list_by_id'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				this.user_access_list = data.data;
				 $.each(this.user_access_list,function(i,val){
					console.log(val.submodule_id);
					$('input[value="'+val.submodule_id+'"]').prop('checked', true);
				})
				console.log(this.user_access_list); 
		});
	
	}
	
	
	
	accessSubmit(){
		console.log(this.emp_id);
		//this.loader = true;
		var access_info = [];
		$('input[type=checkbox]').each(function () {
			var aa = this.emp_id;
			console.log(this.emp_id);
				   if (this.checked) {
					    console.log($(this).closest("td").siblings("td")); 
					   // console.log($('#module_link').val()); 
					 access_info.push({
							submodule_id:$(this).val(),
							module_id :$(this).attr('name')
						}); 
				   }
		});	
		
	
		
		console.log(access_info);
		
		var normalizeSellerPayload = {
			'access_info': access_info,
			'staff_id': this.emp_id,
			'apiname': 'add_access1'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
			//this.loader = false;
			
				if(data.status == 201){
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					this.panelService.openSnackBar(data.message, 'OK','successMsg');
					$('#browseAccessModal .close').trigger('click');
					$('#browseAccessModal').removeClass('show');
				}
				
		});
	}
  
  
}
