import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { forget_passwordComponent } from './forget_password.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [forget_passwordComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([{
      path: '',
      component: forget_passwordComponent
    }])
  ]
})
export class forgetpasswordModule { }
