import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { change_passwordComponent } from './change_password.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [change_passwordComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([{
      path: '',
      component: change_passwordComponent
    }])
  ]
})
export class changepasswordModule { }
