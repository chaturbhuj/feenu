import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { reset_passwordComponent } from './reset_password.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [reset_passwordComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([{
      path: '',
      component: reset_passwordComponent
    }])
  ]
})
export class reset_passwordModule { }
