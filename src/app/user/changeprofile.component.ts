import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { MetaConfig, MetaService } from 'ng2-meta';
import { FormGroup, FormControl } from "@angular/forms";
import { PanelServices } from '../services/panel.service';
import { ImageCroppedEvent } from 'ngx-image-cropper';
declare var jquery: any;
declare var $ : any;
@Component({
  selector: 'app-changeprofile',
  templateUrl: './changeprofile.component.html',
  styleUrls: ['./changeprofile.component.css']
})
export class changeprofileComponent implements OnInit {
  	loader;
	full_name = '';
	admin_email = '';
	imageChangedEvent: any = '';
	croppedImage: any;
	contact = '';
	user_name = '';
	user_id = 0;
	veggmi_admin_id = '';
	admin_img = '';
	admin_img_src = '';
	first_name = '';
	google_plus_link = '';
	twitter_link = '';
	fb_link = '';
	last_name = '';
	profile_show = '';
	gender = '';
	email_id ='';
	profilepic ='Approved';
	constructor(public router: Router,private activatedRoute: ActivatedRoute,private metaService: MetaService,
              private panelService: PanelServices) {
		
				 var user_data = JSON.parse(localStorage.getItem("userData"));
				if(user_data){
				if(user_data== null){
					
					}else if(user_data[0]._id){
						this.user_id = user_data[0]._id;
						
					}
				}else{
					this.router.navigate(['/']);
				}
				console.log(user_data);
				
			}
 
	ngOnInit(){ 
		   if(this.user_id == 0){   
			
		   }else{
			  this.get_profile_by_id(); 
		   }
	this.metaService.setTitle('change profile of Feenu.com - Get To Know Us | Feenu ');
	this.metaService.setTag('og:image','assets/images/favicon.png');
	this.metaService.setTag('og:description','change profile of Feenu.com');
	if(!this.panelService.checkIfUserOnMobile()){
			$('.navbar').addClass('head1');
	        $('.footer').addClass('foot');
		}else{
			$('.header').addClass('head1');
		}
	}
	
	get_profile_by_id(){
		this.loader = true;
		var normalizeSellerPayload = {
			'apiname': 'get_user_profile',
			'user_id': this.user_id
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
			this.loader = false;
				this.first_name = data.data.first_name;
				this.last_name = data.data.last_name;
				this.user_name = data.data.user_name;
				this.gender = data.data.gender;
				this.email_id = data.data.email_id;
				this.google_plus_link = data.data.google_plus_link;
				this.twitter_link = data.data.twitter_link;
				this.fb_link = data.data.fb_link;
				this.profile_show = data.data.profile_show;
				this.admin_img = data.data.user_image;
				console.log(data.data);
				if(data.data.user_image != 'profile.png'){
						this.admin_img_src = (data.data.user_image);
			         }else{
						this.admin_img_src  = this.panelService.returnbaseUrl()+'/'+('profile.png')
			
					 }
		});
	}

  
	block_account() {
		var val = confirm("Do you want to delete the account");
		if(val == true){
		this.loader = true;
		var normalizeSellerPayload = {
			'userstatus': 'remove',
			'user_id': this.user_id,
			'apiname': 'block_account'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				if(data.status == 201){
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					localStorage.setItem('userData' , JSON.stringify(''));
					this.panelService.openSnackBar(data.message, 'OK','successMsg');
					this.router.navigate(['home']);
				}
				
		});
					return true;
		}else{
			return false;
		}
	}
	
	view_profile(){
		console.log('yeseeeeeeeeeeeee');
		this.router.navigate(['/']);
	}
	
	
	onSubmit(){
	//var contactno = /^([0-9\s]{10,})$/;
		if(!this.first_name ){
		  this.panelService.openSnackBar('First name is required', 'Ok','errorMsg');
		  return;
		}		
		if(!this.last_name ){
		  this.panelService.openSnackBar('Last name is required', 'Ok','errorMsg');
		  return;
		}
		if(!this.user_name ){
		  this.panelService.openSnackBar('User name is required', 'Ok','errorMsg');
		  return;
		}
		  if(!this.gender ){
			  this.panelService.openSnackBar('Gender is required', 'Ok','errorMsg');
			  return;
			}		
				
		if(!this.email_id ){
		  this.panelService.openSnackBar('Email id is required', 'Ok','errorMsg');
		  return;
		}
       		
		
		this.loader = true;
		var normalizeSellerPayload = {
		    'user_id': this.user_id,
			'first_name': this.first_name,
			'last_name': this.last_name,
			'user_name': this.user_name,
			'gender': this.gender,
			'user_image': this.admin_img,
			'email_id': this.email_id,
			'fb_link': this.fb_link,
			'twitter_link': this.twitter_link,
			'google_plus_link': this.google_plus_link,
			'profile_show': this.profile_show,
			'profilepicturestatus': this.profilepic,
			'apiname': 'update_profile'
		}
		
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				if(data.status == 201){
				
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					this.panelService.openSnackBar(data.message, 'OK','successMsg');
					
					this.get_profile_by_id();
				}
		});
		
  }
  chooseimg(){
	  $('.mycropimageuploadinput').trigger('click');
  }
    fileChangeEvent(event: any): void {
		this.imageChangedEvent = event;
	}

	imageCropped(event: ImageCroppedEvent) {
			this.croppedImage = event.base64;
	}

	onSaveCropImage(){
		this.loader = true;
		const normalizeSellerPayload = {
			'image_base_64': this.croppedImage,
			'folder_name': 'user-profile-pics',
			'apiname': 'googlecoludImagebase64'
		}
		$('#close_register_modal').trigger('click');
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				
			this.admin_img = (data.myImage);
			this.admin_img_src = (data.myImage);
			$('.mycropimageuploadinput').val('');
			$('.source-image').attr('src','');
			this.croppedImage = '';
			this.profilepic = 'Approved';
			this.fileChangeEvent('ok');
			setTimeout(
				this.loader = false ,
					  1000
					);
		});
		 $('.close').trigger('click');	
	}
  
}
