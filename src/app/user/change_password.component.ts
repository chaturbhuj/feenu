import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { MetaConfig, MetaService } from 'ng2-meta';
import { FormGroup, FormControl } from "@angular/forms";
import { PanelServices } from '../services/panel.service';

@Component({
  selector: 'app-changepassword',
  templateUrl: './change_password.component.html',
  styleUrls: ['./change_password.component.css']
})
export class change_passwordComponent implements OnInit{
  
  loader;
  user_id=0;
  old_pass='';
  new_pass='';
  confirm_pass='';
  constructor(public router: Router,private activatedRoute: ActivatedRoute,private metaService: MetaService,
              private panelService: PanelServices) {
				  var user_data = JSON.parse(localStorage.getItem("userData"));
				
				 if(user_data){
				    if(user_data == null){
					
					} else if(user_data[0]._id){
						
						this.user_id = user_data[0]._id;
						
					}
		
			    }else{
					this.router.navigate(['/']);
				}
			  }

  ngOnInit() {
	 this.metaService.setTitle('change password of Feenu.com - Get To Know Us | Feenu ');
	this.metaService.setTag('og:image','assets/images/favicon.png');
	this.metaService.setTag('og:description','change password of Feenu.com');
	if(!this.panelService.checkIfUserOnMobile()){
			$('.navbar').addClass('head1');
	        $('.footer').addClass('foot');
		}else{
			$('.header').addClass('head1');
		}
  } 

	  
  
  changepass(){
		if(!this.old_pass ){
		  this.panelService.openSnackBar('Old password is required', 'Ok','errorMsg');
		  return;
		}
		if(!this.new_pass ){
		  this.panelService.openSnackBar('New password  is required', 'Ok','errorMsg');
		  return;
		}
		if(!this.confirm_pass ){
		  this.panelService.openSnackBar('confirm password  is required', 'Ok','errorMsg');
		  return;
		}
		
		this.loader = true;
		var normalizeSellerPayload = {
			'old_pass': this.old_pass,
			'new_pass': this.new_pass,
			'user_id': this.user_id,
			'apiname': 'change_password'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				if(data.status == 201){
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					this.panelService.openSnackBar(data.message, 'OK','successMsg');
					this.old_pass = '';
					this.new_pass='';
					this.confirm_pass='';
				}
				
		});
		
  }
}
