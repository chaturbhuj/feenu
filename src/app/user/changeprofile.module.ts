import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { changeprofileComponent } from './changeprofile.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [changeprofileComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([{
      path: '',
      component: changeprofileComponent
    }])
  ]
})
export class changeprofileModule { }
