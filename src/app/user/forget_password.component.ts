import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MetaConfig, MetaService } from 'ng2-meta';
import { MatSnackBar, MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { PanelServices } from '../services/panel.service';

@Component({
  selector: 'app-forgetpassword',
  templateUrl: './forget_password.component.html',
  styleUrls: ['./forget_password.component.css']
})
export class forget_passwordComponent implements OnInit{
  
  forgetForm: FormGroup;
  useremail = '';
  loader;
  constructor(public snackBar : MatSnackBar,
              public matDialog : MatDialog,
              public router : Router,private metaService: MetaService,
							private panelService: PanelServices
						) {
			/* 
					var VeggmiCustomerData = this.panelService.check_customer_auth();
					if(VeggmiCustomerData == null){
					
					}else if(VeggmiCustomerData.customer_id){
						this.router.navigate(['/customer-dashboard']);
					}
					console.log(VeggmiCustomerData); */

			  }

  ngOnInit() {
   // this.registerFormInitialize();
	this.metaService.setTitle('Forget Password of Feenu.com - Get To Know Us | Feenu ');
	this.metaService.setTag('og:image','assets/images/favicon.png');
	this.metaService.setTag('og:description','Forget Password of Feenu.com');
	if(!this.panelService.checkIfUserOnMobile()){
			$('.navbar').addClass('head1');
	        $('.footer').addClass('foot');
		}else{
			$('.header').addClass('head1');
		}
  } 
 
	forgetpassword(){
		console.log(this.useremail);
		if(!this.useremail){
			this.panelService.openSnackBar('Email field is required', 'Ok','errorMsg');
			return;
		}else{
			var EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
			if((!EMAIL_REGEXP.test(this.useremail))){
				this.panelService.openSnackBar('Email not valid', 'Ok','errorMsg');
				return;
			}
		}
		
        this.loader = true;
		var normalizeSellerLoginPayload = {
		     'customer_email': this.useremail,
			'apiname': 'forget_password'
		}

		this.panelService.comman_service_funcation(normalizeSellerLoginPayload)
			.subscribe( data => {
			this.loader = false;
				if(data.status == 201){
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					this.panelService.openSnackBar(data.message, 'OK','successMsg');
					//this.router.navigate(['login']);
				}
				
		});
	}
  
}
