import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatSnackBar, MatDialog } from '@angular/material';
import { MetaConfig, MetaService } from 'ng2-meta';
import { Router, ActivatedRoute } from '@angular/router';
import { PanelServices } from '../services/panel.service';

@Component({
  selector: 'app-resetpassword',
  templateUrl: './reset_password.component.html',
  styleUrls: ['./reset_password.component.css']
})
export class reset_passwordComponent implements OnInit{
  
  resetForm: FormGroup;
  loader;
  verification_code; 
  new_password = '';
  confirm_password = '';
   constructor(public snackBar : MatSnackBar,
              public matDialog : MatDialog,
			  private activatedRoute: ActivatedRoute,private metaService: MetaService,
              public router : Router,
							private panelService: PanelServices
						) {
			
					this.verification_code = activatedRoute.snapshot.params.verification_code;
			
			      console.log(this.verification_code);
					/* var VeggmiCustomerData = this.panelService.check_customer_auth();
					if(VeggmiCustomerData == null){
					
					}else if(VeggmiCustomerData.customer_id){
						this.router.navigate(['/customer-dashboard']);
					}
					console.log(VeggmiCustomerData); */
					
			  }

  ngOnInit() {
   // this.registerFormInitialize();
   this.metaService.setTitle('Reset Password of Feenu.com - Get To Know Us | Feenu ');
	this.metaService.setTag('og:image','assets/images/favicon.png');
	this.metaService.setTag('og:description','Reset Password of Feenu.com');
	if(!this.panelService.checkIfUserOnMobile()){
			$('.navbar').addClass('head1');
	        $('.footer').addClass('foot');
		}else{
			$('.header').addClass('head1');
		}
  } 
 
	resetpassword(){
		var NAME_REGEXP = /^([a-zA-Z0-9\s]{6,})$/;
		
		
		if(!this.new_password ){
			this.panelService.openSnackBar('password is required', 'Ok','errorMsg');
			return;
		}
		if(!this.confirm_password ){
			this.panelService.openSnackBar('confirm password is required', 'Ok','errorMsg');
			return;
		}
		if(this.confirm_password != this.new_password){
			this.panelService.openSnackBar('Password is not match', 'Ok','errorMsg');
			return;
		}
        this.loader = true;
		var normalizeSellerLoginPayload = {
			'new_password': this.new_password,
			'confirm_password': this.confirm_password,
			'verification_code': this.verification_code,
			'apiname': 'reset_password'
		}

		this.panelService.comman_service_funcation(normalizeSellerLoginPayload)
			.subscribe( data => {
			this.loader = false;
				if(data.status == 201){
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					this.panelService.openSnackBar(data.message, 'OK','successMsg');
					//this.router.navigate(['login']);
				}
				
		});
	}
  
}
