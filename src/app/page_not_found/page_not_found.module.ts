import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { page_not_foundComponent } from './page_not_found.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [page_not_foundComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([{
      path: '',
      component: page_not_foundComponent
    }])
  ]
})
export class page_not_foundModule { }
