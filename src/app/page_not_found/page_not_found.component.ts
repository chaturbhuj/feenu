import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { MetaConfig, MetaService } from 'ng2-meta';
import { PanelServices } from '../services/panel.service';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ChangeEvent } from '@ckeditor/ckeditor5-angular/ckeditor.component';

@Component({
  selector: 'app-page_not_found',
  templateUrl: './page_not_found.component.html',
  styleUrls: ['./page_not_found.component.css']
})
export class page_not_foundComponent implements OnInit {
	loader:boolean = false;
	termAndConditionDoc = '';
	products_count = 0;
	recentproducts='';
	recentsellerlist='';
	recentcustomerlist='';
	recentorderlist='';
	user_count = 0;
	game_count = 0;
	product_img_src='';
	customer_count = 0;
	baseUrl='';
	check_device='';
	mainUrl='';
	searchgamelist='';
	constructor(public router: Router, private panelService: PanelServices,private metaService: MetaService,
	private location: Location) {
	$('body').removeClass('body_img');
  }
 
	ngOnInit() {	
		 this.metaService.setTitle('Page not found of Feenu.com - Get To Know Us | Feenu ');
		 this.metaService.setTag('og:image','assets/images/favicon.png');
		 this.metaService.setTag('og:description','Page not found of Feenu.com');
	  this.getFiltergame();
	}
	
	
	 getFiltergame(){
	   if(this.panelService.checkIfUserOnMobile()){
			this.check_device = 'Yes';
		}else{
			this.check_device = 'No';
			}
	   this.loader = true;
		var normalizeSellerPayload = {
			'check_device': this.check_device,
			'apiname': 'add_game_play_time_testing'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				console.log(data.data);
				this.searchgamelist = data.data;
			});
	}

}
