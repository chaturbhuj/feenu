import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { MetaConfig, MetaService } from 'ng2-meta';
import { PanelServices } from '../services/panel.service';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ChangeEvent } from '@ckeditor/ckeditor5-angular/ckeditor.component';

@Component({
  selector: 'app-user_fav',
  templateUrl: './user_fav.component.html',
  styleUrls: ['./user_fav.component.css']
})
export class user_favComponent implements OnInit {
	loader:boolean = false;
	termAndConditionDoc = '';
	products_count = 0;
	recentproducts='';
	gamefavlist='';
	recentcustomerlist='';
	gameplaylist: any = [];
	user_count = 0;
	game_count = 0;
	user_id = 0;
	product_img_src='';
	customer_count = 0;
	baseUrl='';
	mainUrl='';
	constructor(public router: Router, private panelService: PanelServices,private metaService: MetaService,
	private location: Location) {
	$('body').removeClass('body_img');
	
	 var user_data = JSON.parse(localStorage.getItem("userData"));
				if(user_data){
				if(user_data== null){
					
					}else if(user_data[0]._id){
						
					}
				this.user_id = user_data[0]._id;
			
                 }else{
					 this.router.navigate(['/']);
				 }
  }
 
	ngOnInit() {
		
		// this.get_user_game_play_list();	
		 this.get_user_game_fav_list();
		 
	this.metaService.setTitle('User deshboard of Feenu.com - Get To Know Us | Feenu ');
	this.metaService.setTag('og:image','assets/images/favicon.png');
	this.metaService.setTag('og:description','User deshboard of Feenu.com');	
	if(!this.panelService.checkIfUserOnMobile()){
			$('.navbar').addClass('head1');
	        $('.footer').addClass('foot');
		}else{
			$('.header').addClass('head1');
		}
	}
	
	
	 get_user_game_play_list() {
		// this.loader = true;
		var normalizeSellerPayload = {
				'user_id': this.user_id,
				'apiname': 'user_game_play_list'
			}
			this.panelService.comman_service_funcation(normalizeSellerPayload)
				.subscribe( data => {
					this.gameplaylist = data.data;
					console.log(this.gameplaylist);
					  
			});
	 }
	 
	 get_user_game_fav_list() {
		// this.loader = true;
		var normalizeSellerPayload = {
				'user_id': this.user_id,
				'apiname': 'get_user_fav_list'
			}
			this.panelService.comman_service_funcation(normalizeSellerPayload)
				.subscribe( data => {
					this.gamefavlist = data.data;
					console.log("this.gamefavlist");
					console.log(data.data);
					if(data.data == 0){
						$('.no_result').removeClass('display_none');
					}else{
						$('.no_result').addClass('display_none');
					}  
			});
	 }
	 
	
	
}
