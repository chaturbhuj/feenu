import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { user_dashboardComponent } from './user_dashboard.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [user_dashboardComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([{
      path: '',
      component: user_dashboardComponent
    }])
  ]
})
export class user_dashboardModule { }
