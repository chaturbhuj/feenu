import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { user_favComponent } from './user_fav.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [user_favComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([{
      path: '',
      component: user_favComponent
    }])
  ]
})
export class user_favModule { }
