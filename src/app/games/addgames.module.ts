import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown'; 
import { addgamesComponent } from './addgames.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [addgamesComponent],
  imports: [
    CommonModule,CKEditorModule,NgMultiSelectDropDownModule,
    SharedModule,
    RouterModule.forChild([{
      path: '',
      component: addgamesComponent
    }])
  ]
})
export class addgamesModule { }
