import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { gamelistComponent } from './gamelist.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [gamelistComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([{
      path: '',
      component: gamelistComponent
    }])
  ]
})
export class gamelistModule { }
