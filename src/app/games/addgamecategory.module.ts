import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { addgamecategoryComponent } from './addgamecategory.component';
import { RouterModule } from '@angular/router';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [addgamecategoryComponent],
  imports: [
    CommonModule,CKEditorModule,
    SharedModule,
    RouterModule.forChild([{
      path: '',
      component: addgamecategoryComponent
    }])
  ]
})
export class addgamecategoryModule { }
