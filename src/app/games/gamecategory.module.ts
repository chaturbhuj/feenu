import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { gamecategoryComponent } from './gamecategory.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [gamecategoryComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([{
      path: '',
      component: gamecategoryComponent
    }])
  ]
})
export class gamecategoryModule { }
