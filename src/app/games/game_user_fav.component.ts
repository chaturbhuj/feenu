import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FormGroup, FormControl } from "@angular/forms";
import { MatButtonToggleChange } from '@angular/material';
import { PanelServices } from '../services/panel.service';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ChangeEvent } from '@ckeditor/ckeditor5-angular/ckeditor.component';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import * as _ from 'lodash';
declare var jquery: any;
declare var $ : any;
@Component({
  selector: 'app-game_user_fav',
  templateUrl: './game_user_fav.component.html',
  styleUrls: ['./game_user_fav.component.css']
})
export class game_user_favComponent implements OnInit {
	loader;
	public gamelist: Array<number>;
	public pages: Array<number>;
	public page: number = 0;
	public pageSize: number = 3;
	product_images = '';
	csvChangedEvent: any = '';
	croppedImage: any;
	csv = '';
	search_string: any;
	search ='no';
	game_id ='';
	userlist: any;
	arr = [];
	skip = 0;
	limit = 20;
	count: any;
	index1 = 1;
	skipvalue: any;
	
	constructor(public router: Router,private activatedRoute: ActivatedRoute,
              private panelService: PanelServices) {
					$('body').removeClass('body_img');
					
					this.game_id = activatedRoute.snapshot.params.game_id;
					console.log(this.game_id);
					
			}
 	ngOnInit(){ 
		
		this.games_user_fav(); 
		
	}
	

	
	onPhotoPickerChange(event){
	}
	onSaveCSV(){
	}
	games_user_fav(){
		
		var normalizeSellerPayload = {
			'game_id': this.game_id,
			'apiname': 'get_game_fav_list'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				var arr = [];
				$.each(data.data, function(key, value) {
					arr = value;
				});
			this.userlist = arr;	
            console.log(this.userlist);			
		});
	}
	
  
  
}
