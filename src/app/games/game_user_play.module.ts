import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown'; 
import { game_user_playComponent } from './game_user_play.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [game_user_playComponent],
  imports: [
    CommonModule,CKEditorModule,NgMultiSelectDropDownModule,
    SharedModule,
    RouterModule.forChild([{
      path: '',
      component: game_user_playComponent
    }])
  ]
})
export class game_user_playModule { }
