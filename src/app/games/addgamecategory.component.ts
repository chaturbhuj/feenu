import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FormGroup, FormControl } from "@angular/forms";
import { PanelServices } from '../services/panel.service';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ChangeEvent } from '@ckeditor/ckeditor5-angular/ckeditor.component';

declare var jquery: any;
declare var $ : any;
@Component({
  selector: 'app-addgamecategory',
  templateUrl: './addgamecategory.component.html',
  styleUrls: ['./addgamecategory.component.css']
})
export class addgamecategoryComponent implements OnInit {
	public Editor = ClassicEditor;
    game_category_name = '';
	created_date = '';
	created_time = '';
	category_slug = '';
	content = '';
	category_meta_keywords = '';
	category_meta_description = '';
	game_category_id=0;
	gamecategorylist= '';
	cat_id;
	 editorData = '';
	loader;
	constructor(public router: Router,private activatedRoute: ActivatedRoute,
              private panelService: PanelServices) {
				$('body').removeClass('body_img');
					this.cat_id = activatedRoute.snapshot.params.cat_id;
					if(this.cat_id != 0){
						this.get_game_by_id();
					}
			}
 
	ngOnInit(){ 
		var d = new Date();
		var month = d.getMonth()+1;
		var day = d.getDate();

		this.created_date = d.getFullYear() + '-' + (month<10 ? '0' : '') + month + '-' + (day<10 ? '0' : '') + day;
		this.created_time = (d.getHours()<10 ? '0' : '') + d.getHours() + ":" + (d.getMinutes()<10 ? '0' : '') + d.getMinutes() + ":" + (d.getSeconds()<10 ? '0' : '') + d.getSeconds();
		
		
		//this.get_all_game_category_list(); 
		
		  $('.game_category_name').keyup(function(){  
		     this.category_slug =  $(this).val().toLowerCase().replace(/ /g, '-');
			console.log(this.category_slug);
			$('.category_slug_').val(this.category_slug);  
		});  
		
	}

	
	get_game_by_id(){
		this.loader = true;
		var normalizeSellerPayload = {
			'cat_id': this.cat_id,
			'apiname': 'get_cat_game_by_id'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				this.game_category_name = data.data[0].game_category_name;
				this.category_meta_description = data.data[0].category_meta_description;
				this.category_slug = data.data[0].category_slug;
				this.content = data.data[0].content;
		});
	}
	public onChangeckeditor( { editor }: ChangeEvent ) {
		this.content = editor.getData();
	}
	
	onSubmit(){
		this.category_slug = $('.category_slug_').val(); 
		if(!this.game_category_name ){
		  this.panelService.openSnackBar('Game category name is required', 'Ok','errorMsg');
		  return;
		}
		
		/* if(!this.category_meta_keywords){
		  this.panelService.openSnackBar('Game category meta keywords is required', 'Ok','errorMsg');
		  return;
		} */
		if(!this.category_meta_description){
		  this.panelService.openSnackBar('Game category meta description is required', 'Ok','errorMsg');
		  return;
		}
		this.loader = true;
		var normalizeSellerPayload = {
			'game_category_id': this.cat_id,
			'game_category_name': this.game_category_name,
			'category_slug': this.category_slug,
			'category_meta_keywords': this.category_meta_keywords,
			'category_meta_description': this.category_meta_description,
			'content': this.content,
			'created_date': this.created_date,
			'created_time': this.created_time,
			'apiname': 'add_game_category'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				if(data.status == 201){
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					$("#browseNewCategory").modal('hide');
					$("#editCategory").modal('hide');
					$('.close_btn').trigger('click');
					this.panelService.openSnackBar(data.message, 'OK','successMsg');
					
                    this.game_category_name = '';					
                    this.category_slug = '';					
                    this.category_meta_keywords = '';					
                    this.category_meta_description = '';
                    this.content = '';
                  this.router.navigate(['gamecategory']);					
				}
		});
		
  }
  
  removegamecategory(game_category_id){
	  
	  	var val = confirm("Do you want to delete");
		if(val == true){
		this.loader = true;
		var normalizeSellerPayload = {
			'apiname': 'delete_game_category',
			'game_category_id': game_category_id
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
			this.loader = false;
				if(data.status == 201){
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					$('.close_btn').trigger('click');
					this.ngOnInit();  
					this.panelService.openSnackBar(data.message, 'OK','successMsg');
				}
		});
			return true;
		}else{
			return false;
		}
	}
	
	editgamecategory(game_category_id, game_category_name,category_slug,category_meta_keywords,category_meta_description){
		this.game_category_id = game_category_id;
		this.game_category_name = game_category_name;
		this.category_slug = category_slug;
		this.category_meta_keywords = category_meta_keywords;
		this.category_meta_description = category_meta_description;
	}
  
	addNewCategory(){
		this.game_category_id = 0;
		this.game_category_name = '';
		this.category_slug = '';
		this.category_meta_keywords = '';
		this.category_meta_description = '';
	}
  
}
