import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown'; 
import { game_user_favComponent } from './game_user_fav.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [game_user_favComponent],
  imports: [
    CommonModule,CKEditorModule,NgMultiSelectDropDownModule,
    SharedModule,
    RouterModule.forChild([{
      path: '',
      component: game_user_favComponent
    }])
  ]
})
export class game_user_favModule { }
