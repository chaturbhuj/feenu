import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { FormGroup, FormControl } from "@angular/forms";
import { PanelServices } from '../services/panel.service';
import { ImageCroppedEvent } from 'ngx-image-cropper';
declare var jquery: any;
declare var $ : any;
@Component({
  selector: 'app-gamelist',
  templateUrl: './gamelist.component.html',
  styleUrls: ['./gamelist.component.css']
})
export class gamelistComponent implements OnInit {
	loader;
	public gamelist: Array<number>;
	public pages: Array<number>;
	public page: number = 0;
	public move: number = 0;
	public current_page: number = 0;
	public pageSize: number = 3;
	product_images = '';
	game_status = 'All';
	csvChangedEvent: any = '';
	croppedImage: any;
	csv = '';
	search_string: any;
	search ='no';
	arr = [];
	skip = 0;
	limit = 20;
	totalcount: number = 0;
	count: any;
	index1 = 1;
	skipvalue: any;
	tog: any;
	toggame_title: any;
	pageno = 3;
	constructor(public router: Router,
              private panelService: PanelServices) {
				$('body').removeClass('body_img');
			  }

	ngOnInit(){ 
		this.get_all_game_list(this.skip,this.limit); 
	
	
				   
		
	}
	
	game_status_change($event){
		 //console.log($event);
		 this.game_status = $event;
		this.go_to_search_page_query(0,10);
   }
	
	setpage(i, event: any) {
		//event.prevendDefult();
		this.current_page = i;
		this.pageno = i +2;
		console.log(i);
		if(i == 0){
			this.page = ((i*20));
		}else{
			this.page = ((i*20)+1);
		}
		
		this.index1 = ((i*20)+1);
		console.log(this.page);

		this.get_all_game_list(this.page,this.limit);
		   for (var j = 1; j < i ; j++) {
		       $('.selectpage'+j).addClass('display_none');
			} 
     	  
		console.log('pageno: '+this.pageno);
		console.log('current_page: '+this.current_page);
		console.log('i: '+i);  
     	   
	}

	
	get_all_game_list(skip,limit){
		this.loader = true;
		var normalizeSellerPayload = {
			'skip':skip,
			'limit':limit,
			'apiname': 'list_games'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				this.gamelist = data.data;
				this.count = Array(Math.ceil(data.count/this.limit)).fill(0).map((x,i)=>i);
				this.move = Math.ceil(data.count/this.limit);
				this.pages = data.count;
				
				this.totalcount = (Math.round(data.count/this.limit));
				
				
		});
	}
	pagination_forword(){
		console.log(this.page);
	   this.pageno = this.pageno +3;
		
	}
	
	keyDownFunction(event) {
		 console.log('nooooooooo');
	  if(event.keyCode == 13) {
		  console.log('yesss');
		  this.go_to_search_page_query(0 ,10);
		//$('.search_icon').trigger('click');
		// rest of your code
	  }
	}
	
	
	onSaveCSV(){
	
	} 
	removegame(game_id){
	
		this.loader = true;
		var normalizeSellerPayload = {
			'apiname': 'game_play_reset',
			'game_id': game_id
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
			this.loader = false;
				
				if(data.status == 201){
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					this.panelService.openSnackBar(data.message, 'OK','successMsg');
				}
		});
	
}


time_of_game(game_id,game_title){
	
		this.loader = true;
		var normalizeSellerPayload = {
			'apiname': 'time_of_game',
			'game_id': game_id
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
			this.loader = false;
				if(data.status == 201){
					this.toggame_title = game_title;				   
				    this.tog = 0 ;
					$('#tog').modal('show');
				}else{
					this.toggame_title = game_title;
					$('#tog').modal('show');
					console.log(data.new_arr[0].duration)
					this.tog = ((data.new_arr[0].duration/data.user_count));
					console.log(data.user_count);
					console.log(data.new_arr[0].duration);
				}
		});
	
}
	
	
	
	onPhotoPickerChange(ev){
		  
		let file = ev.target.files[0];
		  console.log(file);
		  console.log(file.name);
		let fs = new FileReader();
		fs.onload = (data:any)=>{
		
		  $('#profile_input').val('');
			var normalizeSellerPayload = {
				'csvfile': file.name,
				'apiname': 'add_csv'
			}
			
			this.panelService.comman_service_funcation(normalizeSellerPayload)
				.subscribe( data => {
					$('#browseCsv .close').trigger('click');
					$('#browseCsv').removeClass('show');
					//this.get_all_game_list(); 
				if(data.status == 201){
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					this.panelService.openSnackBar(data.message, 'OK','successMsg');
				  // this.get_all_game_list();
				}
		  
			});
		};
		fs.readAsDataURL(file);
	}
	
	showpage(game_id, page){
		console.log(game_id);
		console.log(page);
		
		
		var normalizeSellerPayload = {
			'game_id': game_id,
			'page': page,
			'apiname': 'add_game_home_page'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				if(data.status == 201){
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					this.panelService.openSnackBar(data.message, 'OK','successMsg');
				 this.get_all_game_list(this.page,this.limit); 
				}
		});
		
	}
	
	go_to_search_page_query(skip,limit){
		this.search = 'yes';
		console.log(this.search_string);
		
		console.log(skip,limit);
		if(!this.search_string){
		  this.search_string = ' ';
			console.log('yessssssss');
		}
		this.loader = true;
			var normalizeSellerPayload = {
				'searchby': this.search_string,
				'game_status': this.game_status,
				'skip': skip,
				'limit': 20,
				'apiname': 'search_game_in_gamelist'
			}
			this.panelService.comman_service_funcation(normalizeSellerPayload)
				.subscribe( data => {
					this.loader = false;
					console.log(this.gamelist);
					
						this.gamelist = data.data;
						console.log(this.gamelist);
						this.totalcount = (Math.round(data.count/this.limit));
						console.log('this.totalcount');
						console.log(this.totalcount);
						if(data.data.length <= 4){
						this.count = Array(Math.round(5/this.limit)).fill(0).map((x,i)=>i);
						}else{
						this.count = Array(Math.round(data.count/this.limit)).fill(0).map((x,i)=>i);
						 
					 }
				
					console.log(this.count);
				 });
	    
	}
	/* callComponentMethodHere(i){
		console.log(i);
		
		 var normalizeSellerPayload = {
			'game_id': i,
			'apiname': 'add_game_home_page'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
			
		}); 
		
	} */
	setsearchpage(i, event: any){
		this.current_page = (i);
		if(i == 0){
			this.skipvalue = ((i*20));
		}else{
			this.skipvalue = ((i*20)+1);
		}
		
		console.log(this.skipvalue);
		this.index1 = ((i*20)+1);
		
		this.go_to_search_page_query(this.skipvalue,10); 
		console.log('pageno: '+this.pageno);
		console.log('current_page: '+this.current_page);
		console.log('i: '+i);
	}
	
  remove_game(game_id){
	  this.loader = true;
		var normalizeSellerPayload = {
			'apiname': 'game_delete',
			'game_id': game_id
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
			this.loader = false;
				
				if(data.status == 201){
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					this.panelService.openSnackBar(data.message, 'OK','successMsg');
				}
		});
  }
  
  
  
	get_all_game_total_list(){
		
		var normalizeSellerPayload = {
			'apiname': 'games_list_total'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				console.log(data.data);
				this.move = Math.ceil(data.data/this.limit);
				this.pages = data.data;
				/*  if(data.data > 140){
					for (var i = 8; i < this.move; i++) {
						console.log('.selectpage'+i);
						$('.selectpage'+i).addClass('display_none');
						
                       }
				}  */
			this.count = Array(Math.ceil(data.data/this.limit)).fill(0).map((x,i)=>i);
			console.log(Math.ceil(data.data/this.limit));
			
			this.totalcount = data.data.length;
			console.log(this.count);
			//console.log(parseFloat(data.data/this.limit));
            			
		});
	}
	
	base64_serverimg(img1){
		var imgUrl = 'https://www.google.de/images/srpr/logo11w.png';
	var bb =  this.getBase64Image(imgUrl);
	   console.log(bb);

		
	   
	}
	getBase64Image(imgUrl) {
		console.log(imgUrl);
	 var img = new Image();
		img.src = imgUrl;
		img.setAttribute('crossorigin', 'anonymous');
		var canvas = document.createElement("canvas");
		canvas.width = img.width;
		canvas.height = img.height;
		var ctx = canvas.getContext("2d");
		ctx.drawImage(img, 0, 0);
		var dataURL = canvas.toDataURL("image/png",1);
		console.log(canvas);
		console.log(dataURL);
    //return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
  }
  
}
