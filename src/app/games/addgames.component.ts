import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FormGroup, FormControl } from "@angular/forms";
import { MatButtonToggleChange } from '@angular/material';
import { PanelServices } from '../services/panel.service';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ChangeEvent } from '@ckeditor/ckeditor5-angular/ckeditor.component';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import * as _ from 'lodash';
declare var jquery: any;
declare var $ : any;
@Component({
  selector: 'app-addgames',
  templateUrl: './addgames.component.html',
  styleUrls: ['./addgames.component.css']
})
export class addgamesComponent implements OnInit {
	loader;
	public Editor = ClassicEditor;
	dropdowntagList = [];
	dropdowntagList1 = [];
	selectedtagItems = [];
	dropdowntagSettings = {};
	 editorData = '';
	 editorData1 = '';
	imageChangedEvent: any = '';
	imageChangedEvent1: any = '';
	croppedImage: any;
	croppedImage1: any;
	game_title = '';
	game_image = '';
	games_img_src = '';
	games_img_jpg_src = '';
	game_image_mobile = '';
	created_date = '';
	created_time = '';
	game_url = '';
	games_content='';
	games_content1='';
	game_tag='';
	game_walkthrough='tubia';
	game_orientation='both';
	game_height='600';
	game_category_id = '';
	game_status = 'Inactive';
	staff_id = 0;
	game_id = 0;
	meta_keyword = '';
	game_slug = '';
	desktop_friendly = 'Yes';
	mobile_friendly = 'No';
	youtube_link = '';
	play_store_url = '';
	apple_store_url = '';
	
	game_category_name = '';
	gamecategorylist = '';
	meta_description = '';
	meta_tags = '';
	games_control = '';
	game_width = '';
	game_slugs = '';
	added_by_game = '';
	localimgname = '';
	
	constructor(public router: Router,private activatedRoute: ActivatedRoute,
              private panelService: PanelServices) {
					$('body').removeClass('body_img');
					this.game_slugs = activatedRoute.snapshot.params.game_slug;
					this.game_id = activatedRoute.snapshot.params.games_id;
					console.log(this.game_id);
					var admin_data = JSON.parse(localStorage.getItem("FeenuAdminData"));
					console.log(admin_data.user.name);
					this.added_by_game = admin_data.user.name;
			}
 
	ngOnInit(){   
		var d = new Date();
		var month = d.getMonth()+1;
		var day = d.getDate();

		this.created_date = d.getFullYear() + '-' + (month<10 ? '0' : '') + month + '-' + (day<10 ? '0' : '') + day;
		this.created_time = (d.getHours()<10 ? '0' : '') + d.getHours() + ":" + (d.getMinutes()<10 ? '0' : '') + d.getMinutes() + ":" + (d.getSeconds()<10 ? '0' : '') + d.getSeconds();
		
		
		if(this.game_id == 0){
		   
			 $('.game_title').keyup(function(){  
				 var gameslug =  $(this).val().toLowerCase().replace(/ /g, '-');
				 this.game_slug =  gameslug.toLowerCase().replace(/'/g, '-');
				console.log(this.game_slug);
				$('.game_slug').val(this.game_slug);  
			});
		}else{
			this.get_game_by_id();
			 $('.game_title').keyup(function(){  
				 var gameslug =  $(this).val().toLowerCase().replace(/ /g, '-');
				 this.game_slug =  gameslug.toLowerCase().replace(/'/g, '-');
				console.log(this.game_slug);
				$('.game_slug').val(this.game_slug);  
			});
		}
		this.get_all_game_category_list();
		this.get_all_tag_list();
		//this.get_jpg_image();
		
		$('.select_game_walkthrough').on('change', function() {
			console.log('yes');
			var select_val = $(this).val();
		
			if(select_val == 'youtube'){
				$('.youtube_link').removeClass('display_none');
			}
			
		});
		
		
		
	}
	
	/* game_slug_click(){
		
		if(this.game_title){
	     this.game_slug =  this.game_title.toLowerCase().replace(' ', '-').replace(/[0-9]/g, '');
		}	
	} */
	
	onItemSelect(item: any) {
    console.log(item);
  }
  onSelectAll(items: any) {
    console.log(items);
  }
  get_all_tag_list(){
		this.loader = true;
		var normalizeSellerPayload = {
			'apiname': 'list_tags'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				this.dropdowntagList1 = data.data;
				console.log(this.dropdowntagList1);
				this.get_all_tag_by_id(this.dropdowntagList1);
		});
	}
  
	
	get_all_tag_by_id(dropdowntagList1){
		var normalizeSellerPayload = {
			'apiname': 'select_tag_id_game',
			'game_id': this.game_id
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.dropdowntagList = dropdowntagList1;
				 console.log(this.dropdowntagList);
				// console.log(data.data);
				 var myarr = [];
				 $.each(data.data, function(key, value) {
					 console.log(value);
						myarr.push({
							_id:value.tag_id,
							tag_slug:value.tag_slug
						}); 
						
					});
				 console.log(myarr);
				this.selectedtagItems = myarr;
				console.log(this.dropdowntagList);
				this.dropdowntagSettings = {
				  singleSelection: false,
				  idField: '_id',
				  textField: 'tag_slug',
				  selectAllText: 'Select All',
				  unSelectAllText: 'UnSelect All',
				  itemsShowLimit: 20,
				  allowSearchFilter: true
				};
		});
	}
	
	get_all_game_category_list(){
		this.loader = true;
		var normalizeSellerPayload = {
			'apiname': 'list_game_category'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				this.gamecategorylist = data.data;
				//console.log(this.gamecategorylist);
		});
	}
	img_convert_jpg(){
		console.log('yesss');
	var base = this.getBase64Image(document.getElementById("gameimage"));
	console.log(base);
	}
	getBase64Image(img){
	  console.log(img);
		var canvas = document.createElement("canvas");
		canvas.width = img.width;
		canvas.height = img.height;
		var ctx = canvas.getContext("2d");
		ctx.drawImage(img, 0, 0);
		var dataURL = canvas.toDataURL("image/png");

		return dataURL;
  }
	
	get_game_by_id(){
	this.loader = true;
		var normalizeSellerPayload = {
			'apiname': 'get_game_by_id',
			'game_id': this.game_id
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
			this.loader = false;
			console.log(data.data);
			 this.game_category_id = data.data.game_category_id;
			// this.onmoduleChange(this.game_category_id);
			this.game_category_name = data.data.game_category_name;
			this.game_title = data.data.game_title;
			this.game_image = data.data.game_image;
			//this.game_image_mobile = data.data.game_image_mobile;
			this.game_image_mobile = data.data.game_image_mobile;
			this.games_img_src = (this.game_image);
			this.games_img_jpg_src = (this.game_image_mobile);
			this.games_content = data.data.game_desc;
			this.game_url = data.data.game_url;
			this.game_tag = data.data.game_tag;
			this.game_orientation = data.data.game_orientation;
			this.meta_tags = data.data.meta_tags;
			this.meta_description = data.data.meta_description;
			this.meta_keyword = data.data.meta_keyword;
			this.game_height = data.data.game_height;
			this.games_control = data.data.games_control;
			this.game_slug = data.data.game_slug;
			this.mobile_friendly = data.data.mobile_friendly;
			this.desktop_friendly = data.data.desktop_friendly;
			this.play_store_url = data.data.play_store_url;
			this.youtube_link = data.data.youtube_link;
			this.apple_store_url = data.data.apple_store_url;
			this.game_width = data.data.game_width;
			this.game_status = data.data.game_status;
			this.game_walkthrough = data.data.game_walkthrough;
			
			if(this.game_walkthrough == 'youtube'){
				$('.youtube_link').removeClass('display_none');
			}
				
		});
	}

  
	public onChangeckeditor( { editor }: ChangeEvent ) {
		this.games_content = editor.getData();
	}
	public onChangeckeditor1( { editor }: ChangeEvent ) {
		this.games_control = editor.getData();
	}
	
	onmoduleChange($event) {
		this.game_category_id = (this.game_category_id);
        this.game_category_name = _.filter(this.gamecategorylist, {
            "_id": (this.game_category_id)
        })[0].game_category_name;
		console.log(this.game_category_name);
		console.log(this.game_category_id);
	}
	
	onSubmit(){
		console.log(this.selectedtagItems);
		this.game_slug = $('.game_slug').val();  
		
		if(!this.game_category_id ){
		  this.panelService.openSnackBar('Game category is required', 'Ok','errorMsg');
		  return;
		}
		
		if(!this.game_title ){
		  this.panelService.openSnackBar('Game title is required', 'Ok','errorMsg');
		  return;
		}
		if(!this.game_image){
		  this.panelService.openSnackBar('Game Image is required', 'Ok','errorMsg');
		  return;
		}
		if(!this.games_content){
		  this.panelService.openSnackBar('Games Content is required', 'Ok','errorMsg');
		  return;
		}
		if(!this.game_url){
		  this.panelService.openSnackBar('Game Url is required', 'Ok','errorMsg');
		  return;
		}
		if(!this.game_walkthrough){
		  this.panelService.openSnackBar('Game Walkthrough is required', 'Ok','errorMsg');
		  return;
		}
		if(!this.game_orientation){
		  this.panelService.openSnackBar('Game Orientation is required', 'Ok','errorMsg');
		  return;
		}
		if(!this.game_height){
		  this.panelService.openSnackBar('Game height is required', 'Ok','errorMsg');
		  return;
		}
		if(!this.game_width){
		  this.panelService.openSnackBar('Game width is required', 'Ok','errorMsg');
		  return;
		}
		/* if(!this.game_tag){
		  this.panelService.openSnackBar('Game Tag is required', 'Ok','errorMsg');
		  return;
		} */
		if(!this.game_status){
		  this.panelService.openSnackBar('game status is required', 'Ok','errorMsg');
		  return;
		}
		//this.loader = true;
		var normalizeSellerPayload = {
			'game_id': this.game_id,
			'game_category_id': this.game_category_id,
			'game_category_name': this.game_category_name,
			'game_title': this.game_title,
			'game_image': this.game_image,
			'game_image_mobile': this.game_image_mobile,
			'game_desc': this.games_content,
			'game_url': this.game_url,
			'game_tag': this.game_tag,
			'selectedtag': this.selectedtagItems,
			'game_walkthrough': this.game_walkthrough,
			'game_orientation': this.game_orientation,
			'meta_tags': this.game_tag,
			'meta_description': this.games_content,
			'meta_keyword': this.meta_keyword,
			'game_height': this.game_height,
			'games_control': this.games_control,
			'game_slug': this.game_slug,
			'mobile_friendly': this.mobile_friendly,
			'desktop_friendly': this.desktop_friendly,
			'play_store_url': this.play_store_url,
			'youtube_link': this.youtube_link,
			'apple_store_url': this.apple_store_url,
			'game_width': this.game_width,
			'game_status': this.game_status,
			'default_rating': '4.5',
			'actual_rating': '0',
			'number_of_users': '0',
			'final_rating': '4.5',
			'game_show': 'other',
			'created_date':this.created_date,
			'created_time':this.created_time,
			'added_by_game':this.added_by_game,
			'apiname': 'add_games' 
		}
		
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				if(data.status == 201){
				
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					this.panelService.openSnackBar(data.message, 'OK','successMsg');
					if(this.game_id == 0){
						this.router.navigate(['gamelist']);
					}else{
						
					}
					
					
				}
		});
		
  }
  imgpopselect(){
	  console.log("imgpopselect");
	  $('.mycropimageuploadinput').trigger('click');
	  
	  
  }
  fileChangeEvent(event: any): void {
	  console.log(event);
		this.imageChangedEvent = event;
		
		console.log(this.imageChangedEvent);
		//this.imageChangedEvent1 = event;
	}

	imageCropped(event: ImageCroppedEvent) {
			this.croppedImage = event.base64;
			//this.croppedImage1 = event.base64;
			console.log('11111');
	}
	
	
	
 onSaveCropImage(){
	 //console.log(this.croppedImage);
	 //console.log(this.croppedImage1);
	 this.loader = true;
		 const normalizeSellerPayload = {
			'image_base_64': this.croppedImage,
			'folder_name': 'game-thumbs',
			'apiname': 'googlecoludImagebase64'
		}
		$('#close_register_modal').trigger('click');
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
		
			this.game_image = (data.imgname);
			this.games_img_src = (data.imgname);
			this.games_img_jpg_src = (data.myImage_jpg);
			this.game_image_mobile = (data.myImage_jpg);
			
			console.log(this.games_img_jpg_src);
			//this.uploadprofileimage(this.games_img_jpg_src);
			this.base64_serverimg(this.games_img_jpg_src);
			$('.mycropimageuploadinput').val('');
			$('.source-image').attr('src','');
			this.croppedImage = '';
			this.fileChangeEvent('ok');
			setTimeout(
				this.loader = false ,
					  1000
					);
		}); 
		//$("#registerImage").modal('hide');
        $('.close').trigger('click');		
	} 
	
	uploadprofileimage(img){
		console.log(img);
		const normalizeSellerPayload = {
			'image_base_64': img,
			'apiname': 'download_pro_img'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe(data => {
				console.log(data.imgname);
			
			});
	}
	
	base64_serverimg(img){
		var canvas = document.createElement("canvas");
		console.log(img);
		//  canvas.width = img.width;
		 // canvas.height = img.height;
		///  var ctx = canvas.getContext("2d");
		//  ctx.drawImage(img, 0, 0);
		 // var dataURL = canvas.toDataURL("image/png");
		 // console.log('dataURL');
		//  console.log(dataURL);
  //return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
	   
	}
  
  
}
