import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { MetaConfig, MetaService } from 'ng2-meta';
import { FormGroup, FormControl } from "@angular/forms";

import { PanelServices } from '../services/panel.service';

declare var jquery: any;
declare var $ : any;
@Component({
  selector: 'app-gamedetail',
  templateUrl: './gamedetail.component.html',
  styleUrls: ['./gamedetail.component.css']
})
export class gamedetailComponent implements OnInit {
	 rewardData:any = {};
    game_category_name = '';
	created_date = '';
	created_time = '';
	gameplaylist = '';
	game_category_id=0;
	gamelist= '';
	first_name = '';
	last_name = '';
	email_id = '';
	pass = '';
	rating_value = '';
	rating_value_data = 0;
	confirm_pass = '';
	gender = '';
	userstatus = '';
	user_comment = '';
	report_message = '';
	user_email = '';
	google_plus_link = '';
	selected_tag_list = '';
	twitter_link = '';
	fb_link = '';
	admin_img_src = '';
	user_password = '';
	gamedetail = '';
	game_title = '';
	game_image = '';
	games_content = '';
	report_subject = '';
	game_orientation = '';
	game_url = '';
	game_tag = '';
	iframe = '';
	game_width:number = 0;
	game_height = '';
	game_slug = '';
	count_user = 0;
	games_img_src = '';
	game_image_mobile = '';
	profile_show = '';
	actual_rating; 
	default_rating = 0;
	number_of_users = 0;
	final_rating = 0;
	start_time = 0;
	insertid:any = {};
	gamecommentlist = '';
	gamecommentlist1 = '';
	gamecommentlist2 = '';
	game_id = 0;
	user_id = '';
	username = 'Guest user';
	new_rating_like:number = 0;
	user_name = '';
	count = 0;
	countdislike = 0;
	getfavirate = 0;
	use = 0;
	users = 0;
	new_rating = 0;
	data_rating_value = 0;
	like_dislike = '';
	user_value = '';
	mobile_friendly='';
	game_walkthrough = '';
	user_image = '';
	profilepicturestatus = '';
	youtube_link = '';
	cate_game_list:any;
	games_control = '';
	play_store_url = '';
	apple_store_url = '';
	urlback = '';
	game_status = '';
	cate_game_limit = 0;
	window_size = 0;
	edit_comment = '';
	comment_id = '';
	check_game_delete = 'no';
	check_device = '';
	populargamelist = '';
	cate_game_latest = '';
	loader;
	randomNumber:any;
	
	constructor(private location: Location,public router: Router,private activatedRoute: ActivatedRoute,private metaService: MetaService,
              private panelService: PanelServices) {
				$('body').removeClass('body_img');
            localStorage.setItem('seachdata' , JSON.stringify(''));
				this.game_slug = activatedRoute.snapshot.params.games_id;
				console.log("this.game_slug");
				console.log(activatedRoute.snapshot.params);
				console.log(activatedRoute.snapshot);
				console.log(this.game_slug);
				console.log(this.urlback);
				var user_play_game = JSON.parse(localStorage.getItem('user_play_game'));
               console.log(user_play_game);
				 var user_data = JSON.parse(localStorage.getItem("userData"));
				 console.log(user_data);
				if(user_data){
				if(user_data== null){
					
					}else if(user_data[0]._id){
						
					}
				this.user_id = user_data[0]._id;
				this.user_name = user_data[0].first_name;
				this.email_id = user_data[0].email_id;
				this.userstatus = user_data[0].userstatus;
				this.gender = user_data[0].gender;
				
				console.log(this.user_id);
				console.log(this.user_name);
			 }
			 
					
				 
			
			}
 
	ngOnInit(){ 
		
		console.log("this.game_slug ngOnInit");
		console.log(this.game_slug);
				
		var d = new Date();
		var month = d.getMonth()+1;
		var day = d.getDate();

		this.created_date = d.getFullYear() + '-' + (month<10 ? '0' : '') + month + '-' + (day<10 ? '0' : '') + day;
		
		this.created_time = (d.getHours()<10 ? '0' : '') + d.getHours() + ":" + (d.getMinutes()<10 ? '0' : '') + d.getMinutes() + ":" + (d.getSeconds()<10 ? '0' : '') + d.getSeconds();
		//$(".container").css("width", $(window).width());
		$(".container").css("margin", 'margin: 0 auto;');
		
		this.window_size = $(window).width();
		this.get_all_gamedetails_list(); 
		if(!this.panelService.checkIfUserOnMobile()){
			$('.navbar').removeClass('head1');
	        $('.footer').removeClass('foot');
		}else{
			$('.navbar').addClass('head11');
			$('.gameplay').css("width", '100%');
			$('.gameplay').css("height", 360);
			$('.gameplay').css("overflow", 'hidden');
		}
		
		window.addEventListener("hashchange", function(e) {
			if(e.oldURL.length > e.newURL.length)
				alert("back")
		});
	
	}
	
	
	
  ratingComponentClick(clickObj: any): void {
	  console.log(clickObj);
	 this.rating_value = clickObj.rating;
	
  }
  
     
  getFiltergame(){
	   if(this.panelService.checkIfUserOnMobile()){
			this.check_device = 'mobile';
		}else{
			this.check_device = 'pc';
			}
	   this.loader = true;
		var normalizeSellerPayload = {
			'check_device': this.check_device,
			'apiname': 'add_game_play_time_testing'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				console.log(data.data);
				this.populargamelist = data.data;
			});
	}
  
  
	get_all_gamedetails_list(){
		this.loader = true;
		var normalizeSellerPayload = {
			'apiname': 'by_id_game',
			'game_slug': this.game_slug
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				
				this.gamedetail = data.data;
				console.log("this.gamedetail");
				console.log(data.data);
				if(data.status == 200){
					 if(data.data.length == 0){
					 this.router.navigate(['page-not-found']);
				} 
				this.game_id = data.data[0]._id;
				
				
				
				this.game_category_id = data.data[0].game_category_id;
				this.game_category_name = data.data[0].game_category_name;
				$('.cat_btn').text(this.game_category_name);
				this.game_title = data.data[0].game_title;
				this.game_slug = data.data[0].game_slug;
				this.game_image = data.data[0].game_image;
				this.game_image_mobile = data.data[0].game_image_mobile;
				this.games_img_src = (this.game_image);
				//alert(this.game_image_mobile);
				this.games_content = (data.data[0].game_desc);
				this.games_control = (data.data[0].games_control);
				this.game_url = data.data[0].game_url;
				this.game_tag = data.data[0].game_tag;
				this.game_walkthrough = data.data[0].game_walkthrough;
				this.game_orientation = data.data[0].game_orientation;
				this.game_height = data.data[0].game_height;
				this.game_width = data.data[0].game_width;
				this.default_rating = data.data[0].default_rating;
				this.actual_rating = data.data[0].actual_rating;
				this.number_of_users = data.data[0].number_of_users;
				this.final_rating = data.data[0].final_rating;
				this.mobile_friendly = data.data[0].mobile_friendly;
				this.play_store_url = data.data[0].play_store_url;
				this.apple_store_url = data.data[0].apple_store_url;
				this.game_status = data.data[0].game_status;
				
				}
				if(this.game_status == 'DMCA'){
					$('.gameplay').addClass('display_none');
					$('.delete_game').addClass('display_none');
					$('.dcma').removeClass('display_none');
				}else if(this.game_status == 'Deleted'){
					$('.gameplay').addClass('display_none');
					$('.delete_game').removeClass('display_none');
					$('.dcma').addClass('display_none');
					
				}else{
					$('.gameplay').removeClass('display_none');
					$('.dcma').addClass('display_none');
				}
				
				if(this.game_status == 'Active'){
					$('.active_game1').removeClass('display_none');
					$('.active_game2').removeClass('display_none');
					$('.active_game3').removeClass('display_none');
					$('.active_game4').removeClass('display_none');
					$('.active_game5').removeClass('display_none');
					$('.active_game6').removeClass('display_none');
					$('.active_game7').removeClass('display_none');
					$('.active_game8').removeClass('display_none');
					$('.active_game9').removeClass('display_none');
					$('.active_game10').removeClass('display_none');
				}else if(this.game_status == 'Inactive'){
					$('.active_game1').removeClass('display_none');
					$('.active_game2').removeClass('display_none');
					$('.active_game3').removeClass('display_none');
					$('.active_game4').removeClass('display_none');
					$('.active_game5').removeClass('display_none');
					$('.active_game6').removeClass('display_none');
					$('.active_game7').removeClass('display_none');
					$('.active_game8').removeClass('display_none');
					$('.active_game9').removeClass('display_none');
					$('.active_game10').removeClass('display_none');
					
				}else{
					$('.active_game1').addClass('display_none');
					$('.active_game2').addClass('display_none');
					$('.active_game3').addClass('display_none');
					$('.active_game4').addClass('display_none');
					$('.active_game5').addClass('display_none');
					$('.active_game6').addClass('display_none');
					$('.active_game7').addClass('display_none');
					$('.active_game8').addClass('display_none');
					$('.active_game9').addClass('display_none');
					$('.active_game10').addClass('display_none');
				}
				
				
				if(this.play_store_url){
					$('.google_play').removeClass('display_none');
				}
				if(this.apple_store_url){
					$('.app_store').removeClass('display_none');
				}
				
				if(!this.panelService.checkIfUserOnMobile()){
					$(".game_img_height").css("height", this.game_height);
					$(".iframewidth").removeClass("display_none");
						/* 150 350 */
						if(this.game_width <= 600){
							var lesswidth = this.window_size - 600;
							
							$(".iframewidth").css("width", 600);
							
							console.log("lesswidth = = "+lesswidth)
							if(lesswidth > 700){ 
								//$(".iframewidth1").css("width", 162);
								$(".iframewidth1").removeClass("display_none");
								$(".iframewidth2").removeClass("display_none");
							}else if(lesswidth > 500){
								$(".iframewidth1").addClass("display_none");
								$(".iframewidth2").removeClass("display_none");
							}else if(lesswidth > 150){
								$(".iframewidth1").removeClass("display_none");
								$(".iframewidth2").addClass("display_none");
							}else{
								$(".iframewidth1").addClass("display_none");
								$(".iframewidth2").addClass("display_none");									
							}
						/*
							if(lesswidth >= 100){
								$(".iframewidth2").css("width", lesswidth);
							}else{
								$(".iframewidth2").addClass('display_none');	
							}
						}else if(this.game_width >= 600 && this.game_width <= 800) {
								var lesswidth = this.window_size - this.game_width;
							$(".iframewidth").css("width", this.game_width);
							$(".iframewidth1").addClass('display_none');
							$(".container").css("margin-left", 50);
							if(lesswidth >= 100){
								$(".iframewidth2").css("width", lesswidth);
							}else{
							  $(".iframewidth2").addClass('display_none');	
							}
						}else if(this.game_width >= 800 && this.game_width <= 1000) { 
								var lesswidth = this.window_size - this.game_width;
							$(".iframewidth").css("width", this.game_width);
							$(".container").css("margin-left", 50);
						    if(lesswidth >= 200 && lesswidth <= 600){
								$(".iframewidth1").addClass('display_none');
								$(".iframewidth2").css("width", lesswidth);
							}else if(lesswidth >= 600 && lesswidth <= 800) {
								
								$(".iframewidth1").css("width", 150);
								$(".iframewidth2").css("width", lesswidth-290);
							}else{
								$(".iframewidth2").removeClass('display_none');
								$(".iframewidth1").css("width", 300);
							}
							//$(".iframewidth1").removeClass('display_none');
							//$(".iframewidth2").addClass('display_none');
						
						}else if(this.game_width > 1000) {
								var lesswidth = this.window_size - this.game_width;
							$(".iframewidth").css("width", this.window_size);
							//$(".game_detail_title").css("width", this.game_width);
							//$(".iframewidth1").css("width", 162);
							$(".iframewidth1").addClass('display_none');
							$(".iframewidth2").addClass('display_none');
						     $(".iframewidth").css("padding-left", 50);
						     $(".iframewidth").css("padding-right", 50);*/
						}else{
							if(this.game_width > (this.window_size - 200)){
								$(".iframewidth1").addClass("display_none");
								$(".iframewidth2").addClass("display_none");
							}else{
								var lesswidth = this.window_size - this.game_width;
								$(".iframewidth").css("width", this.game_width);
								console.log("lesswidth = = "+lesswidth)
								if(lesswidth > 700){
									//$(".iframewidth1").css("width", 162);
									$(".iframewidth1").removeClass("display_none");
									$(".iframewidth2").removeClass("display_none");
								}else if(lesswidth > 500){
									$(".iframewidth1").addClass("display_none");
									$(".iframewidth2").removeClass("display_none");
								}else if(lesswidth > 150){
									$(".iframewidth1").removeClass("display_none");
									$(".iframewidth2").addClass("display_none");
								}else{
									$(".iframewidth1").addClass("display_none");
									$(".iframewidth2").addClass("display_none");									
								}
							}
						
						}
				}else{
					$(".iframewidth1").addClass('display_none');
					$(".iframewidth2").addClass('display_none');
					$(".iframemain").addClass('mobile_play_game');
					$(".iframewidth").removeClass('display_none');
					
					//$(".iframewidth").css("width", this.window_size);
					$(".iframewidth").css("width", '100%');
					//$(".game_detail_title").css("width", this.game_width);
				
					
				}
				 
				if(data.status == 200){
				this.youtube_link = data.data[0].youtube_link.split('watch?v=')[1];	
				}
				
				 //if(this.game_orientation != 'vertical'){
					 console.log('yesrrrrrrrrrrrrrrrrrrrr');
					 if(this.panelService.checkIfUserOnMobile()){
						  this.cate_game_limit = 2;
						  $('.rotation_msg').removeClass('display_none');
						  $('.play_height').removeClass('blurimg');
						  $('.play_height').addClass('blurimg_mobile');
						 $('.rotation_msg').append(' You are using mobile, Please rotate screen, for better view of this game.');
					  }else{
						  if(this.game_width <= 600){
							  this.cate_game_limit = 6;
						  }else if(this.game_width > 600 && this.game_width <= 800){
							 this.cate_game_limit = 6; 
						  }else if(this.game_width > 800 && this.game_width <= 960){
							  this.cate_game_limit = 6;
						  }else{
							  this.cate_game_limit = 3;
						  }
						  
						  $('.rotation_msg').addClass('display_none');
						  $('.play_height').addClass('blurimg');
						  $('.play_height').removeClass('blurimg_mobile');
					  }
				 
				console.log('horizontal');
				console.log(this.game_orientation);
				
				
					$('.gameframe').empty();
					//$('.gameframe').append('<iframe src="https://html5.gamedistribution.com/e1c2844499324daebe5bcbda38d7fbd0/" style="min-width: '+this.game_width+'px; height: '+this.game_height+'px;" scrolling="none" frameborder="0"></iframe>');
					//setTimeout(function(){
                    //},1000); 
					if(this.game_width > 1100){
						$('.main_container').addClass('container-fluid');
						$('.main_container').removeClass('container');
					}
				    this.get_all_comment(); 
				    this.get_all_game_by_cate_id(); 
				    this.get_all_selected_tags(); 
					this.get_all_like(); 
					this.get_all_dislike(); 
				    this.get_count_like(); 
					this.get_count_dislike(); 
					this.get_all_fav(); 
					this.get_all_rating(); 
					this.get_user_rating(); 
					this.get_playgame(); 
					this.total_all_like(); 
					if(this.user_id){
					this.get_user_detail_by_id(); 
					this.get_all_user_unapprove_comment();
                  					
			         }
					 this.getFiltergame();
					 this.get_latest_game_by_cate();
					//this.add_game_play_time();
					
					
					if(this.game_walkthrough == 'tubia'){
						$('.tubaclink').empty();
						$('.tubaclink').append('<iframe frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen width="100%" height="600" src="https://player.tubia.com/?publisherid=5b7c63f6ee804f749c1c128b67caed39&amp;gameid='+this.game_id+'&amp;pageurl='+window.location.href+'&amp;title='+this.game_title+'&amp;coloraccent=#0000ff&amp;langcode=en-IN"></iframe>');
					}
					if(this.game_walkthrough == 'youtube'){
						$('.youtubelink').empty(); 
						//$('.youtubelink').append('<iframe src="https://www.youtube.com/embed/'+this.youtube_link+'"></iframe>');		
						$('.youtubelink').append('<iframe width="560" height="315" src="https://www.youtube.com/embed/'+this.youtube_link+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>');		
					} 
					
					this.metaService.setTitle(this.game_title + ' - Play Online Games on feenu.com');
					this.metaService.setTag('og:image',this.game_image_mobile);
					this.metaService.setTag('og:description',data.data[0].meta_description);
				
		});
	}
	
	gamewalkthrough(){
		$("html, body").animate({ scrollTop: $(document).height()-$(window).height() });
	}
	
	get_latest_game_by_cate(){
		 if(this.panelService.checkIfUserOnMobile()){
			this.check_device = 'mobile';
		}else{
			this.check_device = 'pc';
			}
		 this.loader = true;
		var normalizeSellerPayload = {
		   'game_category_id': this.game_category_id,
		   'check_device': this.check_device,
		   'apiname': 'get_latest_game_by_cate'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				this.cate_game_latest = data.data;
		});
	}
	
	
  
  get_all_game_by_cate_id(){
		 if(this.panelService.checkIfUserOnMobile()){
			this.check_device = 'mobile';
		}else{
			this.check_device = 'pc';
			}
		 this.loader = true;
		var normalizeSellerPayload = {
		   'game_category_id': this.game_category_id,
		   'check_device': this.check_device,
		   'apiname': 'get_all_game_by_cate_id'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				if(data.status == 200){
					var new_arr = [];
					var count = 0;
					$.each(data.data, function(key, value) {
						console.log(value[0]);
						if(value[0]){
							if(count < 20){
							  new_arr.push(value[0]);	
							}
							
							count++;
						}
						});
					this.cate_game_list = new_arr;
					console.log(this.cate_game_list);
				}
				
		});
	}
	
   game_reloaded(game_slug){
	  // window.location.reload();
	  $('.iframewidth2').removeClass('display_none');
	  $('.iframewidth1').removeClass('display_none');
	  $('.gameplay').removeClass('display_none');
	  $('.gameplay').removeClass('display_none');
	  $('.dcma').addClass('display_none');
	  $('.delete_game').addClass('display_none');
	 this.game_slug = game_slug;
	 this.router.navigate(['g/'+game_slug]);
	 this.ngOnInit();
	 
	 //this.get_all_gamedetails_list(); 

   }
   
  /*  go_to_product_detail_page(product_id, product_name0){
		console.log(product_id);
		this.router.navigate(['singleproduct/'+product_name0], {
		queryParams:{"pname":product_name0,"id":product_id}
		});
		this.ngOnInit();
		} */
	get_user_rating(){
		this.loader = true;
		var normalizeSellerPayload = {
			'game_id': this.game_id,
			'user_id': this.user_id,
			'apiname': 'get_user_rating'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				console.log("rating");
				console.log(data.data.length);
				this.rating_value_data = data.data.length;
					console.log(this.rating_value);
				//this.countdislike = data.data;
		});
	}
	
	get_user_detail_by_id(){
		this.loader = true;
		var normalizeSellerPayload = {
			'user_id': this.user_id,
			'apiname': 'get_user_detail_by_id'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				console.log("userData");
				console.log(data.data);
				this.user_image = data.data[0].user_image;
				this.profilepicturestatus = data.data[0].profilepicturestatus;
				console.log(this.user_image);
				//this.countdislike = data.data;
		});
	}
	
	 get_all_selected_tags(){
		 this.loader = true;
		var normalizeSellerPayload = {
		   'game_id': this.game_id,
			'apiname': 'get_all_selected_tags'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				this.selected_tag_list = data.data;
		});
	}
	select_tag_name(tag_slug){
		this.router.navigate(['t/',tag_slug]); 
	}
	
	 get_all_comment(){
		 this.loader = true;
		var normalizeSellerPayload = {
		   'game_id': this.game_id,
			'apiname': 'get_comment_by_game_id'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				this.gamecommentlist1 = data.data.reverse();
				if(this.user_id == ''){
					this.gamecommentlist = this.gamecommentlist1
				}
				
		});
		console.log(this.gamecommentlist);
	}
	
	 get_all_user_unapprove_comment(){
		 this.loader = true;
		var normalizeSellerPayload = {
		   'game_id': this.game_id,
		   'user_id': this.user_id,
			'apiname': 'get_comment_by_user_id'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				this.gamecommentlist2 = data.data.reverse();
				this.gamecommentlist = this.gamecommentlist1.concat(this.gamecommentlist2);
		  
		});
	}
	
	
	 total_all_like(){
		 this.loader = true;
		var normalizeSellerPayload = {
			'game_id': this.game_id,
			'apiname': 'total_all_like'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				console.log("like data");
				var count = 0; 
				var user_value = ''; 
				var user_id = ''; 
				var like_dislike = ''; 
				console.log(data.data);
				 user_id = this.user_id;
				$.each(data.data, function(key, value) {
					console.log(value.user_id);
					console.log(user_id);
					if(value.countlike == 'like'){
						count++;
					}
					if(value.user_id == user_id){
						 user_value = "old user";
						 console.log(value.countlike);
						like_dislike = value.countlike;
					}else{
						 user_value = "new user";
					}
					
				});
				
				console.log(user_value);
				if(user_value == 'old user'){
						console.log(like_dislike);
					 this.users = data.data.length;
					 if(like_dislike == 'like'){
						 this.data_rating_value = ((count*5)-5);
					 }
					 if(this.like_dislike == 'dislike'){
						 this.data_rating_value = ((count*5)+5);
					 }
					 this.user_value = 'olduser';
				}else{
					console.log("2");
					this.users = parseInt(data.data.length+1);
					this.data_rating_value = (count*5);
					 this.user_value = 'newuser';
				}
				console.log(this.users);
				console.log(this.data_rating_value);
		});
	}
	
	 
	
	likebtn(){
		console.log(this.users);
		console.log(this.user_value);
		console.log(this.data_rating_value);
		if(this.user_value = 'olduser'){
			this.new_rating_like = ((this.data_rating_value)/this.users);
		}
		if(this.user_value = 'newuser'){
			this.new_rating_like = ((this.data_rating_value+5)/this.users);
		}
		var new_final_rating = 0;
		//var new_rating_like = 0;
		var new_number_of_users = 0;
		 
		console.log(this.new_rating_like); 
		if(this.users <= 9){
			var new_default_rating = this.default_rating;
			var new_actual_rating = this.new_rating_like;
			 new_number_of_users = this.users;
			 new_final_rating = this.default_rating;
		}else{ 
			var new_default_rating = this.default_rating;
			var new_actual_rating = this.new_rating_like;
			 new_number_of_users = this.users;
			 new_final_rating = this.new_rating_like;
		}
		if(!this.user_id){
		  this.panelService.openSnackBar('User Login is required ', 'Ok','errorMsg');
		  return;
		}
		this.loader = true;
		var normalizeSellerPayload = {
	
			'game_id': this.game_id,
			'user_id': this.user_id,
			'default_rating': new_default_rating,
			'actual_rating': new_actual_rating,
			'number_of_users': new_number_of_users,
			'final_rating': new_final_rating,
			'countlike': 'like',
			'apiname': 'insert_like' 
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				if(data.status == 201){
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					this.panelService.openSnackBar('Like successfully', 'OK','successMsg');
					this.get_count_like();
					this.get_count_dislike();
					this.get_all_like(); 
                    this.get_all_dislike(); 
					
				}
		});
	
	
	}
	 dislikebtn(){
		 console.log(this.users);
		console.log(this.user_value);
		console.log(this.data_rating_value);
		if(this.user_value = 'olduser'){
			this.new_rating_like = ((this.data_rating_value)/this.users);
		}
		if(this.user_value = 'newuser'){
			this.new_rating_like = ((this.data_rating_value+0)/this.users);
		}
		
		console.log(this.new_rating_like);
		if(this.users <= 9){
			var new_default_rating = this.default_rating;
			var new_actual_rating = this.new_rating_like;
			var new_number_of_users = this.users;
			var new_final_rating = this.default_rating;
		}else{
			var new_default_rating = this.default_rating;
			var new_actual_rating = this.new_rating_like;
			var new_number_of_users = this.users;
			var new_final_rating = this.new_rating_like;
		}
		if(!this.user_id){
		  this.panelService.openSnackBar('User Login is required ', 'Ok','errorMsg');
		  return;
		}
		this.loader = true;
		var normalizeSellerPayload = {
	
			'game_id': this.game_id,
			'user_id': this.user_id,
			'default_rating': new_default_rating,
			'actual_rating': new_actual_rating,
			'number_of_users': new_number_of_users,
			'final_rating': new_final_rating,
			'countlike': 'dislike',
			'apiname': 'insert_dislike' 
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				if(data.status == 201){
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					this.panelService.openSnackBar('Dislike Successfully', 'OK','successMsg');
					this.get_count_like();
					this.get_count_dislike();
					this.get_all_like(); 
                    this.get_all_dislike(); 
				}
		});
	
	}
	
	get_all_like(){
		 this.loader = true;
		var normalizeSellerPayload = {
			'user_id': this.user_id,
			'game_id': this.game_id,
			'apiname': 'list_like'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				console.log(data.data);
				if(data.status == 200){
					if(data.data.length > 0){
					console.log("noooooooooooo");
					$('.likethumb').attr('disabled', true);
					
				}else{
					$('.likethumb').attr('disabled', false);
				}
				}
				
		});
	}
	
	 get_all_dislike(){
		 this.loader = true;
		var normalizeSellerPayload = {
			'user_id': this.user_id,
			'game_id': this.game_id,
			'apiname': 'get_all_dislike'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				console.log(data.data);
				if(data.status == 200){
				if(data.data.length > 0){
					$('.dislikethumb').attr('disabled', true);
				}else{
					$('.dislikethumb').attr('disabled', false);
				}	
				}
				
		});
	}
	
	 get_all_rating(){
		var normalizeSellerPayload = {
			'apiname': 'list_rating'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				console.log(data.data);
		});
	}
	get_all_fav(){
		this.loader = true;
		var normalizeSellerPayload = {
			'game_id': this.game_id,
			'user_id': this.user_id,
			'apiname': 'list_fav'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				console.log(data.data);
				if(data.status == 200){
				if(data.data.length > 0){
					$('.favourite').addClass('addfav');
					this.getfavirate = 1;
					$('.add_to_favrate').text('Added to Fav');
				}else{
					$('.favourite').removeClass('addfav');
					this.getfavirate = 0;
					$('.add_to_favrate').text('Add to Fav');
				}
			}else{
				$('.favourite').removeClass('addfav');
				this.getfavirate = 0;
			}
		});
	}
	
	get_count_like(){
		this.loader = true;
		var normalizeSellerPayload = {
			'game_id': this.game_id,
			'user_id': this.user_id,
			'countlike': 'like',
			'apiname': 'list_like_count'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				console.log(data.data);
				this.count = data.data;
		});
	}
	
	get_count_dislike(){
		this.loader = true;
		var normalizeSellerPayload = {
			'game_id': this.game_id,
			'user_id': this.user_id,
			'countlike': 'dislike',
			'apiname': 'dislike_count'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				console.log(data.da1ta);
				this.countdislike = data.data;
		});
	}
	
	
	
	back_to_main_screen(){
		$('#unlock-button').trigger("click");
		$('.mobile_back').addClass('display_none');
		$('.game-p-header').removeClass('display_none1');
		//$('.active_game2').removeClass('display_none1');
		$('.active_game3').removeClass('display_none1');
		$('.games_content').removeClass('display_none1');
		$('.active_game5').removeClass('display_none1');
		//$('.how_to_play').removeClass('display_none1');
		$('.active_game7').removeClass('display_none1');
		$('.other_related_games').removeClass('display_none1');
		$('.game_user_comments').removeClass('display_none1');
		$('.iframewidth2').removeClass('display_none1');
		$('.website_menu').removeClass('display_none1');
		$('.website_footer').removeClass('display_none1');
		
		$('.mobile_play_game').removeClass('zero_p_m');
		$('.gamechange').removeClass('zero_p_m');
		$('.mobile_responsive').removeClass('zero_p_m');
		
		$('.gameframe').empty();
		$('.gameplay').removeClass('display_none');
		$('.gameshow').addClass('display_none');
		$('html').removeClass('portrait_mode');
		$('.gameframe').removeClass('gameframe_land');
		
		
	}
	

 
  gameplay(){
		if(this.check_device == 'mobile'){
			$('.mobile_back').removeClass('display_none');
				console.log('mobile');
				this.rotation_msg();
				
				$('.game-p-header').addClass('display_none1');
				//$('.active_game2').addClass('display_none1');
				$('.active_game3').addClass('display_none1');
				$('.games_content').addClass('display_none1');
				$('.active_game5').addClass('display_none1');
				//$('.how_to_play').addClass('display_none1');
				$('.active_game7').addClass('display_none1');
				$('.other_related_games').addClass('display_none1');
				$('.game_user_comments').addClass('display_none1');
				$('.iframewidth2').addClass('display_none1');
				$('.website_menu').addClass('display_none1');
				$('.website_footer').addClass('display_none1');
				$('.mobile_play_game').addClass('zero_p_m');
				$('.gamechange').addClass('zero_p_m');
				$('.mobile_responsive').addClass('zero_p_m');
			/* 	$('html, body').animate({
					scrollTop: $('.gameshow').offset().top - 170
				}, 80); */
				//$('.gameframe').append('<iframe style="width: '+this.game_width+'px;max-width: 100%;height: '+this.game_height+'px;" allowFullScreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" src="'+this.game_url+'" scrolling="none" frameborder="0"></iframe>');
			        if(this.game_orientation == 'horizontal'){
						
						if (window.innerHeight > window.innerWidth) {
							 $('#lock-landscape-button').trigger("click");
							$('.gameframe').addClass('gameframe_vertical');
						    //$('body').css('line-height' , '2.4');
						    $('.gameframe').append('<iframe style="width: 100%; height: 100%;" allowFullScreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" src="'+this.game_url+'" scrolling="none" frameborder="0"></iframe>');
						   }else{
							    $('#lock-landscape-button').trigger("click");
							  $('.gameframe').addClass('gameframe_vertical');
						       $('.gameframe').append('<iframe style="width: 100%; height: 100%;" allowFullScreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" src="'+this.game_url+'" scrolling="none" frameborder="0"></iframe>');
			          
						   }
					}else{
						if (window.innerHeight > window.innerWidth) {
							$('#lock-paitroit-button').trigger("click");
							   $('.gameframe').addClass('gameframe_vertical');
						     $('.gameframe').append('<iframe style="width: 100%; height: 100%;" allowFullScreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" src="'+this.game_url+'" scrolling="none" frameborder="0"></iframe>');
			        
						}else{
						$('#lock-paitroit-button').trigger("click");
						$('.gameframe').addClass('gameframe_vertical');
						$('.gameframe').append('<iframe style="width: 100%; height: 100%;" allowFullScreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" src="'+this.game_url+'" scrolling="none" frameborder="0"></iframe>');
			        
						}
					}
			/**/
		}else{
			$('.mobile_back').addClass('display_none');
			$('.gameframe').append('<iframe style="min-width: '+this.game_width+'px; height: '+this.game_height+'px;" allowFullScreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" src="'+this.game_url+'" scrolling="none" frameborder="0"></iframe>');
		}
		
		$('.gameplay').addClass('display_none');
		$('.gameshow').removeClass('display_none');
		var userid = '';
		if(this.user_id != ''){
			userid = this.user_id;
			this.username = this.user_name;
		}else{
			userid = '0'
		}
		this.start_time = Math.floor(Date.now() / 1000);
		var normalizeSellerPayload = {
			'game_id': this.game_id,
			'game_title': this.game_title,
			'game_slug': this.game_slug,
			'final_rating': this.final_rating,
			'games_category_id': this.game_category_id,
			'games_category_name': this.game_category_name,
			'game_image': this.game_image,
			'user_id': userid,
			'user_fullname': this.username,
			'start_time': this.start_time,
			'end_time': this.start_time,
			'duration': '0',
			'created_date': this.created_date,
			'apiname': 'user_play_game'
		} 
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				console.log(data.data);
				this.insertid = data.data;
					
				if(this.user_id == ''){
					let tasksDone = [];
						var addnew = JSON.parse(localStorage.getItem('user_play_game'));
						if(addnew){
						tasksDone.push(addnew);
						}
					var insertdata = {
							'game_id': this.game_id,
							'game_title': this.game_title,
							'games_category_id': this.game_category_id,
							'games_category_name': this.game_category_name,
							'user_id': this.user_id,
							'start_time': this.start_time,
							'end_time': this.start_time,
							'duration': '0',
							'created_date': this.created_date,
							'apiname': 'user_play_game'
						}
						
						
						tasksDone.push(insertdata);	
						console.log(tasksDone);
						
						localStorage.setItem("user_play_game", JSON.stringify(tasksDone));
				}
				
				console.log(this.insertid);
		});
		window.setInterval(() => {
			  this.add_game_play_time();
		}, 5000);  
		
		
  }
 
  add_game_play_time(){
	  var userid = '';
	  if(this.user_id != ''){
		userid = this.user_id;
	}else{
		userid = '0'
	}
	  var timeStamp = Math.floor(Date.now() / 1000);
	   var diff = Math.abs(this.start_time-timeStamp);
		var normalizeSellerPayload = {
			'insertid': this.insertid,
			'game_id': this.game_id,
			'end_time': timeStamp,
			'duration': diff,
			'apiname': 'add_game_play_time'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				console.log(data.data);
				
		});
	}
  
  /* setTimeout(function(){
	  this.add_game_play_time();
	  }, 5000); */
	   
  get_playgame(){
		this.loader = true;
		var normalizeSellerPayload = {
			
			'apiname': 'playgame'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				console.log(data.data);
		});
	}
 
  /* $('.gameplay').click(function(){
      console.log("play game");
 }); */
	
  commentSubmit(){
		
		if(!this.user_comment){
		  this.panelService.openSnackBar('Game comment is required', 'Ok','errorMsg');
		  return;
		}
		//this.loader = true;
		var normalizeSellerPayload = {
	
			'game_id': this.game_id,
			'game_title': this.game_title,
			'game_category_id': this.game_category_id,
			'game_category_name': this.game_category_name,
			'user_name': this.user_name,
			'user_email': this.email_id,
			'user_image': this.user_image,
			'user_comment': this.user_comment,
			'user_id': this.user_id,
			'profilepicturestatus': this.profilepicturestatus,
			'user_type': "customer",
			'comment_status': "Unapproved",
			'created_date': this.created_date,
			'created_time': this.created_time,
			'apiname': 'insert_comment' 
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				//this.loader = false;
				if(data.status == 201){
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					this.panelService.openSnackBar('your comment successfully submitted for admin permission', 'OK','successMsg');
					
					this.get_all_comment(); 
					this.get_all_user_unapprove_comment(); 
					this.user_comment = '';
				}
		});
	
	}
	
 
	
	
	
	 addtofav(){
		 if(!this.user_id){
		  this.panelService.openSnackBar('User Login is required ', 'Ok','errorMsg');
		  return;
		}
		this.loader = true;
		var normalizeSellerPayload = {
	
			'game_id': this.game_id,
			'game_title': this.game_title,
			'game_category_id': this.game_category_id,
			'game_category_name': this.game_category_name,
			'user_name': this.user_name,
			'user_id': this.user_id,
			'favourite_mark': this.getfavirate,
			'apiname': 'add_favourite' 
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				if(data.status == 201){
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					this.panelService.openSnackBar(data.message, 'OK','successMsg');
					this.get_all_fav(); 
				}
		});
	
	}
	
	 rating(){
		if(!this.user_id){
		  this.panelService.openSnackBar('User Login is required  ', 'Ok','errorMsg');
		  return;
		}
		this.loader = true;
		var normalizeSellerPayload = {
	
			'game_id': this.game_id,
			'user_id': this.user_id,
			'rating_value': this.rating_value,
			'apiname': 'insert_user_rating' 
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				if(data.status == 201){
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					this.panelService.openSnackBar(data.message, 'OK','successMsg');
				$('#rating_value').val('');	
				}
		});
	
	}
	
	reportSubmit(){
		
		this.report_subject = window.location.href;
		console.log("window.location.href");
		console.log(window.location.href);
		this.loader = true;
		var normalizeSellerPayload = {
	        'game_id': this.game_id,
			'game_title': this.game_title,
			'game_category_id': this.game_category_id,
			'game_category_name': this.game_category_name,
			'user_name': this.user_name,
			'user_id': this.user_id,
			'report_subject': this.report_subject,
			'report_message': this.report_message,
			'apiname': 'add_game_report' 
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				if(data.status == 201){
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					this.panelService.openSnackBar(data.message, 'OK','successMsg');
				    $('#browsereport .close').trigger('click');
				    $('#browsereport').removeClass('show');	
					this.report_message = '';
				}
		});
	}
	
  removegamecategory(game_category_id){
	  	var val = confirm("Do you want to delete");
		if(val == true){
        this.loader = true;
		var normalizeSellerPayload = {
			'apiname': 'delete_game_category',
			'game_category_id': game_category_id
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
			this.loader = false;
				//this.get_all_game_category_list();  
				if(data.status == 201){
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					this.panelService.openSnackBar(data.message, 'OK','successMsg');
				}
		});
			return true;
		}else{
			return false;
		}
	}
	/*
	editgamecategory(game_category_id, game_category_name){
		this.game_category_id = game_category_id;
		this.game_category_name = game_category_name;
	}*/
  
	addNewCategory(){
		this.game_category_id = 0;
		this.game_category_name = "";
	}
	
	
	
	async onshareBtn(action){
	  if(this.panelService.checkIfUserOnMobile()){
		this.onOpenSharer(action);
	  }else{
		
		let shareUrl = "www.feenu.com/g/"+this.game_slug;
		this.panelService.openShareWindowForWeb(action, shareUrl);
		this.addReward(action);
	  }
	}

	addReward(detail?){
	  let data = {
		type: 'share_link',
		detail: `Share Link ${detail ? 'on ' + detail : ''}`
	  }
	  this.loader = true;
	  this.panelService.addReward(data)
      .subscribe((res)=>{
        this.loader = false;
        this.rewardData = res;
        $('#referModal').modal('show');
        console.log(res);
      }, (err)=>{
        this.loader = false;
        console.log(err);
      })
  }
  
  
   // open sharer
   onOpenSharer(action){
 let shareUrl = "www.feenu.com/gamedetail/"+this.game_id;
    let newVariable: any;
    newVariable = window.navigator;
    if (newVariable && newVariable.share) {
      this.panelService.openNativeShare(shareUrl);
      // newVariable.share({
      //   title: '',
      //   text: '',
      //   url: shareUrl,
      // })
      // .then((e) =>  {
      //   console.log('Successful share', e);
      //   this.addReward();
      // })
      // .catch((error) => console.log('Error sharing', error));
    } else {
     let shareUrl = "www.feenu.com/gamedetail/"+this.game_id;
      this.panelService.openShareWindowForWeb(action, shareUrl);
      this.addReward(action);
      // alert('share not supported');
    }
  }
  
  usercommentinfo(user_id){
	  console.log(user_id)
	 	this.loader = true;
		var normalizeSellerPayload = {
			'apiname': 'get_user_profile',
			'user_id': user_id
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
			this.loader = false;
				this.first_name = data.data.first_name;
				this.last_name = data.data.last_name;
				this.gender = data.data.gender;
				this.email_id = data.data.email_id;
				this.google_plus_link = data.data.google_plus_link;
				this.twitter_link = data.data.twitter_link;
				this.fb_link = data.data.fb_link;
				this.profile_show = data.data.profile_show;
				this.admin_img_src = (data.data.user_image);
				console.log(data.data);
					console.log(this.profile_show);
					if(this.profile_show == 'Public'){
						this.game_play_info(user_id);
					}
			
		}); 
	}
	
	game_play_info(user_id){
			
		console.log("yes");
	var normalizeSellerPayload1 = {
			'user_id': user_id,
			'apiname': 'user_game_play_list'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload1)
			.subscribe( data => {
				this.gameplaylist = data.data;
				
				console.log(this.gameplaylist);
				  
		}); 
				
	}
	
	userprofile(userid,user_name){
		console.log(userid);
			this.loader = true;
		var normalizeSellerPayload = {
			'apiname': 'get_user_profile',
			'user_id': userid
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
			this.loader = false;
					if(data.data.profile_show != 'Public'){
						this.panelService.openSnackBar('User Profile is Private  ', 'Ok','errorMsg');
		                   return;
					}
			this.router.navigate(['u/',data.data.user_name]); 
		}); 
		
	}
  
  keyDownFunction(event) {
		 console.log('nooooooooo');
	  if(event.keyCode == 13) {
		  console.log('yesss');
		  this.commentSubmit();
	
	  }
	}
edit_user_comment(commentid,comment){

	this.comment_id = commentid;
	this.edit_comment = comment;
}
	
edit_commentSubmit(){
		
			this.loader = true;
		var normalizeSellerPayload = {
			'apiname': 'edit_comment',
			'comment_id': this.comment_id,
			'edit_comment': this.edit_comment,
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
			this.loader = false;
			this.get_all_user_unapprove_comment();
			$('#browseNewCategory').modal('hide');
		});
	
}

rotation_msg(){
	$('.rotation_msg').addClass('display_none');
}
  
}
