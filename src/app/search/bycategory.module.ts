import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { bycategoryComponent } from './bycategory.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [bycategoryComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([{
      path: '',
      component: bycategoryComponent
    }])
  ]
})
export class bycategoryModule { }
