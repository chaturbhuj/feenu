import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { searchComponent } from './search.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [searchComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([{
      path: '',
      component: searchComponent
    }])
  ]
})
export class searchModule { }
