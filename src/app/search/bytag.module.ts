import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { bytagComponent } from './bytag.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [bytagComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([{
      path: '',
      component: bytagComponent
    }])
  ]
})
export class bytagModule { }
