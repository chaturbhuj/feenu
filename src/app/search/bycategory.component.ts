import { Component, OnInit } from '@angular/core';
//import { Router } from '@angular/router';
import { PanelServices } from '../services/panel.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MetaConfig, MetaService } from 'ng2-meta';

declare var jquery : any;
declare var $ : any;

@Component({
  selector: 'app-bycategory',
  templateUrl: './bycategory.component.html',
  styleUrls: ['./bycategory.component.css']
})
export class bycategoryComponent implements OnInit {
	created_date = '';
	created_time = '';
	game_category_id=0;
	category_id='';
	gamelist= '';
	first_name = '';
	last_name = '';
	email_id = '';
	pass = '';
	searchgamelist = '';
	searchgamecatelist = '';
	searchgamecatelistdata = 0;
	searchby;
	populargamelist;
	confirm_pass = '';
	select_dropdown_content = '';
	category_name;
	gender = '';
	mycate = '';
	user_email = '';
	user_password = '';
	check_device = '';
	content = '';
	  length = 0;
	 loader;

   constructor(private router: Router, private activatedRoute: ActivatedRoute, private metaService: MetaService, 
              public panelService: PanelServices) {
			/* 	  
				 var seach_info = JSON.parse(localStorage.getItem("seachdata"));
			     // console.log(seach_info.cate_id);
				 if(seach_info){
					if(seach_info== null){
					}else if(seach_info.category_name){
					this.metaService.setTitle('Play '+ seach_info.category_name + ' Games on Feenu.com ');
					}
				 }
				 if(seach_info){
					  this.category_id = seach_info.cate_id
					  
				 } */
				this.category_name = activatedRoute.snapshot.params.mycate;
					console.log(this.category_name);
	}

	ngOnInit() {
	  var d = new Date();
		var month = d.getMonth()+1;
		var day = d.getDate();

		this.created_date = d.getFullYear() + '-' + (month<10 ? '0' : '') + month + '-' + (day<10 ? '0' : '') + day;
		this.created_time = (d.getHours()<10 ? '0' : '') + d.getHours() + ":" + (d.getMinutes()<10 ? '0' : '') + d.getMinutes() + ":" + (d.getSeconds()<10 ? '0' : '') + d.getSeconds();
		$('#my_cat_name').val(this.category_name);
		
		this.getFiltercategame();
		this.get_cate_name();
		if(!this.panelService.checkIfUserOnMobile()){
			$('.navbar').addClass('head1');
	        $('.footer').addClass('foot');
		}else{
			$('.header').addClass('head1');
		}
	  
  }

	
	getFiltercategame(){
	    if(this.panelService.checkIfUserOnMobile()){
			this.check_device = 'mobile';
		}else{
			this.check_device = 'pc';
			}
		var category_name = this.category_name.charAt(0).toUpperCase() + this.category_name.slice(1);
		console.log(category_name);
	    this.loader = true;
		var normalizeSellerPayload = {
			
			'category_id': this.category_id,
			'category_name': category_name,
			'category_slug': this.category_name,
			'check_device': this.check_device,
			'apiname': 'search_game_by_cate'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				 this.loader = false;
				console.log(this.category_name);
				if(data.status == 201){
				  this.router.navigate(['page-not-found']);
				}else{
					this.searchgamecatelist = data.data;
					this.searchgamecatelistdata = data.data.length;
					if(this.searchgamecatelist.length == 0){
					
						$('.no_search').removeClass('display_none');
							 this.getpopular();
					}else{
						 $('.no_search').addClass('display_none');
					}
					console.log(this.searchgamecatelist.length);
					var category_name = this.category_name.charAt(0).toUpperCase() + this.category_name.slice(1);
					console.log(this.category_name);
					//this.metaService.setTitle('Play '+ this.category_name + ' Games on Feenu.com ');
						if(data.data){
						$.each(this.searchgamecatelist , function(index, val) { 
						  console.log(index)
						  console.log(val.game_category_id)
						  $('#my_cat_id').val(val.game_category_id);
						});
					}
				}
				
			});
	}
	
	  getpopular(){
		if(this.panelService.checkIfUserOnMobile()){
			this.check_device = 'mobile';
		}else{
			this.check_device = 'pc';
			}
	    this.loader = true;
		var normalizeSellerPayload = {
			'check_device': this.check_device,
			'apiname': 'add_game_play_time_testing'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				
				var new_array = [];
				$.each(data.data, function(key, value) {
					if(value[0]){
						new_array.push(value[0]);
						}
					});
				this.populargamelist = new_array;
				console.log(this.populargamelist);
			});
	}
	
	get_cate_name(){
	 this.loader = true;
		var normalizeSellerPayload = {
			'category_id': this.category_id,
			'category_slug': this.category_name,
			'apiname': 'get_cate_name'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				 this.loader = false;
				if(data.status == 201){
				  this.router.navigate(['page-not-found']);
				}else{
					 this.content = data.data[0].content;
					this.metaService.setTitle('Play '+ data.data[0].game_category_name + ' Games on Feenu.com ');
					
				}
				
			});
	}
   
}
