import { Component, OnInit } from '@angular/core';
//import { Router } from '@angular/router';
import { PanelServices } from '../services/panel.service';
import { Router, ActivatedRoute } from '@angular/router';
declare var jquery : any;
declare var $ : any;

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class searchComponent implements OnInit {
	created_date = '';
	created_time = '';
	game_category_id=0;
	gamelist= '';
	first_name = '';
	last_name = '';
	email_id = '';
	pass = '';
	searchgamelist = '';
	searchgamecatelist = '';
	searchby;
	confirm_pass = '';
	category_name;
	gender = '';
	user_email = '';
	category_id = '';
	user_password = '';
	check_device = '';
	populargamelist = '';
	popular_active = 'no';
	  length = 0;
	 loader;

   constructor(private router: Router,private route: ActivatedRoute,
              public panelService: PanelServices) {
				  
				  this.route.queryParams.subscribe(params => {
			this.category_name = params['category_name'] ? params['category_name'] : null;
			this.category_id = params['category_id'] ? params['category_id'] : null;
			this.searchby = params['searchby'] ? params['searchby'] : null;

			console.log(this.searchby);
			console.log(this.category_name);
			if(this.searchby != 'null'){
				this.getFiltergame();
			}
            if(this.category_name != 'null'){
				this.getFiltercategame();
			}
            
			//this.getFiltergame();
		});
			  }

  ngOnInit(){
	  var d = new Date();
		var month = d.getMonth()+1;
		var day = d.getDate();

		this.created_date = d.getFullYear() + '-' + (month<10 ? '0' : '') + month + '-' + (day<10 ? '0' : '') + day;
		this.created_time = (d.getHours()<10 ? '0' : '') + d.getHours() + ":" + (d.getMinutes()<10 ? '0' : '') + d.getMinutes() + ":" + (d.getSeconds()<10 ? '0' : '') + d.getSeconds();
		
		//this.getFiltergame();
	if(!this.panelService.checkIfUserOnMobile()){
			$('.navbar').addClass('head1');
	        $('.footer').addClass('foot');
		}else{
			$('.header').addClass('head1');
		}
	$('.search_input').val(this.searchby);
  }
   
  getFiltergame(){
	   if(this.panelService.checkIfUserOnMobile()){
			this.check_device = 'mobile';
		}else{
			this.check_device = 'pc';
			}
	   this.loader = true;
		var normalizeSellerPayload = {
			'category_name': this.category_name,
			'searchby': this.searchby,
			'category_id': this.category_id,
			'check_device': this.check_device,
			'apiname': 'search_game'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				 this.loader = false;
				if(data.status == 201){
				   $('.no_search').removeClass('display_none');
				  this.popular_active = 'yes'
				  
				   this.getpopular();
				}else{
					this.searchgamelist = data.data;
					$('.no_search').addClass('display_none');
					this.popular_active = 'no'
					
				}
				
			});
	}
	
	getpopular(){
		if(this.panelService.checkIfUserOnMobile()){
			this.check_device = 'mobile';
		}else{
			this.check_device = 'pc';
			}
	    this.loader = true;
		var normalizeSellerPayload = {
			'check_device': this.check_device,
			'apiname': 'add_game_play_time_testing'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				this.populargamelist = data.data;
			});
	}
	
	getFiltercategame(){
	    this.loader = true;
		var normalizeSellerPayload = {
			'category_name': this.category_name,
			'apiname': 'search_game_by_cate'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				 this.loader = false;
				console.log(data.data);
				this.searchgamecatelist = data.data;
			});
	}
	
	
   
}
