import { Component, OnInit } from '@angular/core';
//import { Router } from '@angular/router';
import { PanelServices } from '../services/panel.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MetaConfig, MetaService } from 'ng2-meta';

declare var jquery : any;
declare var $ : any;

@Component({
  selector: 'app-bytag',
  templateUrl: './bytag.component.html',
  styleUrls: ['./bytag.component.css']
})
export class bytagComponent implements OnInit {
	created_date = '';
	created_time = '';
	game_category_id=0;
	category_id='';
	gamelist= '';
	first_name = '';
	last_name = '';
	email_id = '';
	pass = '';
	searchgamelist:any;
	searchgamecatelist = '';
	searchby;
	confirm_pass = '';
	select_dropdown_content = '';
	category_name;
	tag_name;
	tag_slug;
	gender = '';
	mycate = '';
	content = '';
	user_email = '';
	user_password = '';
	check_device = '';
	populargamelist:any;
	  length = 0;
	 loader;

   constructor(private router: Router, private activatedRoute: ActivatedRoute, private metaService: MetaService, 
              public panelService: PanelServices) {
				 
				this.tag_slug = activatedRoute.snapshot.params.mytag;
				/* this.tag_name = tags_name.charAt(0).toUpperCase() + tags_name.slice(1);	
				console.log(this.tag_name); */
	}

	ngOnInit() {
	  var d = new Date();
		var month = d.getMonth()+1;
		var day = d.getDate();

		this.created_date = d.getFullYear() + '-' + (month<10 ? '0' : '') + month + '-' + (day<10 ? '0' : '') + day;
		this.created_time = (d.getHours()<10 ? '0' : '') + d.getHours() + ":" + (d.getMinutes()<10 ? '0' : '') + d.getMinutes() + ":" + (d.getSeconds()<10 ? '0' : '') + d.getSeconds();
		$('#my_cat_name').val(this.category_name);
		
		//this.getFiltercategame();
		this.filtertags();
		this.get_tag_name_by_tag_slug();
	  if(!this.panelService.checkIfUserOnMobile()){
			$('.navbar').addClass('head1');
	        $('.footer').addClass('foot');
		}else{
			$('.header').addClass('head1');
		}
  }
   
 filtertags(){
	 if(this.panelService.checkIfUserOnMobile()){
			this.check_device = 'mobile';
		}else{
			this.check_device = 'pc';
			}
	   this.loader = true;
		var normalizeSellerPayload = {
			'tag_slug': this.tag_slug,
			'check_device': this.check_device,
			'apiname': 'search_game_by_tag'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
			if(data.status == 201){
				  $('.no_search').removeClass('display_none');
				   this.getpopular();
			}else{
				var pop_array = [];
				$.each(data.data, function(key, value) {
					if(value[0]){pop_array.push(value[0]);}
					});
				if(pop_array.length == 0){
					 $('.no_search').removeClass('display_none');
					$('.no_result_found').removeClass('display_none');
					   this.getpopular();
				}else{
					$('.no_search').addClass('display_none');
					this.searchgamelist = pop_array;
				} 
				console.log(this.searchgamelist);
			}
			
				//this.metaService.setTitle('Play '+ this.tag_slug + ' games on feenu.com ');
				
			});
	}

   getpopular(){
		if(this.panelService.checkIfUserOnMobile()){
			this.check_device = 'mobile';
		}else{
			this.check_device = 'pc';
			}
	    this.loader = true;
		var normalizeSellerPayload = {
			'check_device': this.check_device,
			'apiname': 'add_game_play_time_testing'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				
				var new_array = [];
				$.each(data.data, function(key, value) {
					if(value[0]){
						new_array.push(value[0]);
						}
					});
				this.populargamelist = new_array;
				console.log(this.populargamelist);
			});
	}	
	
	get_tag_name_by_tag_slug(){
		   this.loader = true;
		var normalizeSellerPayload = {
			'tag_slug': this.tag_slug,
			'apiname': 'get_tag_name_by_tag_slug'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				console.log(data.data[0].tag_name);
				this.content = data.data[0].content;
				this.metaService.setTitle('Play '+ data.data[0].tag_name + ' games on feenu.com ');
				
			});
	}
	
	getFiltercategame(){
	
		//var category_name = this.category_name.charAt(0).toUpperCase() + this.category_name.slice(1);
		//console.log(category_name);
	    this.loader = true;
		var normalizeSellerPayload = {
			'category_name': this.category_name,
			'category_id': this.category_id,
			'apiname': 'search_game_by_cate'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				 this.loader = false;
				console.log(this.category_name);
				this.searchgamecatelist = data.data;
					var category_name = this.category_name.charAt(0).toUpperCase() + category_name.slice(1);
					this.metaService.setTitle('Play '+ this.tag_slug + ' Games on Feenu.com ');
				if(data.data){
				$.each(this.searchgamecatelist , function(index, val) { 
				  console.log(index)
				  console.log(val.game_category_id)
				  $('#my_cat_id').val(val.game_category_id);
				});
			}
		});
	}
   
}
