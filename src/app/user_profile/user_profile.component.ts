import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { MetaConfig, MetaService } from 'ng2-meta';
import { PanelServices } from '../services/panel.service';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ChangeEvent } from '@ckeditor/ckeditor5-angular/ckeditor.component';

@Component({
  selector: 'app-user_profile',
  templateUrl: './user_profile.component.html',
  styleUrls: ['./user_profile.component.css']
})
export class user_profileComponent implements OnInit {
	loader:boolean = false;
	termAndConditionDoc = '';
	products_count = 0;
	recentproducts='';
	gamefavlist = '';
	email_id='';
	gender='';
	user_name ='';
	fb_link='';
	recentcustomerlist='';
	user_img_src='';
	gameplaylist: any = [];
	finalAr: any = [];
	currentPair: any = [];
	user_count = 0;
	game_count = 0;
	user_id = 'null';
	u_name = '';
	product_img_src='';
	customer_count = 0;
	user_profile_id = '';
	baseUrl='';
	mainUrl='';
	linkdin_link='';
	twitter_link='';
	private_public='';
	constructor(public router: Router,private activatedRoute: ActivatedRoute,
	private panelService: PanelServices,private metaService: MetaService,
	private location: Location) {
	$('body').removeClass('body_img');
	this.u_name = activatedRoute.snapshot.params.user_id;
	//console.log(this.user_id);
	 var user_data = JSON.parse(localStorage.getItem("userData"));
				if(user_data){
				if(user_data== null){
					
					}else if(user_data[0]._id){
						
					}
						this.user_id = user_data[0]._id;
						console.log(this.user_id);
                 }else{
					 
				 }
				 
			
  }
 
	ngOnInit() {
		
		
		this.get_user_profile_by_id();
			 
		this.metaService.setTitle('User deshboard of Feenu.com - Get To Know Us | Feenu ');
		this.metaService.setTag('og:image','assets/images/favicon.png');
		this.metaService.setTag('og:description','User deshboard of Feenu.com');	
		if(!this.panelService.checkIfUserOnMobile()){
				$('.navbar').addClass('head1');
				$('.footer').addClass('foot');
			}else{
				
			}
		
	}
	
	get_user_profile_by_id() {
	 this.loader = true;
		
		var normalizeSellerPayload = {
				'user_name': this.u_name,
				'apiname': 'user_profile'
			}
			this.panelService.comman_service_funcation(normalizeSellerPayload)
				.subscribe( data => {
					this.loader = false;
					console.log(data.data);
					this.user_name = data.data[0]['first_name'];
					this.user_profile_id = data.data[0]['_id'];
					this.email_id = data.data[0]['email_id'];
					this.gender = data.data[0]['gender'];
					this.fb_link = data.data[0]['fb_link'];
					this.twitter_link = data.data[0]['twitter_link'];
					this.linkdin_link = data.data[0]['google_plus_link'];
					this.private_public = data.data[0]['profile_show'];
					console.log(data.data[0]['user_image'])
					if(data.data[0]['user_image'] != 'profile.png'){
						this.user_img_src = data.data[0]['user_image'];
			         }else{
						this.user_img_src = this.panelService.returnbaseUrl()+'/'+('profile.png')
			
					 }
					 if(this.private_public != 'Public'){
						$('.private_public').removeClass('display_none'); 
					 }
					 
					  this.get_user_game_play_list(data.data[0]['_id']);	
		              this.get_user_game_fav_list(data.data[0]['_id']);
			});
	 }
	
	
	 get_user_game_play_list(userid) {
		 this.loader = true;
		var normalizeSellerPayload = {
				'user_id': userid,
				'apiname': 'user_game_duration_list'
			}
			this.panelService.comman_service_funcation(normalizeSellerPayload)
				.subscribe( data => {
					this.loader = false;
					
					//var finalAr = [];
					//var currentPair = [];
					for( var i=0; i<data.data.length; i++){
						this.currentPair = data.data[i];
						
						var pos = null;
						for(var j=0; j<this.finalAr.length;j++){
							var finalAr_j = this.finalAr[j];
							if(finalAr_j.game_id == this.currentPair.game_id){
								pos = j;
							} 
						} 
					  if(pos == null){
						this.finalAr.push(this.currentPair);
					  }					  
					} 
					this.gameplaylist = this.finalAr;
					if(this.gameplaylist == ''){
						$('.data_not_found').removeClass('display_none');
					}  
			});
	 }
	 
	 get_user_game_fav_list(userid) {
	 this.loader = true;
		var normalizeSellerPayload = {
				'user_id': userid,
				'apiname': 'get_user_fav_list'
			}
			this.panelService.comman_service_funcation(normalizeSellerPayload)
				.subscribe( data => {
					this.loader = false;
					this.gamefavlist = data.data;
					console.log("this.gamefavlist");
					console.log(this.gamefavlist);
					 if(this.gamefavlist == ''){
						$('.data_not_found_fav').removeClass('display_none');
					} 
			});
	 }
	 
	 gameurl(game_id){
		 console.log(game_id);
		 this.loader = true;
		 var normalizeSellerPayload = {
				'game_id': game_id,
				'apiname': 'get_game_detail'
			}
			this.panelService.comman_service_funcation(normalizeSellerPayload)
				.subscribe( data => {
					this.loader = false;
					console.log(data.data[0]['game_slug']);
					this.router.navigate(['gamedetail/',data.data[0]['game_slug']]);   
			});
	 }
	
	
}
