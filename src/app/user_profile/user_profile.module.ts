import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { user_profileComponent } from './user_profile.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [user_profileComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([{
      path: '',
      component: user_profileComponent
    }])
  ]
})
export class user_profileModule { }
