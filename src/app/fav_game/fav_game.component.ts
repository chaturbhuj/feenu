import { Component, OnInit } from '@angular/core';
//import { Router } from '@angular/router';
import { PanelServices } from '../services/panel.service';
import { Router, ActivatedRoute } from '@angular/router';
declare var jquery : any;
declare var $ : any;

@Component({
  selector: 'app-fav_game',
  templateUrl: './fav_game.component.html',
  styleUrls: ['./fav_game.component.css']
})
export class fav_gameComponent implements OnInit {
	created_date = '';
	created_time = '';
	game_category_id=0;
	gamelist= '';
	first_name = '';
	last_name = '';
	email_id = '';
	pass = '';
	searchgamelist = '';
	searchgamecatelist = '';
	searchby;
	confirm_pass = '';
	category_name;
	gender = '';
	user_email = '';
	category_id = '';
	user_password = '';
	check_device = '';
	  length = 0;
	 loader;

   constructor(private router: Router,private route: ActivatedRoute,
              public panelService: PanelServices) {
				  
				  this.route.queryParams.subscribe(params => {
			this.category_name = params['category_name'] ? params['category_name'] : null;
			this.category_id = params['category_id'] ? params['category_id'] : null;
			this.searchby = params['searchby'] ? params['searchby'] : null;

			console.log(this.searchby);
			console.log(this.category_name);
			if(this.searchby != 'null'){
				//this.getFiltergame();
			}
            if(this.category_name != 'null'){
				//this.getFiltercategame();
			}
            
			//this.getFiltergame();
		});
			  }

  ngOnInit() {
	  var d = new Date();
		var month = d.getMonth()+1;
		var day = d.getDate();

		this.created_date = d.getFullYear() + '-' + (month<10 ? '0' : '') + month + '-' + (day<10 ? '0' : '') + day;
		this.created_time = (d.getHours()<10 ? '0' : '') + d.getHours() + ":" + (d.getMinutes()<10 ? '0' : '') + d.getMinutes() + ":" + (d.getSeconds()<10 ? '0' : '') + d.getSeconds();
		
		this.getFiltergame();
	 if(!this.panelService.checkIfUserOnMobile()){
			$('.navbar').addClass('head1');
	        $('.footer').addClass('foot');
		}else{
			$('.header').addClass('head1');
		}
  }
   
  getFiltergame(){
	   if(this.panelService.checkIfUserOnMobile()){
			this.check_device = 'mobile';
		}else{
			this.check_device = 'pc';
			}
	   this.loader = true;
		var normalizeSellerPayload = {
			'check_device': this.check_device,
			'apiname': 'add_game_play_time_testing'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				console.log(data.data);
				this.searchgamelist = data.data;
			});
	}

   
}
