import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { fav_gameComponent } from './fav_game.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [fav_gameComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([{
      path: '',
      component: fav_gameComponent
    }])
  ]
})
export class fav_gameModule { }
