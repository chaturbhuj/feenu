import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { contactusComponent } from './contactus.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [contactusComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([{
      path: '',
      component: contactusComponent
    }])
  ]
})
export class contactusModule { }
