import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ChangeEvent } from '@ckeditor/ckeditor5-angular/ckeditor.component';

import { PanelServices } from '../services/panel.service';

declare var jquery: any;
declare var $ : any;
@Component({
  selector: 'app-publiccontent',
  templateUrl: './publiccontent.component.html',
  styleUrls: ['./publiccontent.component.css']
})
export class publiccontentComponent implements OnInit {
	public Editor = ClassicEditor;
	content_cat_ = '';
	content_id = 0;
	content = '';
	contentlist = '';
	content_title = '';
	 editorData = '';
	 loader;
  constructor(public router: Router, private panelService: PanelServices, private location: Location) {
	$('body').removeClass('body_img');
  }


  ngOnInit() {
    this.get_all_content_list(); 
    //this.change_category();  /*this funcation is used to remove public content*/
	
	if(!this.panelService.checkIfUserOnMobile()){
			$('.navbar').addClass('head1');
	        $('.footer').addClass('foot');
		}else{
			$('.navbar').addClass('head1');
		}
  }
  
	public_conten_change_get_detail(){
		
		this.loader = true;
		var normalizeSellerPayload = {
			'content_id': this.content_cat_,
			'apiname': 'by_id_public_content'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				this.editorData = (data.data.content);
				this.content_title = (data.data.content_title);
				this.content_id = (data.data._id);
				
		});
	}
  
  get_all_content_list(){
		this.loader = true;
		var normalizeSellerPayload = {
			'apiname': 'list_public_content'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				
				this.loader = false;
				 this.contentlist = data.data;
				console.log(this.contentlist);
		});
	}
	
	 change_category(){
		this.loader = true;
		var normalizeSellerPayload = {
			'content_id': '5d09f815cad221674b7de660',
			'apiname': 'update_public_cate'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				
				this.loader = false;
				/*  this.contentlist = data.data;
				console.log(this.contentlist); */
		});
	}
	
	public onChangeckeditor( { editor }: ChangeEvent ) {
		this.content = editor.getData();
	}
	
	onSubmit(){
		this.loader = true;
		console.log(this.content);
		var normalizeSellerPayload = {
			'content_id': this.content_id,
			'content_title': this.content_title,
			'content': this.content,
			'apiname': 'add_content'
		}
			
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				if(data.status == 201){
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					this.panelService.openSnackBar(data.message, 'OK','successMsg');
				}
		});
	}
}
