import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { publiccontentComponent } from './publiccontent.component';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [publiccontentComponent],
  imports: [
    CommonModule,
    SharedModule,CKEditorModule,
    RouterModule.forChild([{
      path: '',
      component: publiccontentComponent
    }])
  ]
})
export class publiccontentModule { }
