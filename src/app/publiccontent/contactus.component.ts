import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { MetaConfig, MetaService } from 'ng2-meta';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ChangeEvent } from '@ckeditor/ckeditor5-angular/ckeditor.component';

import { PanelServices } from '../services/panel.service';

declare var jquery: any;
declare var $ : any;
@Component({
  selector: 'app-contactus',
  templateUrl: './contactus.component.html',
  styleUrls: ['./contactus.component.css']
})
export class contactusComponent implements OnInit {
	public Editor = ClassicEditor;
contact_name='';
contact_email='';
contact_number='';
contact_subject='Feedback';
contact_message='';
contact_url='';
contact_facebook='';
contact_twitter='';
contact_address='';
contact_youtube='';
contact_pinterest='';
google_plus='';
content_title='';
content='';
contactus_content_id='';
created_date='';
created_time='';
	 loader;
  constructor(public router: Router, private panelService: PanelServices,private metaService: MetaService,
  private location: Location) {
	$('body').removeClass('body_img');
  }


  ngOnInit() {
	     var d = new Date();
		var month = d.getMonth()+1;
		var day = d.getDate();

		this.created_date = d.getFullYear() + '-' + (month<10 ? '0' : '') + month + '-' + (day<10 ? '0' : '') + day;
		this.created_time = (d.getHours()<10 ? '0' : '') + d.getHours() + ":" + (d.getMinutes()<10 ? '0' : '') + d.getMinutes() + ":" + (d.getSeconds()<10 ? '0' : '') + d.getSeconds();
	
    this.get_public_contactus_info();
    this.get_enquiry_list();
	if(!this.panelService.checkIfUserOnMobile()){
			$('.navbar').addClass('head1');
	        $('.footer').addClass('foot');
		}else{
			$('.header').addClass('head1');
		}
  }
  
	get_public_contactus_info(){
	this.loader = true;
		var normalizeSellerPayload = {
			'apiname': 'by_id_public_content',
			'content_id': '5cb80d05ce79b806542e1aab'
			
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				this.content = data.data.content;
				this.content_title = data.data.content_title;
				console.log(this.content);
		this.metaService.setTitle(' Contact Feenu.com - Get To Know Us | Feenu '+this.content_title);
		this.metaService.setTag('og:image','assets/images/favicon.png');
		this.metaService.setTag('og:description',this.content);
		});
	}
	
	get_enquiry_list(){
	
		var normalizeSellerPayload = {
			'apiname': 'list_enquiry'
			
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				console.log(data.data);
		});
	}
  
  
	
	onSubmit(){
		
		
		var normalizeSellerPayload = {
			
			'contact_name': this.contact_name,
			'contact_email': this.contact_email,
			'contact_number': this.contact_number,
			'contact_url': this.contact_url,
			'contact_facebook': this.contact_facebook,
			'contact_address': this.contact_address,
			'contact_youtube': this.contact_youtube,
			'contact_pinterest': this.contact_pinterest,
			'contact_twitter': this.contact_twitter,
			'google_plus': this.google_plus,
			'contactus_content_id': 1,
			'apiname': 'configure/update_public_contact_info'
		}
		
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				if(data.status == 201){
				
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					this.panelService.openSnackBar(data.message, 'OK','successMsg');
				
				}
		});
		
  }
  
  enquirySubmit(){
	var descripation = /^([a-zA-Z0-9\s]{30,})$/;
	
		if(!this.contact_name ){
		  this.panelService.openSnackBar('Contact name is required', 'Ok','errorMsg');
		  return;
		}
		
		if(!this.contact_email){
			this.panelService.openSnackBar(' Email field is required', 'Ok','errorMsg');
			return;
		}else{
			var EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
			if((!EMAIL_REGEXP.test(this.contact_email))){
				this.panelService.openSnackBar('Email not valid', 'Ok','errorMsg');
				return;
			}
		}
		if(!this.contact_subject){
		  this.panelService.openSnackBar('Contact subject is required', 'Ok','errorMsg');
		  return;
		}
		
		if(!this.contact_message){
		  this.panelService.openSnackBar('Contact message is required', 'Ok','errorMsg');
		  return;
		}
		
		
		this.loader = true;
		var normalizeSellerPayload = {
			'contact_name': this.contact_name,
			'contact_email': this.contact_email,
			'contact_subject': this.contact_subject,
			'contact_message': this.contact_message,
			'created_date': this.created_date,
			'created_time': this.created_time,
			'apiname': 'contact_enquiry_email'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				if(data.status == 201){
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					this.panelService.openSnackBar(data.message, 'OK','successMsg');
					//this.router.navigate(['aboutus']);
					this.contact_name = '';
					this.contact_email = '';
					this.contact_subject = '';
					this.contact_message = '';
				}
		});
	}
}
