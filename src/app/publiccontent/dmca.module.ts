import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { dmcaComponent } from './dmca.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [dmcaComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([{
      path: '',
      component: dmcaComponent
    }])
  ]
})
export class dmcaModule { }
