import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { privacyComponent } from './privacy.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [privacyComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([{
      path: '',
      component: privacyComponent
    }])
  ]
})
export class privacyModule { }
