import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { cookies_policyComponent } from './cookies_policy.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [cookies_policyComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([{
      path: '',
      component: cookies_policyComponent
    }])
  ]
})
export class cookies_policyModule { }
