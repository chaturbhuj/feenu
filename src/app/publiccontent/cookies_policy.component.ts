import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { MetaConfig, MetaService } from 'ng2-meta';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ChangeEvent } from '@ckeditor/ckeditor5-angular/ckeditor.component';

import { PanelServices } from '../services/panel.service';

declare var jquery: any;
declare var $ : any;
@Component({
  selector: 'app-cookies_policy',
  templateUrl: './cookies_policy.component.html',
  styleUrls: ['./cookies_policy.component.css']
})
export class cookies_policyComponent implements OnInit {
	public Editor = ClassicEditor;
contact_name='';
contact_email='';
contact_number='';
contact_url='';
contact_facebook='';
contact_twitter='';
contact_address='';
contact_youtube='';
contact_pinterest='';
google_plus='';
content='';
contentlist='';
content_title='';
contactus_content_id='';
	 loader;
  constructor(public router: Router, private panelService: PanelServices,private metaService: MetaService,
  private location: Location) {
	$('body').removeClass('body_img');
  }


  ngOnInit() {
    this.get_public_privacy_info();
	if(!this.panelService.checkIfUserOnMobile()){
			$('.navbar').addClass('head1');
	        $('.footer').addClass('foot');
		}else{
			$('.header').addClass('head1');
		}
  }
  

  get_all_content_list(){
		this.loader = true;
		var normalizeSellerPayload = {
			'apiname': 'list_public_content'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				 this.contentlist = data.data;
				console.log(this.contentlist);
		});
	}
	
	get_public_privacy_info(){
		this.loader = true;
		var normalizeSellerPayload = {
			'apiname': 'by_id_public_content',
			'content_id': '5cb80d9eedf7781600242452'
			
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				this.content = data.data.content;
				this.content_title = data.data.content_title;
				console.log(this.content);
		this.metaService.setTitle('cookies policy of Feenu.com - Get To Know Us | Feenu '+this.content_title);
		this.metaService.setTag('og:image','assets/images/favicon.png');
		this.metaService.setTag('og:description',this.content);
		});
	}
	
	onSubmit(){
		
		this.loader = true;
		var normalizeSellerPayload = {
			
			'contact_name': this.contact_name,
			'contact_email': this.contact_email,
			'contact_number': this.contact_number,
			'contact_url': this.contact_url,
			'contact_facebook': this.contact_facebook,
			'contact_address': this.contact_address,
			'contact_youtube': this.contact_youtube,
			'contact_pinterest': this.contact_pinterest,
			'contact_twitter': this.contact_twitter,
			'google_plus': this.google_plus,
			'contactus_content_id': 1,
			'apiname': 'configure/update_public_contact_info'
		}
		
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				if(data.status == 201){
				    this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					this.panelService.openSnackBar(data.message, 'OK','successMsg');
				
				}
		});
		
  }
}
