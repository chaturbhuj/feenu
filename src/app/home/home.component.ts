import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MetaConfig, MetaService } from 'ng2-meta';
import { Location } from '@angular/common';
import { FormGroup, FormControl } from "@angular/forms";
import { PanelServices } from '../services/panel.service';

declare var jquery: any;
declare var $ : any;
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class homeComponent implements OnInit {
    game_category_name = '';
	created_date = '';
	created_time = '';
	game_category_id = 0;
	gamelist:any[];
	array1:any[];
	array2:any[];
	array3:any[];
	first_name = '';
	last_name = '';
	recentaddedgame = ''; 
	email_id = '';
	pass = '';
	confirm_pass = '';
	gender = '';
	user_email = '';
	user_password = '';
	check_device = '';
	gamelimit = 70;
	gameskip = 70;
	total_game = 0;
	total_game_mobile = 0;
	device = '';
	recent_game_list = '';
	populargamelist = '';
	content = '';
	loader;
	constructor(public router: Router,private activatedRoute: ActivatedRoute,private metaService: MetaService,
              private panelService: PanelServices) {
				$('body').removeClass('body_img');
			
			}
 
	ngOnInit(){
     	        if(!this.panelService.checkIfUserOnMobile()){
					  $(window).scroll(function() {
						$('.load_game').trigger('click');
					});
					}else{
						$(window).scroll(function() {
						   $('.load_game_mobile').trigger('click');
						});
					}
		var d = new Date();
		var month = d.getMonth()+1;
		var day = d.getDate();

		this.created_date = d.getFullYear() + '-' + (month<10 ? '0' : '') + month + '-' + (day<10 ? '0' : '') + day;
		this.created_time = (d.getHours()<10 ? '0' : '') + d.getHours() + ":" + (d.getMinutes()<10 ? '0' : '') + d.getMinutes() + ":" + (d.getSeconds()<10 ? '0' : '') + d.getSeconds();
		
		
		this.count_total_no_game(); 
		this.get_footer_home_text(); 
		//this.home_page_default_game();
		if(!this.panelService.checkIfUserOnMobile()){
		   this.show_game(); 
		 this.home_page_game(); 
		  this.device = 'pc';
		}else{
			this.show_game(); 
			this.home_page_game(); 
			this.device = 'mobile';
		}
		
		this.metaService.setTitle('Play 1000s of Html5 and WebGL games on Feenu.com');
		this.metaService.setTag('og:image','assets/images/favicon.png'); 
		if(!this.panelService.checkIfUserOnMobile()){
			$('.navbar').addClass('head1');
	        $('.footer').addClass('foot');
		}else{
			$('.header').addClass('head1');
			
		}
		
	}

	
	get_footer_home_text(){
	this.loader = true;
		var normalizeSellerPayload = {
			'apiname': 'by_id_public_content',
			'content_id': '5d5bbaddaca7d118a4496863'
			
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				if(data.status == 200){
				    this.content = data.data.content;
				}else{
					
				}
				
		});
	}
	
	
	count_total_no_game(){
		if(this.panelService.checkIfUserOnMobile()){
			this.check_device = 'mobile';
		}else{
			this.check_device = 'pc';
			}
		var normalizeSellerPayload1 = {
			'check_device':this.check_device,
			'apiname': 'count_total_no_game_pc'
		}
			this.panelService.comman_service_funcation(normalizeSellerPayload1)
				.subscribe( data => {
					this.total_game = data.data;
					 
			})
	}
	
	
	
  
  onSubmit(){
	    if(!this.user_email ){
		  this.panelService.openSnackBar('User email is required', 'Ok','errorMsg');
		  return;
		}
		if(!this.user_password ){
		  this.panelService.openSnackBar('Password name is required', 'Ok','errorMsg');
		  return;
		}
		
		this.loader = true;
		var normalizeSellerPayload = {
			'user_email': this.user_email,
			'user_password': this.user_password,
			'apiname': 'userlogin'
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
				this.loader = false;
				if(data.status == 201){
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					localStorage.setItem('userData' , JSON.stringify(data.data));
					this.panelService.openSnackBar(data.message, 'OK','successMsg');
					//this.get_all_game_category_list(); 
					$("#browseLogin").modal('hide');
				}
				
		});
		
  }
 
  removegamecategory(game_category_id){
	  	var val = confirm("Do you want to delete");
		if(val == true){
  this.loader = true;
		var normalizeSellerPayload = {
			'apiname': 'delete_game_category',
			'game_category_id': game_category_id
		}
		this.panelService.comman_service_funcation(normalizeSellerPayload)
			.subscribe( data => {
			this.loader = false;
				//this.get_all_game_category_list();  
				if(data.status == 201){
					this.panelService.openSnackBar(data.message, 'OK','errorMsg');
				}else{
					this.panelService.openSnackBar(data.message, 'OK','successMsg');
				}
		});
			return true;
		}else{
			return false;
		}
	}
	
	
	
	home_page_game(){
		this.gamelimit = this.gamelimit+20;
		if(this.panelService.checkIfUserOnMobile()){
			this.check_device = 'mobile';
		}else{
			this.check_device = 'pc';
			}
		
		
		this.loader = true;
	      var normalizeSellerPayload = {
		    'check_device': this.check_device,
			'gamelimit': this.gamelimit,
			'apiname': 'list_hundred_games'
		   }
			this.panelService.comman_service_funcation(normalizeSellerPayload)
				.subscribe( data => {
				this.loader = false ;
				if(data.status == 200){
				    var arr_new = [];
				    $.each(data.data, function(key, value) {
							console.log(value[0]);
							if(value[0]){
							  arr_new.push(value[0]);	
							}
						});
					this.gamelist = arr_new;
				}
				
			
				
			})
	}
	
	home_page_load_more_game(){
		this.gameskip = this.gameskip+20;
		if(this.panelService.checkIfUserOnMobile()){
			this.check_device = 'mobile';
		}else{
			this.check_device = 'pc';
			}
		if(this.gameskip >= this.total_game){
			$('.load_more').addClass('display_none');
			$('.load_more_mobile').addClass('display_none');
			return false;
		}
		this.loader = true;
	      var normalizeSellerPayload = {
		    'check_device': this.check_device,
			'gameskip': this.gameskip,
			'apiname': 'home_page_load_more_game'
		   }
			this.panelService.comman_service_funcation(normalizeSellerPayload)
				.subscribe( data => {
				this.loader = false ;
				if(data.status == 200){
					var arr_ne = data.data;
					/* load more merge game  */
					var arr_new = [];					
					arr_new = this.gamelist;
					$.each(data.data, function(key, value) {
						console.log(value[0]);
						if(value[0]){
							arr_new.push(value[0]);	
						}
					});
					//var arr3 = $.merge( arr_new, arr_ne );
					this.gamelist = arr_new;
					console.log(this.gamelist);
					console.log(this.gamelist.length);
					
				}
			})
		
	}
	
	show_game(){
		if(this.panelService.checkIfUserOnMobile()){
			this.check_device = 'mobile';
		}else{
			this.check_device = 'pc';
			}
	   var normalizeSellerPayload1 = {
			'check_device': this.check_device,
			'limit': 10,
			'apiname': 'home_game_recent'
		}
			this.panelService.comman_service_funcation(normalizeSellerPayload1)
				.subscribe( data => {
					this.recentaddedgame = data.data;
              				
			})
		
	}
	
	editgamecategory(game_category_id, game_category_name){
		this.game_category_id = game_category_id;
		this.game_category_name = game_category_name;
	}
  
	addNewCategory(){
		this.game_category_id = 0;
		this.game_category_name = "";
	}
	
	
  
}
